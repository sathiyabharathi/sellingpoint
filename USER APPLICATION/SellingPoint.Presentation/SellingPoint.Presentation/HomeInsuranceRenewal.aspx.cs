﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class HomeInsuranceRenewal : System.Web.UI.Page
    {
        General master;

        public HomeInsuranceRenewal()
        {
            master = Master as General;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!IsPostBack)
            {
                // dropdown();
                loadd();
                //divThankYou.Visible = false;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        private void ExportGridToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "MotorRenew" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            gvHomeInsurance.GridLines = GridLines.Both;
            gvHomeInsurance.HeaderStyle.Font.Bold = true;
            gvHomeInsurance.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();

        }
        public void loadd()
        {
            if (!master.IsSessionAvailable())
            {
                master.RedirectToLogin();
            }
            var service = master.GetService();
            var fetchdetailsrequest = new AdminFetchHomeDetailsRequest();

            fetchdetailsrequest.DocumentNo = "";
            fetchdetailsrequest.Type = "Renewal";


            var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                         <BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchHomeDetailsResponse>,
                         BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchHomeDetailsRequest>
                       (BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.FetchDetails, fetchdetailsrequest);

            DataTable dt = new DataTable();

            if (result.StatusCode == 200)
            {
                if (result.Result.IsTransactionDone)
                {

                    gvHomeInsurance.DataSource = result.Result.HomeDetails;
                    gvHomeInsurance.DataBind();

                }
                else
                {
                    // lbler.Text = result.Result.TransactionErrorMessage;
                }

            }
            else
            {
                //  lbler.Text = result.ErrorMessage;
            }
        }
    }
}