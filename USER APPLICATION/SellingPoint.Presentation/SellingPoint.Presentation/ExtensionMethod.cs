﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace BKIC.SellingPoint.Presentation
{
    public static class ExtensionMethod
    {
        public static List<T> BindList<T>(this DataTable dt)
        {

            var fields = typeof(T).GetFields();

            List<T> lst = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                // Create the object of T
                var ob = Activator.CreateInstance<T>();

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        // Matching the columns with fields
                        if (fieldInfo.Name == dc.ColumnName)
                        {
                            // Get the value from the datatable cell
                            object value = dr[dc.ColumnName];

                            // Set the value into the object
                            fieldInfo.SetValue(ob, value);
                            break;
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }

        public static string ConvertTodatestring(this DateTime dt)
        {
            if (dt != null)
            {
                string DateTime = Convert.ToString(dt);
                string[] date = DateTime.Split();
                return date[0];
            }

            return "";
        }


        public static string ConvertTodatestringNullable(this DateTime? dt)
        {
            if (dt != null)
            {
                string DateTime = Convert.ToString(dt);
                string[] date = DateTime.Split();
                return date[0];
            }

            return "";
        }


        public static DateTime CovertToCustomDateTime(this string value)
        {
            return DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture);
        }

        public static DateTime CovertToCustomDateTime2(this string value)
        {

            string[] formats = {
                "MM/dd/yyyy hh:mm:ss tt",
                "M/dd/yyyy hh:mm:ss tt", "M/d/yyyy hh:mm:ss tt", "M/d/yyyy hh:mm:ss tt"
            ,  "M/d/yyyy hh:mm:ss tt"};


            return DateTime.ParseExact(value, formats, CultureInfo.CurrentCulture, DateTimeStyles.None);
        }

        public static DateTime? ConvertToDateTimeNull(this string value)
        {
            return !(string.IsNullOrEmpty(value)) ? DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                : (DateTime?)null;
        }

        public static string CovertToLocalFormat(this DateTime value)
        {
            return value.ToString("dd/MM/yyyy");
        }

        public static string ConvertToLocalFormat(this DateTime? value)
        {
            if (value.HasValue)
            {
                return value.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                return "";
            }
        }

        public static void MsgBox(String ex, Page pg, Object obj)
        {
            string s = "<SCRIPT language='javascript'>alert('" + ex.Replace("\r\n", "\\n").Replace("'", "") + "'); </SCRIPT>";
            Type cstype = obj.GetType();
            ClientScriptManager cs = pg.ClientScript;
            cs.RegisterClientScriptBlock(cstype, s, s.ToString());
        }

    }
}