﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class DomesticPolicyDetails : System.Web.UI.Page
    {
        General master;

        public DomesticPolicyDetails()
        {
            master = Master as General;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            var service = master.GetService();

            if (Request.QueryString["DomesticId"] != null && Request.QueryString["InsuredCode"] != null)
            {
                int DomesticId = Convert.ToInt32(Request.QueryString["DomesticId"]);
                string InsuredCode = Convert.ToString(Request.QueryString["InsuredCode"]);
                BindDomesticPolicyPage(DomesticId, InsuredCode);
            }
        }

        private void BindDomesticPolicyPage(int domesticId, string insuredCode)
        {
            var service = master.GetService();

            var client = new BKIC.SellingPoint.Presentation.ClientUtility();
            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);

            var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyDomesticRequest();
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;

            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                req.AgentBranch = userInfo.AgentBranch;
                req.AgentCode = userInfo.AgentCode;
                //req.Agency = userInfo.;
            }
            var url = BKIC.SellingPoint.DTO.Constants.DomesticURI.GetSavedQuote.Replace("{domesticID}", domesticId.ToString()).Replace("{insuredCode}", insuredCode);
            var domesticDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                  <BKIC.SellingPoint.DTO.RequestResponseWrappers.DomesticHelpSavedQuotationResponse>>(url);

            var res = domesticDetails.Result.DomesticHelp;

            //Gv
            var DomesticHelps = domesticDetails.Result.DomesticHelpMemberList;
            gvDomesticHelp.DataSource = DomesticHelps;
            gvDomesticHelp.DataBind();

            //Details
            lblInsured.InnerText = res.FullName;
            lblcpr.InnerText = res.CPR;
            lblPolicyExpDate.InnerText = res.PolicyExpiryDate.CovertToLocalFormat();
            //cpr.InnerText = res.CPR;

        }
    }
}