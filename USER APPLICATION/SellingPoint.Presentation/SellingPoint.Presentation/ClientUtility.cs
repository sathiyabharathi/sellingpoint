﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BKIC.SellingPoint.Presentation
{
    public class ClientUtility
    {
        public static string WebApiUri = ConfigurationManager.AppSettings["WebApiUri"].ToString();
        public BKIC.SellingPoint.DTO.RequestResponseWrappers.OAuthTokenResponse UserInfo;
        public KBIC.Utility.DataServiceManager serviceManger;

        public static string CredimaxMerchantid = ConfigurationManager.AppSettings["CredimaxMerchantId"].ToString();
        public static string CredimaxApiUserName = ConfigurationManager.AppSettings["CrediMaxAPIUserName"].ToString();
        public static string CredimaxApiPassword = ConfigurationManager.AppSettings["CrediMaxAPIPassword"].ToString();
        public static string CredimaxSessionApi = ConfigurationManager.AppSettings["CrediMaxSessionAPI"].ToString()
            .Replace("{merchantId}", CredimaxMerchantid);
        public static string CredimaxCheckoutJs = ConfigurationManager.AppSettings["CrediMaxCheckOutJS"].ToString();
        public static string FrontEndWebUri = ConfigurationManager.AppSettings["WebFrontEndUri"].ToString();
        public static string QuickRenewUserName = ConfigurationManager.AppSettings["QuickRenewUserName"].ToString();
        public static string QuickRenewPassword = ConfigurationManager.AppSettings["QuickRenewPassword"].ToString();
        public static string IsTestEnvironment = ConfigurationManager.AppSettings["Test"].ToString();
        public static string BKICLogUrl = ConfigurationManager.AppSettings["BKICLogoUrl"].ToString();
        public static string CredimaxSessionRetrieveAPI = ConfigurationManager.AppSettings["CrediMaxSessionRetrieveAPI"].ToString()
            .Replace("{merchantId}", CredimaxMerchantid);
        public static string CredimaxTransactionRetrieveAPI = ConfigurationManager.AppSettings["CrediMaxTransactionRetrieveAPI"].ToString()
            .Replace("{merchantId}", CredimaxMerchantid);

        public ClientUtility()
        {
            UserInfo = new BKIC.SellingPoint.DTO.RequestResponseWrappers.OAuthTokenResponse();
        }

    }
    
}
