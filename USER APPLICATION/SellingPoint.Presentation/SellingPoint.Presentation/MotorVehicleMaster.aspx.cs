﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using KBIC.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class MotorVehicleMaster : System.Web.UI.Page
    {
        General master;
        public MotorVehicleMaster()
        {
            master = Master as General;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!Page.IsPostBack)
            {
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();
                btnSubmit.Text = "Save";
                LoadVehicle(userInfo, service);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                if (Page.IsValid)
                {
                    string opertaion = string.Empty;
                    //master.ShowLoading = true;

                    var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                    if (userInfo == null)
                    {
                        Response.Redirect("Login.aspx");
                    }
                    var service = master.GetService();

                    var details = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMaster();

                    details.ManufacturerID = txtManufacturarID.Text.Trim();
                    details.ModelID = txtModelID.Text.Trim();
                    details.BodyType = txtBodyType.Text.Trim();
                    details.Capacity = string.IsNullOrEmpty(txtCapacity.Text) ? 0 : Convert.ToInt32(txtCapacity.Text);
                    details.Excess = string.IsNullOrEmpty(txtExcess.Text) ? 0 : Convert.ToDecimal(txtExcess.Text);
                    details.Value = string.IsNullOrEmpty(txtCapacity.Text) ? 0 : Convert.ToInt32(txtCapacity.Text);
                    details.VehicleType = "";
                    details.Category = "";

                    details.SeatingCapacity = string.IsNullOrEmpty(txtSeatingCapacity.Text) ? 0 : Convert.ToInt32(txtSeatingCapacity.Text);
                    details.Year = string.IsNullOrEmpty(txtYear.Text) ? 0 : Convert.ToInt32(txtYear.Text);

                    opertaion = (sender as Button).Text;
                    if (opertaion == "Update")
                    {
                        details.ID = Convert.ToInt32(ViewState["Id"].ToString());
                        details.Type = "update";
                    }
                    else
                    {
                        details.Type = "insert";

                    }

                    var results = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                  <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMasterResponse>,
                                  BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMaster>
                                  (BKIC.SellingPoint.DTO.Constants.AdminURI.MotorVehicleOperation, details);

                    if (results.StatusCode == 200 && results.Result.IsTransactionDone)
                    {                     
                        btnSubmit.Text = "Save";
                        if (details.Type == "insert")
                        {
                            master.ShowErrorPopup("Motor vehicle added successfully", "Motor Vehicle");
                        }
                        if (details.Type == "Update")
                        {
                            master.ShowErrorPopup("Motor vehicle updated successfully", "Motor Vehicle");
                        }
                        LoadVehicle(userInfo, service);
                    }
                    else
                    {
                        master.ShowErrorPopup(results.ErrorMessage, "Request Failed!");
                    }
                }
             }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnSubmit.Text = "Save";            
            Response.Redirect("Homepage.aspx");
        }
        protected void lnkbtnDelete_Click(object sender, EventArgs e)
        {
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                
                int id = Convert.ToInt32(row.Cells[1].Text.Trim());
                var details = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMaster();
                details.ID = id;
                details.Type = "delete";

                var vehicleResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                    <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMasterResponse>,
                                    BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMaster>
                                    (BKIC.SellingPoint.DTO.Constants.AdminURI.MotorVehicleOperation, details);

                if (vehicleResult.StatusCode == 200 && vehicleResult.Result.IsTransactionDone)
                {
                    LoadVehicle(userInfo, service);
                    master.ShowErrorPopup("Motor vehicle deleted successfully", "Motor Vehicle");
                }
            }
        }

        protected void lnkbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
                {
                   
                    if (ViewState["Id"] != null)
                    {
                        ViewState["Id"] = string.Empty;
                    }
                    string id = row.Cells[1].Text.Trim();
                    ViewState["Id"] = id;

                    txtManufacturarID.Text = HttpUtility.HtmlDecode(row.Cells[2].Text);
                    txtModelID.Text = HttpUtility.HtmlDecode(row.Cells[3].Text);
                    txtCapacity.Text = HttpUtility.HtmlDecode(row.Cells[4].Text);
                    txtValue.Text = HttpUtility.HtmlDecode(row.Cells[5].Text);
                    txtExcess.Text = HttpUtility.HtmlDecode(row.Cells[6].Text);
                    txtBodyType.Text = HttpUtility.HtmlDecode(row.Cells[7].Text);
                    btnSubmit.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }

        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dlist.DefaultView.Sort = e.SortExpression + " " + SortDir(e.SortExpression);
            //gvMotorInsurance.DataSource = dlist;
            //gvMotorInsurance.DataBind();
        }
        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            gvMotorVehicle.PageIndex = e.NewPageIndex;
            LoadVehicle(userInfo, service); //bindgridview will get the data source and bind it again
        }

        private void LoadVehicle(OAuthTokenResponse userInfo, DataServiceManager service)
        {

            var request = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMaster
            {
                Type = "fetch",
                ManufacturerID = txtMakeSearch.Text.Trim()
            };
            var results = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                 <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMasterResponse>,
                                 BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorVehicleMaster>
                                 (BKIC.SellingPoint.DTO.Constants.AdminURI.MotorVehicleOperation, request);

            if (results.StatusCode == 200 && results.Result.IsTransactionDone)
            {
                gvMotorVehicle.DataSource = results.Result.MotorVehicleMaster;
                gvMotorVehicle.DataBind();
            }
        }
        protected void gvMotorVehicle_DataBound(object sender, EventArgs e)
        {
        }

        protected void btnMakeSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();
                LoadVehicle(userInfo, service);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }
    }
}