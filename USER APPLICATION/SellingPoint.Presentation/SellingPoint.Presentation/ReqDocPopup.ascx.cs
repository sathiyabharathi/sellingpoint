﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using KBIC.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class ReqDocPopup : System.Web.UI.UserControl
    {
        public string InsuredCode
        {
            get { return this.txtInsuredIDReqDoc.Text.Trim(); }
            set { this.txtInsuredIDReqDoc.Text = value; }
        }

        public string RefID
        {
            get { return this.RefIDReqDoc.Text.Trim(); }
            set { this.RefIDReqDoc.Text = value; }
        }
        public string Message
        {
            get { return this.txtMessage.Text.Trim(); }
            set { this.txtMessage.Text = value; }
        }

        public bool ShowModal
        {
            get { return RequestDocPopup.Visible; }
            set { this.RequestDocPopup.Visible = value; }
        }

        public string InsuranceType
        {
            get { return this.hiddenInsuranceType.Value; }
            set { this.hiddenInsuranceType.Value = value; }
        }

        public string PolicyNo
        {
            get { return this.hiddenPolicyNo.Value; }
            set { this.hiddenPolicyNo.Value = value; }
        }

        public string LinkId
        {
            get { return this.hiddenLinkId.Value; }
            set { this.hiddenLinkId.Value = value; }
        }

        protected void btnClosePopup_Click(object sender, EventArgs e)
        {
            this.RequestDocPopup.Visible = false;
            this.InsuredCode = "";
            this.RefID = "";
            this.Message = "";

        }

        //private System.Delegate _parentPageFunc;
        //public System.Delegate ParentPageFunc
        //{
        //    set { _parentPageFunc = value;
        //    }
        //    get { return _parentPageFunc; }
        //}

        public delegate void LoadGrid();
        public event LoadGrid LoadParentPageGrid;

        protected void btnSubmitReqDoc_Click(object sender, EventArgs e)
        {
            var session = new OAuthTokenResponse();

            if (Session["UserInfo"] != null)
            {
                session = Session["UserInfo"] as OAuthTokenResponse;
            }

            var service = new DataServiceManager(ClientUtility.WebApiUri, session.AccessToken, false);

            var request = new UpdateHIRStatusRequest();
            request.HIRStatusCode = 4;
            request.ID = Convert.ToInt32(RefID);
            request.InsuranceType = this.hiddenInsuranceType.Value;
            request.Message = Message;
            request.InsuredCode = this.InsuredCode;
            request.DocumentNo = this.PolicyNo;
            request.LinkId = this.LinkId;
            request.URL = Constants.WebRedirectUrl + "?UserId=" + this.InsuredCode + "&&type=Motor&&" + "&&RefID=" + this.RefID;
            var approvedresponse = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateHIRStatusResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateHIRStatusRequest>
                                   (BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.UpdateHIRStatus, request);

            if (approvedresponse.StatusCode == 200 && approvedresponse.Result.IsTransactionDone == true)
            {
                this.RequestDocPopup.Visible = false;
                this.InsuredCode = "";
                this.RefID = "";
                this.Message = "";
                //object[] obj = new object[1];
                //obj[0] = 
                //this.ParentPageFunc.DynamicInvoke();
                //LoadParentPageGrid();
            }


        }
    }
}