﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class TravelInsuranceEdit : System.Web.UI.Page
    {
        General master;
        public TravelInsuranceEdit()
        {
            master = Master as General;
        }

       // CommonMethods methods = new CommonMethods();
        public static DataTable Nationalitydt;
        public static DataTable Relationdt;
        public static DataTable Genderdt;

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!IsPostBack)
            {
                BindDropDown();
                if (Request.QueryString["Ref"] != null && Request.QueryString["InsuredCode"] != null)
                {
                    string travelID = Convert.ToString(Request.QueryString["Ref"]);
                    string InsuredCode = Convert.ToString(Request.QueryString["InsuredCode"]);
                    BindDetails(travelID, InsuredCode);

                }
            }

        }

        public void BindDetails(string motorID, string insuredCode)
        {
            var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuotation.Replace("{travelQuotationId}", motorID);
            url = url.Replace("{userInsuredCode}", insuredCode);
            url = url.Replace("{type}", "Portal");
            var service = master.GetService();
            var traveldetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelSavedQuotationResponse>>(url);

            if (traveldetails.StatusCode == 200 && traveldetails.Result.IsTransactionDone == true)
            {
                var quotationresult = new TravelSavedQuotationResponse();
                quotationresult.TravelInsurancePolicyDetails = traveldetails.Result.TravelInsurancePolicyDetails;
                quotationresult.TravelMembers = traveldetails.Result.TravelMembers;
                quotationresult.InsuredDetails = traveldetails.Result.InsuredDetails;
                BindQuotationDetails(quotationresult);
            }
        }
        public void BindQuotationDetails(TravelSavedQuotationResponse results)
        {
            ddlPackage.SelectedValue = results.TravelInsurancePolicyDetails.SubClass == "FAMIL" ? "FM001" : "IN001";


            DataTable dependentdt = new DataTable();
            dependentdt.Columns.Add("ItemSerialNo", typeof(Int32));
            dependentdt.Columns.Add("ITEMNAME", typeof(string));
            dependentdt.Columns.Add("DATEOFBIRTH", typeof(string));
            dependentdt.Columns.Add("SEX", typeof(string));
            dependentdt.Columns.Add("CPR", typeof(string));
            dependentdt.Columns.Add("PASSPORT", typeof(string));
            dependentdt.Columns.Add("MAKE", typeof(string));
            dependentdt.Columns.Add("CATEGORY", typeof(string));
            dependentdt.Columns.Add("OCCUPATIONCODE", typeof(string));
            dependentdt.Columns.Add("Count", typeof(Int32));
            dependentdt.Columns.Add("AGE", typeof(Int32));

            foreach (var item in results.TravelMembers)
            {
                dependentdt.Rows.Add(item.ItemSerialNo, item.ItemName, item.DateOfBirth.ConvertToLocalFormat(), item.Sex.Trim(), item.CPR, item.Passport, item.Make,
                    item.Category, item.OccupationCode, item.ItemSerialNo);
            }
            BindDepedentDropDown();
            DependentRepeater.DataSource = dependentdt;
            DependentRepeater.DataBind();

            ddlPolicyPeroidDetails.SelectedValue = results.TravelInsurancePolicyDetails.PeroidOfCoverCode;
            ddlCoverageType.SelectedValue = results.TravelInsurancePolicyDetails.CoverageType;
            txtCommencementDate.Text = results.TravelInsurancePolicyDetails.InsuranceStartDate.ConvertToLocalFormat();
            txtPasportNo.Text = results.TravelInsurancePolicyDetails.Passport;
            txtOccupation.Text = results.TravelInsurancePolicyDetails.Occupation;
            ddlphysicaldefect.SelectedValue = results.TravelInsurancePolicyDetails.PhysicalStateDescription != null ? "yes" : "no";
            if (ddlphysicaldefect.SelectedItem.Text == "Yes")
            {
                physdescDiv.Visible = true;
                txtphysicaldefectDesc.Text = results.TravelInsurancePolicyDetails.PhysicalStateDescription;
            }
            else
            {
                physdescDiv.Visible = false;
            }
            txtFFPNo.Text = results.TravelInsurancePolicyDetails.FFPNumber;
            txtPremium.Text = Convert.ToString(results.TravelInsurancePolicyDetails.PremiumAmount);
            txtDiscountPremium.Text = Convert.ToString(results.TravelInsurancePolicyDetails.DiscountAmount);

        }
        public void BindDropDown()
        {
            var service = master.GetService();
            //Todo
            //Need to change the page type
            var dropDown = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>(
            BKIC.SellingPoint.DTO.Constants.DropDownURI.GetPageDropDowns.Replace("{pageName}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.Travelnsurance));
            if (dropDown.StatusCode == 200 && dropDown.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDown.Result.dropdownresult);
                DataTable Packagedt = dropdownds.Tables["BK_TravelInsurancePackage"];
                DataTable Peroiddt = dropdownds.Tables["BK_TravelInsurancePeroid"];
                Nationalitydt = dropdownds.Tables["BK_Nationality"];
                Relationdt = dropdownds.Tables["BK_FamilyRelationShip"];
                Genderdt = dropdownds.Tables["BK_Gender"];
                DataTable travelCoveragedt = dropdownds.Tables["BK_TravelCoverage"];
                if (Packagedt.Rows.Count > 0)
                {
                    ddlPackage.DataValueField = "Code";
                    ddlPackage.DataTextField = "Name";
                    ddlPackage.DataSource = Packagedt;
                    ddlPackage.DataBind();
                    ddlPackage.Items.Insert(0, new ListItem("--Please Select--", ""));
                }

                if (Peroiddt.Rows.Count > 0)
                {
                    ddlPolicyPeroidDetails.DataValueField = "Code";
                    ddlPolicyPeroidDetails.DataTextField = "Name";
                    ddlPolicyPeroidDetails.DataSource = Peroiddt;
                    ddlPolicyPeroidDetails.DataBind();
                    ddlPolicyPeroidDetails.Items.Insert(0, new ListItem("--Please Select--", ""));
                }

                if (travelCoveragedt.Rows.Count > 0)
                {
                    ddlCoverageType.DataValueField = "Code";
                    ddlCoverageType.DataTextField = "CoverageType";
                    ddlCoverageType.DataSource = travelCoveragedt;
                    ddlCoverageType.DataBind();
                    ddlCoverageType.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
            }
        }


        protected void btnBack_Click(object sender, EventArgs e)
        {

        }

        protected void cvDOB_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //if (!string.IsNullOrEmpty(txtDateOfBirth.Value.ToString()) && master.CalculateAge(Convert.ToDateTime(txtDateOfBirth.Value)) > 18)
            //    args.IsValid = true;
            //else
            //    args.IsValid = false;
        }
        protected void ddlPackage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPackage.SelectedItem.Text == "Family")
            {
                int count = 1;
                Dependentdiv.Visible = true;
                DataTable dt = CreateDependents(count);
                BindDepedentDropDown();
                DependentRepeater.DataSource = dt;
                DependentRepeater.DataBind();
                btnAdd.Visible = true;
                btnRemove.Visible = false;
            }
            else
            {
                Dependentdiv.Visible = false;
            }
        }

        protected void ddlPhysicalDefects_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void DependentRepeater_ItemDataBound1(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                if (Nationalitydt != null && Nationalitydt.Rows.Count > 0)
                {
                    DropDownList ddl = (DropDownList)e.Item.FindControl("ddlNationalityDependent");
                    ddl.DataValueField = "Code";
                    ddl.DataTextField = "Description";
                    ddl.DataSource = Nationalitydt;
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("--Please Select--", ""));
                }

                if (Relationdt != null && Relationdt.Rows.Count > 0)
                {
                    DropDownList ddlRelationship = (DropDownList)e.Item.FindControl("ddlRelationship");
                    ddlRelationship.DataValueField = "Relationship";
                    ddlRelationship.DataTextField = "Relationship";
                    ddlRelationship.DataSource = Relationdt;
                    ddlRelationship.DataBind();
                    ddlRelationship.Items.Insert(0, new ListItem("--Please Select--", ""));
                }

                if (Genderdt != null && Genderdt.Rows.Count > 0)
                {
                    DropDownList ddlGender = (DropDownList)e.Item.FindControl("ddlDependentGender");
                    ddlGender.DataValueField = "Value";
                    ddlGender.DataTextField = "Text";
                    ddlGender.DataSource = Genderdt;
                    ddlGender.DataBind();
                    ddlGender.Items.Insert(0, new ListItem("--Please Select--", ""));
                }


                if (drv != null && !string.IsNullOrEmpty(drv.Row["MAKE"].ToString()))
                {
                    DropDownList ddlNationality = (DropDownList)e.Item.FindControl("ddlNationalityDependent");
                    ddlNationality.SelectedValue = Convert.ToString(drv.Row["MAKE"]);

                }
                if (drv != null && !string.IsNullOrEmpty(drv.Row["CATEGORY"].ToString()))
                {
                    DropDownList ddlRelationship = (DropDownList)e.Item.FindControl("ddlRelationship");
                    ddlRelationship.SelectedValue = Convert.ToString(drv.Row["CATEGORY"]);
                }
                if (drv != null && !string.IsNullOrEmpty(drv.Row["SEX"].ToString()))
                {
                    DropDownList ddlGender = (DropDownList)e.Item.FindControl("ddlDependentGender");
                    ddlGender.SelectedValue = Convert.ToString(drv.Row["SEX"]);
                }



            }
        }


        public DataTable CreateDependents(int count)
        {
            DataTable dependentdt = new DataTable();
            dependentdt.Columns.Add("ITEMNAME", typeof(string));
            dependentdt.Columns.Add("DATEOFBIRTH", typeof(string));
            dependentdt.Columns.Add("SEX", typeof(string));
            dependentdt.Columns.Add("CPR", typeof(string));
            dependentdt.Columns.Add("PASSPORT", typeof(string));
            dependentdt.Columns.Add("MAKE", typeof(string));
            dependentdt.Columns.Add("CATEGORY", typeof(string));
            dependentdt.Columns.Add("OCCUPATIONCODE", typeof(string));
            dependentdt.Columns.Add("Count", typeof(Int32));
            for (int i = 1; i <= count; i++)
            {
                dependentdt.Rows.Add("", "", "", "", "", "", "", "", i);
            }

            return dependentdt;

        }

        public DataTable GetFamilyDependentDetails()
        {

            string Name = string.Empty;
            string DOB = string.Empty;
            string Gender = string.Empty;
            string CPR = string.Empty;
            string Passport = string.Empty;
            string Nationality = string.Empty;
            string Relation = string.Empty;
            string Occupation = string.Empty;
            int Count = 0;
            int ItemSerialNo = 0;
            int Age = 0;

            DataTable dependentdt = new DataTable();
            dependentdt.Columns.Add("ItemSerialNo", typeof(Int32));
            dependentdt.Columns.Add("ITEMNAME", typeof(string));
            dependentdt.Columns.Add("DATEOFBIRTH", typeof(string));
            dependentdt.Columns.Add("SEX", typeof(string));
            dependentdt.Columns.Add("CPR", typeof(string));
            dependentdt.Columns.Add("PASSPORT", typeof(string));
            dependentdt.Columns.Add("MAKE", typeof(string));
            dependentdt.Columns.Add("CATEGORY", typeof(string));
            dependentdt.Columns.Add("OCCUPATIONCODE", typeof(string));
            dependentdt.Columns.Add("Count", typeof(Int32));
            dependentdt.Columns.Add("AGE", typeof(Int32));


            foreach (RepeaterItem i in DependentRepeater.Items)
            {
                ItemSerialNo = ItemSerialNo + 1;
                TextBox txtdependentName = (TextBox)i.FindControl("txtDependentName");
                TextBox txtDOB = (TextBox)i.FindControl("txtDependentDOB");
                DropDownList ddlGender = (DropDownList)i.FindControl("ddlDependentGender");
                TextBox txtCPR = (TextBox)i.FindControl("txtDependentCPR");
                TextBox txtDependentPassport = (TextBox)i.FindControl("txtDependentPassport");
                DropDownList ddlNationality = (DropDownList)i.FindControl("ddlNationalityDependent");
                DropDownList ddlRelationship = (DropDownList)i.FindControl("ddlRelationship");
                TextBox txtDependentOccupation = (TextBox)i.FindControl("txtDependentOccupation");
                Label lbldependentcount = (Label)i.FindControl("lbldependentcount");

                Name = !string.IsNullOrEmpty(txtdependentName.Text) ? txtdependentName.Text : "";
                DOB = txtDOB.Text;

                Gender = ddlGender.SelectedItem != null ? ddlGender.SelectedItem.Value : "";
                CPR = !string.IsNullOrEmpty(txtCPR.Text) ? txtCPR.Text : "";
                Passport = !string.IsNullOrEmpty(txtDependentPassport.Text) ? txtDependentPassport.Text : "";
                Nationality = ddlNationality.SelectedItem != null ? ddlNationality.SelectedItem.Value : "";
                Relation = ddlRelationship.SelectedItem != null ? ddlRelationship.SelectedItem.Value : "";
                Occupation = !string.IsNullOrEmpty(txtDependentOccupation.Text) ? txtDependentOccupation.Text : "";
                //Count = !string.IsNullOrEmpty(lbldependentcount.Text) ? Convert.ToInt32(lbldependentcount.Text.Substring(lbldependentcount.Text.Length - 1)) : 0;
                //Todo
                //Need to calculate the age.
                //Age = !string.IsNullOrEmpty(txtDOB.Text) ? methods.CalculateAge(txtDOB.Text.CovertToCustomDateTime()) : 0;
                Age = 0;

                dependentdt.Rows.Add(ItemSerialNo, Name, DOB, Gender, CPR, Passport, Nationality, Relation, Occupation, ItemSerialNo, Age);

            }
            return dependentdt;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataTable dependentdt = GetFamilyDependentDetails();
            int existingcount = dependentdt.Rows.Count;
            dependentdt.Rows.Add(existingcount + 1, "", "", "", "", "", "", "", "", existingcount + 1, 0);
            ViewState["Dependentdt"] = dependentdt;
            btnRemove.Visible = true;
            DependentRepeater.DataSource = dependentdt;
            DependentRepeater.DataBind();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["Dependentdt"];
            if (dt != null)
            {
                DataRow lastRow = dt.Rows[dt.Rows.Count - 1];
                dt.Rows.Remove(lastRow);
                DependentRepeater.DataSource = dt;
                DependentRepeater.DataBind();
                if (dt.Rows.Count <= 1)
                {
                    btnRemove.Visible = false;
                }

            }
        }


        protected void ddlPackageDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPackage.SelectedItem.Text == "Family")
            {
                int count = 1;
                DataTable dt = CreateDependents(count);
                BindDepedentDropDown();
                DependentRepeater.DataSource = dt;
                DependentRepeater.DataBind();
                btnAdd.Visible = true;
                btnRemove.Visible = false;
            }


        }


        public void BindDepedentDropDown()
        {

            var service = master.GetService();
            var dropDown = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>(
            BKIC.SellingPoint.DTO.Constants.DropDownURI.GetPageDropDowns.Replace("{pageName}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.Travelnsurance));
            if (dropDown.StatusCode == 200 && dropDown.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDown.Result.dropdownresult);
                Nationalitydt = dropdownds.Tables["BK_Nationality"];
                Relationdt = dropdownds.Tables["BK_FamilyRelationShip"];
                Genderdt = dropdownds.Tables["BK_Gender"];
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var travelInsurnacePolicy = new TravelInsurancePolicy();
            var travelmembers = new TravelMembers();

            //  travelInsurnacePolicy.DOB = Convert.ToDateTime(UserRegisteredForm.DOB);
            travelInsurnacePolicy.PackageCode = ddlPackage.SelectedItem.Value;
            travelInsurnacePolicy.PolicyPeroidYears = ddlPolicyPeroidDetails.SelectedItem.Value == "AN001" ? 1 : 2;
            // travelInsurnacePolicy.InsuredCode =;
            //  travelInsurnacePolicy.PremiumAmount = PremiumBeforeDiscount;
            travelInsurnacePolicy.InsuranceStartDate = txtCommencementDate.Text.CovertToCustomDateTime();
            travelInsurnacePolicy.MainClass = "MISC";
            travelInsurnacePolicy.Passport = txtPasportNo.Text;
            travelInsurnacePolicy.Occupation = txtOccupation.Text;
            travelInsurnacePolicy.PeroidOfCoverCode = ddlPolicyPeroidDetails.SelectedItem.Value;
            travelInsurnacePolicy.FFPNumber = txtFFPNo.Text;
            travelInsurnacePolicy.QuestionaireCode = "QST_STP_002";
            travelInsurnacePolicy.IsPhysicalDefect = ddlphysicaldefect.SelectedItem.Value;
            travelInsurnacePolicy.PhysicalStateDescription = ddlphysicaldefect.SelectedItem.Value == "Yes" ? txtphysicaldefectDesc.Text : "";
            travelInsurnacePolicy.CoverageType = ddlCoverageType.SelectedIndex != 0 ? ddlCoverageType.SelectedItem.Text : "";
            // travelInsurnacePolicy.CreatedBy = Convert.ToInt32(userInfo.UserId);
            List<TravelMembers> members = new List<TravelMembers>();
            if (ddlPackage.SelectedItem.Value == "FM001")
            {

                DataTable familydt = GetFamilyDependentDetails();

                members = (from DataRow row in familydt.Rows

                           select new TravelMembers
                           {
                               TravelID = 0,
                               DocumentNo = "",
                               ItemSerialNo = Convert.ToInt32(row["ItemSerialNo"]),
                               ItemName = Convert.ToString(row["ITEMNAME"]),
                               ForeignSumInsured = 0,
                               Category = Convert.ToString(row["CATEGORY"]),
                               Title = "",
                               Sex = Convert.ToString(row["SEX"]),
                               DateOfBirth = Convert.ToDateTime(row["DATEOFBIRTH"]),
                               Age = Convert.ToInt32(row["AGE"]),
                               Make = Convert.ToString(row["MAKE"]),
                               OccupationCode = Convert.ToString(row["OCCUPATIONCODE"]),
                               CPR = Convert.ToString(row["CPR"]),
                               Passport = Convert.ToString(row["PASSPORT"]),
                               FirstName = Convert.ToString(row["ITEMNAME"]),
                               MiddleName = "",
                               LastName = "",
                               // CreatedBy = Convert.ToInt32(userInfo.UserId),
                               CreatedDate = DateTime.Now,
                               UpdatedBy = 0,
                               UpdatedDate = DateTime.Now

                           }).ToList();

            }


        }

        protected void btnnback_Click(object sender, EventArgs e)
        {

        }
        protected void BtnUpdateInsurance_Click(object sender, EventArgs e)
        {
            var service = master.GetService();
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            var travelInsurnacePolicy = new TravelInsurancePolicy();
            var travelmembers = new TravelMembers();
            int travelID = Convert.ToInt32(Request.QueryString["Ref"]);
            string insuredCode = Request.QueryString["InsuredCode"];
            // travelInsurnacePolicy.DOB = Convert.ToDateTime(UserRegisteredForm.DOB);
            travelInsurnacePolicy.PackageCode = ddlPackage.SelectedItem.Value;
            travelInsurnacePolicy.PolicyPeroidYears = ddlPolicyPeroidDetails.SelectedItem.Value == "AN001" ? 1 : 2;
            travelInsurnacePolicy.InsuredCode = insuredCode;
            travelInsurnacePolicy.SumInsured = 0;
            travelInsurnacePolicy.PremiumAmount = Convert.ToDecimal(txtPremium.Text);
            travelInsurnacePolicy.InsuranceStartDate = txtCommencementDate.Text.CovertToCustomDateTime();
            travelInsurnacePolicy.MainClass = "MISC";
            travelInsurnacePolicy.Passport = txtPasportNo.Text;
            travelInsurnacePolicy.Renewal = 'N';
            travelInsurnacePolicy.Occupation = txtOccupation.Text;
            travelInsurnacePolicy.PeroidOfCoverCode = ddlPolicyPeroidDetails.SelectedItem.Value;
            travelInsurnacePolicy.DiscountAmount = Convert.ToDecimal(txtDiscountPremium.Text);
            travelInsurnacePolicy.FFPNumber = txtFFPNo.Text;
            travelInsurnacePolicy.QuestionaireCode = "QST_STP_002";
            travelInsurnacePolicy.IsPhysicalDefect = ddlphysicaldefect.SelectedItem.Value;
            travelInsurnacePolicy.PhysicalStateDescription = ddlphysicaldefect.SelectedItem.Value == "Yes" ? txtphysicaldefectDesc.Text : "";
            travelInsurnacePolicy.CreatedBy = 0;
            travelInsurnacePolicy.IsSaved = false;
            travelInsurnacePolicy.LoadAmount = !String.IsNullOrEmpty(txtLoadAmount.Text) ? Convert.ToDecimal(txtLoadAmount.Text) : 0;
            travelInsurnacePolicy.Discounted = !String.IsNullOrEmpty(txtDiscountAmount.Text) ? Convert.ToDecimal(txtDiscountAmount.Text) : 0;
            travelInsurnacePolicy.Remarks = txtRemark.Text;
            //  travelInsurnacePolicy.Code = ddlc;
            List<TravelMembers> members = new List<TravelMembers>();
            if (ddlPackage.SelectedItem.Value == "FM001")
            {

                DataTable familydt = GetFamilyDependentDetails();

                members = (from DataRow row in familydt.Rows

                           select new TravelMembers
                           {
                               TravelID = 0,
                               DocumentNo = "",
                               ItemSerialNo = Convert.ToInt32(row["ItemSerialNo"]),
                               ItemName = Convert.ToString(row["ITEMNAME"]),
                               ForeignSumInsured = 0,
                               Category = Convert.ToString(row["CATEGORY"]),
                               Title = "",
                               Sex = Convert.ToString(row["SEX"]),
                               DateOfBirth = Convert.ToDateTime(row["DATEOFBIRTH"]),
                               Age = Convert.ToInt32(row["AGE"]),
                               PremiumAmount = Convert.ToDecimal(txtPremium.Text),
                               Make = Convert.ToString(row["MAKE"]),
                               OccupationCode = Convert.ToString(row["OCCUPATIONCODE"]),
                               CPR = Convert.ToString(row["CPR"]),
                               Passport = Convert.ToString(row["PASSPORT"]),
                               FirstName = Convert.ToString(row["ITEMNAME"]),
                               MiddleName = "",
                               LastName = "",
                               //Todo
                               //Need to change the userid.
                               CreatedBy = Convert.ToInt32(1),
                               CreatedDate = DateTime.Now,
                               UpdatedBy = 0,
                               UpdatedDate = DateTime.Now

                           }).ToList();

            }
            else
            {
                DataTable individualdt = GetFamilyDependentDetails();
                members = (from DataRow row in individualdt.Rows

                           select new TravelMembers
                           {
                               TravelID = 0,
                               DocumentNo = "",
                               ItemSerialNo = Convert.ToInt32(row["ItemSerialNo"]),
                               ItemName = Convert.ToString(row["ITEMNAME"]),
                               SumInsured = 0,
                               ForeignSumInsured = 0,
                               Category = Convert.ToString(row["CATEGORY"]),
                               Title = "",
                               Sex = Convert.ToString(row["SEX"]),
                               DateOfBirth = Convert.ToDateTime(row["DATEOFBIRTH"]),
                               Age = Convert.ToInt32(row["AGE"]),
                               PremiumAmount = Convert.ToDecimal(txtDiscountPremium.Text),
                               Make = Convert.ToString(row["MAKE"]),
                               OccupationCode = Convert.ToString(row["OCCUPATIONCODE"]),
                               CPR = Convert.ToString(row["CPR"]),
                               Passport = Convert.ToString(row["PASSPORT"]),
                               FirstName = Convert.ToString(row["ITEMNAME"]),
                               MiddleName = "",
                               LastName = "",
                               //Todo
                               //Need to change the userid.
                               CreatedBy = Convert.ToInt32(1),
                               CreatedDate = DateTime.Now,
                               UpdatedBy = 0,
                               UpdatedDate = DateTime.Now

                           }).ToList();

            }



            var traveldetails = new BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateTravelDetailsRequest();
            travelInsurnacePolicy.TravelID = Convert.ToInt32(travelID);
            traveldetails.TravelInsurancePolicyDetails = travelInsurnacePolicy;
            traveldetails.TravelMembers = members;
            var updateData = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateTravelDetailsResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateTravelDetailsRequest>
                    (BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.UpdatePolicyDetails, traveldetails);

            if (updateData.StatusCode == 200 && updateData.Result.IsTransactionDone)
            {
                successmsg.Visible = true;

            }
            else
            {
                errormsg.Visible = false;
            }



        }
        protected void BtnBackToInsurance_Click(object sender, EventArgs e)
        {
           // master.RedirectToPreviousURl();

        }

        protected void txtLoadAmount_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPremium.Text = Convert.ToString(Convert.ToDecimal(txtDiscountPremium.Text) + Convert.ToDecimal(txtLoadAmount.Text));
        }

        protected void txtDiscountAmount_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPremium.Text = Convert.ToString(Convert.ToDecimal(txtDiscountPremium.Text) - Convert.ToDecimal(txtDiscountAmount.Text));
        }
    }
}