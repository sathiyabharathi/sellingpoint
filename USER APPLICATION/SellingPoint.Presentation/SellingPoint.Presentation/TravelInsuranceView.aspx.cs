﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class TravelInsuranceView : System.Web.UI.Page
    {
     //   CommonMethods methods = new CommonMethods();
        General master;
        public TravelInsuranceView()
        {
            master = Master as General;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!IsPostBack)
            {
                if (Request.QueryString["Ref"] != null && Request.QueryString["InsuredCode"] != null)
                {
                    string travelID = Convert.ToString(Request.QueryString["Ref"]);
                    string InsuredCode = Convert.ToString(Request.QueryString["InsuredCode"]);
                    BindDetails(travelID, InsuredCode);

                }
            }
        }

        public void BindDetails(string travelID, string insuredCode)
        {
            var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuotation.Replace("{travelQuotationId}", travelID);
            url = url.Replace("{userInsuredCode}", insuredCode);
            url = url.Replace("{type}", "Portal");
            var service = master.GetService();
            var traveldetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelSavedQuotationResponse>>(url);
            if (traveldetails.StatusCode == 200 && traveldetails.Result.IsTransactionDone == true)
            {
                var quotationresult = new TravelSavedQuotationResponse();
                quotationresult.TravelInsurancePolicyDetails = traveldetails.Result.TravelInsurancePolicyDetails;
                quotationresult.TravelMembers = traveldetails.Result.TravelMembers;
                quotationresult.InsuredDetails = traveldetails.Result.InsuredDetails;
                BindQuotationDetails(quotationresult);
            }
        }

        public void BindQuotationDetails(TravelSavedQuotationResponse results)
        {
            lblPolicyType.Text = results.TravelInsurancePolicyDetails.MainClass;
            lblPolicyNo.Text = results.TravelInsurancePolicyDetails.DocumentNumber;

            DataTable dependentdt = new DataTable();
            dependentdt.Columns.Add("ItemSerialNo", typeof(Int32));
            dependentdt.Columns.Add("ITEMNAME", typeof(string));
            dependentdt.Columns.Add("DATEOFBIRTH", typeof(string));
            dependentdt.Columns.Add("SEX", typeof(string));
            dependentdt.Columns.Add("CPR", typeof(string));
            dependentdt.Columns.Add("PASSPORT", typeof(string));
            dependentdt.Columns.Add("MAKE", typeof(string));
            dependentdt.Columns.Add("CATEGORY", typeof(string));
            dependentdt.Columns.Add("OCCUPATIONCODE", typeof(string));
            dependentdt.Columns.Add("Count", typeof(Int32));
            dependentdt.Columns.Add("AGE", typeof(Int32));

            foreach (var item in results.TravelMembers)
            {
                dependentdt.Rows.Add(item.ItemSerialNo, item.ItemName, item.DateOfBirth.ConvertToLocalFormat(), item.Sex, item.CPR, item.Passport, item.Make,
                    item.Category, item.OccupationCode, item.ItemSerialNo, item.Age);
            }

            rptDependentDiv.DataSource = dependentdt;
            rptDependentDiv.DataBind();

            lblPeroidOfCover.Text = results.TravelInsurancePolicyDetails.PeroidOfCoverCode;
            lblStartDate.Text = results.TravelInsurancePolicyDetails.InsuranceStartDate.ConvertToLocalFormat();
            lblPassportNo.Text = results.TravelInsurancePolicyDetails.Passport;
            // lbl.Text = results.TravelInsurancePolicyDetails.Occupation;
            lblPhysicalDefect.Text = !string.IsNullOrEmpty(results.TravelInsurancePolicyDetails.PhysicalStateDescription) ? "yes" : "no";
            if (lblPhysicalDefect.Text == "yes")
            {
                PhysicalDescDiv.Visible = true;
                lblPhysicalDesc.Text = results.TravelInsurancePolicyDetails.PhysicalStateDescription;
            }
            else
            {
                PhysicalDescDiv.Visible = false;
            }
            lblFFPNumber.Text = results.TravelInsurancePolicyDetails.FFPNumber;
            lblTotalPremium.Text = Convert.ToString(results.TravelInsurancePolicyDetails.PremiumAmount);
            lblDiscountPremium.Text = Convert.ToString(results.TravelInsurancePolicyDetails.DiscountAmount);
            lblCPR.Text = results.TravelInsurancePolicyDetails.CPR;
            lblInsuredName.Text = results.TravelInsurancePolicyDetails.InsuredName;
            lblExpiryDate.Text = results.TravelInsurancePolicyDetails.ExpiryDate.ConvertToLocalFormat();
            lblMainClass.Text = results.TravelInsurancePolicyDetails.MainClass;
            lblSumInsured.Text = Convert.ToInt32(results.TravelInsurancePolicyDetails.SumInsured).ToString();
            lblNationality.Text = results.InsuredDetails.UserInfo.Nationality;
            lblCoverage.Text = results.TravelInsurancePolicyDetails.CoverageType;

            if (!results.TravelInsurancePolicyDetails.ISHIR)
            {
                downloadschedule.Visible = true;
                string url = ClientUtility.WebApiUri + BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.downloadschedule.Replace("{insuranceType}", Constants.Travel)
                            .Replace("{insuredCode}", results.TravelInsurancePolicyDetails.InsuredCode).Replace("{RefID}", Convert.ToString(results.TravelInsurancePolicyDetails.TravelID));

                downloadschedule.HRef = url;
            }
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
           // master.RedirectToPreviousURl();
        }
        public static string ConvertTodatestring(DateTime dt)
        {
            if (dt != null)
            {
                return dt.ToString("dd/MM/yyyy");
            }

            return "";
        }
    }
}