﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using KBIC.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class TravelEndorsement : System.Web.UI.Page
    {
        public static DataTable Genderdt;
        public static DataTable Nationalitydt;
        public static List<BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyTravelPolicy> policyList;
        public static DataTable Relationdt;
        private General master;

        public TravelEndorsement()
        {
            master = Master as General;
        }

        public static long _TravelEndorsementID { get; set; }
        public static bool AjdustedPremium { get; set; }
        public static List<BKIC.SellingPoint.DTO.RequestResponseWrappers.InsuredMasterDetails> InsuredNames { get; set; }
        public static string MainClass { get; set; }
        public static string SubClass { get; set; }
        public static decimal OriginalPremium { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;

            if (!Page.IsPostBack)
            {
                depentdetails.Visible = false;
                CorrectPremiumDiv.Visible = false;
                SetInitialRow();
                BindAgencyClientCodeDropdown();
            }
        }
        public void CalculateTravelEndorsementQuote(bool showPremium)
        {
            master.ShowLoading = true;
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();


            if(ddlEndorsementType.SelectedItem.Value == "ChangeMemberDetails" || ddlEndorsementType.SelectedItem.Value == "AddRemoveFamilyMember")
            {
                ShowPremium(userInfo, 0, 0);
            }
            else
            {
                var travelEndorementQuote = new BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementQuote();
                travelEndorementQuote.Agency = userInfo.Agency;
                travelEndorementQuote.AgentCode = userInfo.AgentCode;

                // SetEndorsementType(travelEndorementQuote);

                var travelEndorsementQuoteResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementQuoteResponse>,
                                                   BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementQuote>
                                                   (BKIC.SellingPoint.DTO.Constants.TravelEndorsementURI.GetTravelEndorsementQuote,
                                                    travelEndorementQuote);

                if (travelEndorsementQuoteResult.StatusCode == 200 && travelEndorsementQuoteResult.Result.IsTransactionDone)
                {
                    var endoresementPremium = travelEndorsementQuoteResult.Result.EndorsementPremium;
                    if (travelEndorementQuote.EndorsementType == "CancelPolicy")
                    {
                        endoresementPremium = travelEndorsementQuoteResult.Result.RefundPremium;
                    }
                    calculatedPremium.Value = endoresementPremium.ToString();

                    var commisionRequest = new BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionRequest();
                    commisionRequest.AgentCode = userInfo.AgentCode;
                    commisionRequest.Agency = userInfo.Agency;
                    commisionRequest.SubClass = subClass.Value;
                    commisionRequest.PremiumAmount = endoresementPremium;

                    var commissionresult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionResponse>,
                                     BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionRequest>("api/insurance/Commission", commisionRequest);

                    if (commissionresult.StatusCode == 200 && commissionresult.Result.CommissionAmount > 0)
                    {
                        //commission.Text = Convert.ToString(commissionresult.Result.CommissionAmount);
                        calculatedCommision.Value = Convert.ToString(commissionresult.Result.CommissionAmount);
                        if (showPremium)
                        {
                            ShowPremium(userInfo, endoresementPremium, commissionresult.Result.CommissionAmount);
                        }
                    }
                    //txtExcessAmount.Text =  GetExcess().ToString();
                    //amtDisplay.Visible = true;
                }
            }     
            
        }

        public void ShowPremium(OAuthTokenResponse userInfo, decimal Premium, decimal Commission)
        {
            amtDisplay.Visible = true;
            if (userInfo.Roles == "SuperAdmin" || userInfo.Roles == "BranchAdmin")
            {
                premiumAmount.Text = Convert.ToString(Premium);
                commission.Text = Convert.ToString(Commission);
                includeDisc.Visible = true;
            }
            else
            {
                premiumAmount1.Text = Convert.ToString(Premium);
                commission1.Text = Convert.ToString(Commission);
                excludeDisc.Visible = true;
            }
        }

        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Homepage.aspx");
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                Page_CustomValidate();
                if (Page.IsValid)
                {
                    SaveAuthorize(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                Page_CustomValidate();
                if (Page.IsValid)
                {
                    if (ddlTravelPolicies.SelectedIndex > 0)
                    {
                        CalculateTravelEndorsementQuote(true);
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void Changed_CPR(object sender, EventArgs e)
        {
            try
            {
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();

                var travelreq = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyTravelRequest();
                travelreq.AgentCode = userInfo.AgentCode;
                travelreq.Agency = userInfo.Agency;
                travelreq.AgentBranch = userInfo.AgentBranch;
                travelreq.CPR = ddlCPR.SelectedIndex > 0 ? ddlCPR.SelectedItem.Text.Trim() : string.Empty;
                travelreq.Type = Constants.Travel;

                //Get PolicyNo by Agency
                var travelPolicies = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                    <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyTravelPolicyResponse>,
                                    BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyTravelRequest>
                                    (BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetTravelPoliciesByCPR, travelreq);

                if (travelPolicies.StatusCode == 200 && travelPolicies.Result.AgencyTravelPolicies.Count > 0)
                {
                    policyList = travelPolicies.Result.AgencyTravelPolicies;

                    ddlTravelPolicies.DataSource = travelPolicies.Result.AgencyTravelPolicies;
                    ddlTravelPolicies.DataTextField = "DOCUMENTNO";
                    ddlTravelPolicies.DataValueField = "DOCUMENTNO";
                    ddlTravelPolicies.DataBind();
                    ddlTravelPolicies.Items.Insert(0, new ListItem("--Please Select--", "none"));
                }
                Page_CustomValidate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void Changed_EndorsementType(object sender, EventArgs e)
        {
            if (ddlTravelPolicies.SelectedIndex > 0 && ddlEndorsementType.SelectedIndex > 0)
            {
                SetTravelEndorsement();
                depentdetails.Visible = true;
            }
            else
            {
                depentdetails.Visible = false;
            }
        }

        public void SetTravelEndorsement()
        {
            if (ddlEndorsementType.SelectedItem.Value == "ChangeMemberDetails")
            {
                CorrectPremiumDiv.Visible = false;
                txtEffectiveToDate.Enabled = false;
                EnableGrid(true);
            }
            else if (ddlEndorsementType.SelectedItem.Value == "AddRemoveFamilyMember")
            {
                CorrectPremiumDiv.Visible = false;
                txtEffectiveToDate.Enabled = false;
                EnableGrid(false);               
            }        
            else if(ddlEndorsementType.SelectedItem.Value == "CorrectPremium")
            {
                CorrectPremiumDiv.Visible = true;
                changeAddMemberDeatilsDiv.Visible = false;
                txtEffectiveToDate.Enabled = false;
                txtOldPremium.Text = OriginalPremium.ToString();
            }
            else if(ddlEndorsementType.SelectedItem.Value == "CancelPolicy")
            {
                CorrectPremiumDiv.Visible = false;
                changeAddMemberDeatilsDiv.Visible = false;
                txtEffectiveToDate.Enabled = true;
            }
        }

        protected void Changed_TravelPolicy(object sender, EventArgs e)
        {
            try
            {
                if (ddlEndorsementType.SelectedIndex > 0 && ddlTravelPolicies.SelectedIndex > 0)
                {
                    GetPolicyInfo();
                    SetTravelEndorsement();
                    depentdetails.Visible = true;
                }
                else
                {
                    depentdetails.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        public void GetPolicyInfo()
        {
            master.ShowLoading = true;

            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();        
            
            var request = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyTravelRequest();
            request.AgentBranch = userInfo.AgentBranch;
            request.AgentCode = userInfo.AgentCode;
            request.Agency = userInfo.Agency;
            
            ListEndorsements(service, userInfo);

            //Get saved policy details by document(policy) number.
            var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuoteDocumentNo.
                          Replace("{documentNo}", ddlTravelPolicies.SelectedItem.Text.Trim())
                          .Replace("{agentCode}", request.AgentCode);

            var travelDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelSavedQuotationResponse>>(url);

            //Update policy details on current page for dispaly the details.
            if (travelDetails.StatusCode == 200 && travelDetails.Result.IsTransactionDone)
            {
                var res = travelDetails.Result.TravelInsurancePolicyDetails;
                OriginalPremium = travelDetails.Result.TravelInsurancePolicyDetails.PremiumAfterDiscount;
                txtEffectiveFromDate.Text = travelDetails.Result.TravelInsurancePolicyDetails.InsuranceStartDate.ConvertToLocalFormat();
                txtEffectiveToDate.Text = travelDetails.Result.TravelInsurancePolicyDetails.ExpiryDate.ConvertToLocalFormat();

                if (travelDetails.Result.TravelMembers != null && 
                   travelDetails.Result.TravelMembers.Count > 0 &&
                   res.PackageName.ToLower() == "family")
                {
                    if (ViewState["CurrentTable"] != null)
                    {
                        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                        DataRow drCurrentRow = null;
                        Gridview1.DataSource = null;
                        if (dtCurrentTable.Rows.Count > 0)
                        {
                            for (int i = dtCurrentTable.Rows.Count; i > 0; i--)
                            {
                                dtCurrentTable.Rows[i - 1].Delete();
                                dtCurrentTable.AcceptChanges();
                            }
                            for (int i = 1; i <= travelDetails.Result.TravelMembers.Count; i++)
                            {
                                if (travelDetails.Result.TravelMembers[i - 1].ItemSerialNo != 1)
                                {
                                    drCurrentRow = dtCurrentTable.NewRow();
                                    dtCurrentTable.Rows.Add(drCurrentRow);
                                    dtCurrentTable.Rows[i - 1]["Insured Name"] = travelDetails.Result.TravelMembers[i - 1].ItemName;
                                    dtCurrentTable.Rows[i - 1]["Relationship"] = travelDetails.Result.TravelMembers[i - 1].Category;
                                    dtCurrentTable.Rows[i - 1]["CPR"] = travelDetails.Result.TravelMembers[i - 1].CPR;
                                    dtCurrentTable.Rows[i - 1]["Date Of Birth"] = travelDetails.Result.TravelMembers[i - 1].DateOfBirth.ConvertToLocalFormat();
                                    dtCurrentTable.Rows[i - 1]["Passport No"] = travelDetails.Result.TravelMembers[i - 1].Passport;
                                    dtCurrentTable.Rows[i - 1]["Nationality"] = travelDetails.Result.TravelMembers[i - 1].Make;
                                    dtCurrentTable.Rows[i - 1]["Occupation"] = travelDetails.Result.TravelMembers[i - 1].OccupationCode;
                                }
                            }
                            ViewState["CurrentTable"] = dtCurrentTable;
                            Gridview1.DataSource = dtCurrentTable;
                            Gridview1.DataBind();
                            depentdetails.Visible = true;
                            SetPreviousData();
                        }
                    }
                }
                Page_CustomValidate();
            }
        }

        protected void ddlNational_SelectedIndexChanged(object sender, EventArgs e)
        {
            int a = 10;
        }

        protected void ddlPaymentMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentMethods.SelectedIndex == 1)
            {
                //txtAccountNumber.Text = "";
                //txtAccountNumber.Enabled = false;
            }
            else
            {
                //txtAccountNumber.Enabled = true;
            }
            Page_CustomValidate();
        }

        protected void ddlRelation_Changed(object sender, EventArgs e)
        {
            //if (ddlRelation.SelectedIndex == 2)
            //{
            //    txtDOB.CssClass.Replace("blow20years", "dateofbirth");
            //}
            //else
            //{
            //    txtDOB.CssClass.Replace("dateofbirth", "blow20years");

            //}
            Page_CustomValidate();
        }

        protected void Gridview1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (Nationalitydt != null && Nationalitydt.Rows.Count > 0)
                {
                    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlNational");
                    ddl.DataValueField = "Code";
                    ddl.DataTextField = "Description";
                    ddl.DataSource = Nationalitydt;
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("--Please Select--", ""));
                }

                if (Relationdt != null && Relationdt.Rows.Count > 0)
                {
                    DropDownList ddlRelationship = (DropDownList)e.Row.FindControl("ddlRelation");
                    ddlRelationship.DataValueField = "Relationship";
                    ddlRelationship.DataTextField = "Relationship";
                    ddlRelationship.DataSource = Relationdt;
                    ddlRelationship.DataBind();
                    ddlRelationship.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dlist.DefaultView.Sort = e.SortExpression + " " + SortDir(e.SortExpression);
            //gvMotorInsurance.DataSource = dlist;
            //gvMotorInsurance.DataBind();
        }

        protected void gvTravelEndorsement_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvTravelEndorsement.Rows)
            {
                bool IsSaved = Convert.ToBoolean((row.FindControl("lblIsSaved") as Label).Text.Trim());
                bool IsActive = Convert.ToBoolean((row.FindControl("lblIsActive") as Label).Text.Trim());

                if (IsActive)
                {
                    var btnAuthorize = row.FindControl("lnkbtnAuthorize") as LinkButton;
                    btnAuthorize.Visible = false;

                    var btnDelete = row.FindControl("lnkbtnDelete") as LinkButton;
                    btnDelete.Visible = false;
                }
            }
        }

        protected void lnkbtnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void lnkbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void lnkbtnMemberDelete_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void Gridview1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["CurrentTable"] as DataTable;
            dt.Rows[index].Delete();
            ViewState["CurrentTable"] = dt;
            Gridview1.DataSource = dt;
            Gridview1.DataBind();
            SetPreviousData();
        }



        protected void validate_Premium(object sender, EventArgs e)
        {
            var Premium = Convert.ToDecimal(calculatedPremium.Value);
            var Commision = Convert.ToDecimal(calculatedCommision.Value);
            decimal Discount = string.IsNullOrEmpty(txtDiscount.Text) ? decimal.Zero : Convert.ToDecimal(txtDiscount.Text);
            var reduceablePremium = Premium - Commision;
            var premiumDiff = Premium - Discount;

            if (premiumDiff < reduceablePremium)
            {
                premiumAmount.Text = Convert.ToString(reduceablePremium);
                txtDiscount.Text = Convert.ToString(calculatedCommision.Value);
                commission.Text = Convert.ToString(0);
            }
            else if (Discount > Premium)
            {
                premiumAmount.Text = Convert.ToString(reduceablePremium);
                txtDiscount.Text = Convert.ToString(calculatedCommision.Value);
                commission.Text = Convert.ToString(0);
            }
            else
            {
                premiumAmount.Text = Convert.ToString(premiumDiff);
                commission.Text = Convert.ToString(Commision - Discount);
                btnSubmit.Enabled = true;
            }
        }

        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox txName = (TextBox)Gridview1.Rows[rowIndex].Cells[0].FindControl("txtMemberName");
                        DropDownList ddlRelation = (DropDownList)Gridview1.Rows[rowIndex].Cells[1].FindControl("ddlRelation");
                        TextBox txOccupation = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtOccupation");
                        TextBox txDOB = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtDOB");
                        TextBox txPassport = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtPassport");
                        DropDownList ddlNation = (DropDownList)Gridview1.Rows[rowIndex].Cells[5].FindControl("ddlNational");
                        //TextBox txOccupation = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtOccupation");

                        drCurrentRow = dtCurrentTable.NewRow();

                        dtCurrentTable.Rows[i - 1]["Insured Name"] = txName.Text;
                        dtCurrentTable.Rows[i - 1]["Relationship"] = ddlRelation.SelectedItem.Text;
                        //dtCurrentTable.Rows[i - 1]["CPR"] = txCPR.Text;
                        dtCurrentTable.Rows[i - 1]["Date Of Birth"] = txDOB.Text;
                        dtCurrentTable.Rows[i - 1]["Passport No"] = txPassport.Text;
                        dtCurrentTable.Rows[i - 1]["Nationality"] = ddlNation.SelectedItem.Text;
                        dtCurrentTable.Rows[i - 1]["Occupation"] = txOccupation.Text;

                        rowIndex++;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;
                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        private void BindAgencyClientCodeDropdown()
        {
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;

            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();
            var dropDownResult = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                 <BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>
                                (BKIC.SellingPont.DTO.Constants.DropdownURI.GetPageDropDowns.Replace("{type}", 
                                BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.Travelnsurance));

                if (dropDownResult.StatusCode == 200 && dropDownResult.Result.IsTransactionDone == true)
                {
                    DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDownResult.Result.dropdownresult);
                    Nationalitydt = dropdownds.Tables["Nationality"];
                    Relationdt = dropdownds.Tables["FamilyRelationShip"];
                    DataTable paymentTypes = dropdownds.Tables["PaymentType"];
                    DataTable branches = dropdownds.Tables["BranchMaster"];

                    if (branches != null && branches.Rows.Count > 0)
                    {
                        ddlBranch.DataValueField = "AGENTBRANCH";
                        ddlBranch.DataTextField = "BranchName";
                        ddlBranch.DataSource = branches;
                        ddlBranch.DataBind();
                        ddlBranch.Items.Insert(0, new ListItem("--Please Select--", ""));
                    }
                }

                var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest();
                req.AgentBranch = userInfo.AgentBranch;
                req.AgentCode = userInfo.AgentCode;

                var insuredResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredResponse>,
                                   BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest>
                                   (BKIC.SellingPoint.DTO.Constants.AdminURI.GetAgencyInsured, req);

                if (insuredResult.StatusCode == 200 && insuredResult.Result.AgencyInsured.Count > 0)
                {
                    ddlCPR.DataSource = insuredResult.Result.AgencyInsured;
                    ddlCPR.DataTextField = "CPR";
                    ddlCPR.DataValueField = "InsuredCode";
                    ddlCPR.DataBind();
                    ddlCPR.Items.Insert(0, new ListItem("--Please Select--", ""));
                    InsuredNames = insuredResult.Result.AgencyInsured;
                }
                txtIndroducedBy.Text = userInfo.UserName;
                ddlBranch.SelectedIndex = ddlBranch.Items.IndexOf(ddlBranch.Items.FindByValue(userInfo.AgentBranch));               
                txtIssueDate.Text = DateTime.Now.CovertToLocalFormat();
        }
        private void ListEndorsements(DataServiceManager service, OAuthTokenResponse userInfo)
        {
            if (ddlTravelPolicies.SelectedIndex > 0)
            {
                var travelEndoRequest = new BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndoRequest();
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    travelEndoRequest.Agency = userInfo.Agency;
                    travelEndoRequest.AgentCode = userInfo.AgentCode;
                    travelEndoRequest.InsuranceType = Constants.Travel;
                    travelEndoRequest.DocumentNo = ddlTravelPolicies.SelectedItem.Text.Trim();

                    var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                 <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndoResponse>,
                                 BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndoRequest>
                                 (BKIC.SellingPoint.DTO.Constants.TravelEndorsementURI.GetAllEndorsements, travelEndoRequest);

                    if (result.StatusCode == 200 && result.Result.IsTransactionDone)
                    {
                        gvTravelEndorsement.DataSource = result.Result.TravelEndorsements;
                        gvTravelEndorsement.DataBind();

                        if (result.Result.TravelEndorsements.Count > 0)
                        {
                            _TravelEndorsementID = result.Result.TravelEndorsements[result.Result.TravelEndorsements.Count - 1].TravelEndorsementID;
                        }
                        else
                        {
                            _TravelEndorsementID = 0;
                        }
                    }
                }
            }
        } 

        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("Insured Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Relationship", typeof(string)));
            dt.Columns.Add(new DataColumn("CPR", typeof(string)));
            dt.Columns.Add(new DataColumn("Date Of Birth", typeof(string)));

            dt.Columns.Add(new DataColumn("Passport No", typeof(string)));
            dt.Columns.Add(new DataColumn("Nationality", typeof(string)));
            dt.Columns.Add(new DataColumn("Occupation", typeof(string)));

            dr = dt.NewRow();

            dr["Insured Name"] = string.Empty;
            dr["Relationship"] = string.Empty;
            dr["CPR"] = string.Empty;
            dr["Date Of Birth"] = string.Empty;
            dr["Passport No"] = string.Empty;
            dr["Nationality"] = string.Empty;
            dr["Occupation"] = string.Empty;

            dt.Rows.Add(dr);

            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            Gridview1.DataSource = dt;
            Gridview1.DataBind();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox txName = (TextBox)Gridview1.Rows[rowIndex].Cells[0].FindControl("txtMemberName");
                        DropDownList ddlRelation = (DropDownList)Gridview1.Rows[rowIndex].Cells[1].FindControl("ddlRelation");
                        TextBox txtOccupation = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtOccupation");
                        TextBox txDOB = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtDOB");
                        TextBox txPassport = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtPassport");
                        DropDownList ddlNation = (DropDownList)Gridview1.Rows[rowIndex].Cells[5].FindControl("ddlNational");
                        // DropDownList ddlOccupation = (DropDownList)Gridview1.Rows[rowIndex].Cells[6].FindControl("ddlTravelOccupation");

                        txName.Text = dt.Rows[i]["Insured Name"].ToString();
                        ddlRelation.SelectedIndex = ddlRelation.Items.IndexOf(ddlRelation.Items.FindByText(dt.Rows[i]["Relationship"].ToString()));
                        //txCPR.Text=dt.Rows[i]["CPR"].ToString();
                        txDOB.Text = dt.Rows[i]["Date Of Birth"].ToString();
                        txPassport.Text = dt.Rows[i]["Passport No"].ToString();
                        ddlNation.SelectedIndex = ddlNation.Items.IndexOf(ddlNation.Items.FindByValue(dt.Rows[i]["Nationality"].ToString()));
                        txtOccupation.Text = dt.Rows[i]["Occupation"].ToString();
                        //  ddlOccupation.SelectedIndex = ddlOccupation.Items.IndexOf(ddlOccupation.Items.FindByText(dt.Rows[i]["Occupation"].ToString()));

                        rowIndex++;
                    }
                }
            }
        }

        private void EnableGrid(bool isEnable)
        {
            for (int row = 1; row <= Gridview1.Rows.Count; row++)
            {
                //DataRow TempRow = TempTable.NewRow();
                var obj = new TravelMembers();
                obj.ItemSerialNo = row + 1;
                obj.ForeignSumInsured = 50000;
                obj.SumInsured = 18900;

                for (int col = 0; col < Gridview1.Columns.Count; col++)
                {
                    if (Gridview1.Columns[col].Visible)
                    {
                        if (String.IsNullOrEmpty(Gridview1.Rows[row - 1].Cells[col].Text))
                        {
                            if (Gridview1.Rows[row - 1].Cells[col].Controls[1].GetType().ToString().Contains("Label"))
                            {
                                ((Label)Gridview1.Rows[row - 1].Cells[col].Controls[1]).Enabled = isEnable;
                            }
                            else if (Gridview1.Rows[row - 1].Cells[col].Controls[1].GetType().ToString().Contains("LinkButton"))
                            {
                                ((LinkButton)Gridview1.Rows[row - 1].Cells[col].Controls[1]).Visible = !isEnable;
                            }
                            else if (Gridview1.Rows[row - 1].Cells[col].Controls[1].GetType().ToString().Contains("TextBox"))
                            {
                                ((TextBox)Gridview1.Rows[row - 1].Cells[col].Controls[1]).Enabled = isEnable;
                            }
                            else if (Gridview1.Rows[row - 1].Cells[col].Controls[1].GetType().ToString().Contains("DropDownList"))
                            {
                                ((DropDownList)Gridview1.Rows[row - 1].Cells[col].Controls[1]).Enabled = isEnable;
                            }
                        }
                    }
                }
            }
            if (!isEnable)
            {
                // ButtonAdd.Visible = false;
            }
            else
            {
                // ButtonAdd.Visible = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isSave"></param>
        public void SaveAuthorize(bool isSave)
        {
            try
            {
                var postTravelEndorsement = new BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsement();
                var service = new DataServiceManager(ClientUtility.WebApiUri, "", false);
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    postTravelEndorsement.Agency = userInfo.Agency;
                    postTravelEndorsement.AgencyCode = userInfo.AgentCode;
                }
                if (isSave)
                {
                    postTravelEndorsement.IsSaved = true;
                    postTravelEndorsement.IsActivePolicy = false;
                }
                else
                {
                    postTravelEndorsement.IsSaved = false;
                    postTravelEndorsement.IsActivePolicy = true;
                }
                if (isSave /*&& EndorsementPrecheck()*/)
                {
                    modalBodyText.InnerText = "Your travel policy already have saved endorsement";
                    btnYes.Visible = false;
                    btnOK.Text = "OK";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowPopup();", true);
                    return;
                }
                postTravelEndorsement.PremiumAmount = string.IsNullOrEmpty(paidPremium.Value) ? decimal.Zero : Convert.ToDecimal(paidPremium.Value);


                //Get saved policy details by document(policy) number.
                var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuoteDocumentNo.
                          Replace("{documentNo}", ddlTravelPolicies.SelectedItem.Text.Trim()).Replace("{agentCode}", userInfo.AgentCode);

                var motorDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                  <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelSavedQuotationResponse>>(url);

                //Update policy details on current page for dispaly the details.
                if (motorDetails.StatusCode == 200 && motorDetails.Result.IsTransactionDone)
                {
                    var response = motorDetails.Result.TravelInsurancePolicyDetails;
                    //SetEndorsementType(postTravelEndorsement, response);
                }
                postTravelEndorsement.CreatedBy = Convert.ToInt32(userInfo.ID);

                var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                             <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementResponse>, 
                             BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsement>
                            (BKIC.SellingPoint.DTO.Constants.TravelEndorsementURI.PostTravelEndorsement, postTravelEndorsement);

                if (result.Result != null && result.StatusCode == 200 && result.Result.IsTransactionDone)
                {
                    _TravelEndorsementID = result.Result.TravelEndorsementID;
                    ListEndorsements(service, userInfo);
                    if (result.Result.IsHIR)
                    {
                        //lblSavedMessage.Text = "Your motor endorsement saved and moved into HIR";

                        modalBodyText.InnerText = "Your travel endorsement saved and moved into HIR :" + result.Result.EndorsementNo;
                        btnYes.Visible = false;
                        btnOK.Text = "OK";
                    }
                    else
                    {
                        modalBodyText.InnerText = "Your travel endorsement has been saved sucessfully :" + result.Result.EndorsementNo;
                        btnYes.Visible = false;
                        btnOK.Text = "OK";
                    }

                    //string sourceDateText = txtEffectiveToDate.Text;
                    //DateTime sourceDate = DateTime.ParseExact(sourceDateText, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                    //string formatted = sourceDate.ToString("yyyy-MM-dd");

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowPopup();", true);
                    //SetScheduleHRef(ddlMotorPolicies.SelectedItem.Text.Trim(), Constants.Motor, userInfo, ddlEndorsementType.SelectedItem.Text.Trim(),
                    //                string.IsNullOrEmpty(txtOldClientCode.Text.Trim()) ? "ClientCode" : txtOldClientCode.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewClientCode.Text.Trim()) ? "ClientCode" : txtNewClientCode.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewInsuredName.Text.Trim()) ? "ClientCode" : txtNewInsuredName.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewChassesNo.Text.Trim()) ? "ClientCode" : txtNewChassesNo.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewRegistrationNo.Text.Trim()) ? "ClientCode" : txtNewRegistrationNo.Text.Trim(),
                    //                string.IsNullOrEmpty(txtEffectiveToDate.Text.Trim()) ? "ClientCode" : formatted.Trim());

                    //SetCertificateHRef(ddlMotorPolicies.SelectedItem.Text.Trim(), "Portal", userInfo, ddlEndorsementType.SelectedItem.Text.Trim(),
                    //                string.IsNullOrEmpty(txtOldClientCode.Text.Trim()) ? "ClientCode" : txtOldClientCode.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewClientCode.Text.Trim()) ? "ClientCode" : txtNewClientCode.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewInsuredName.Text.Trim()) ? "ClientCode" : txtNewInsuredName.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewChassesNo.Text.Trim()) ? "ClientCode" : txtNewChassesNo.Text.Trim(),
                    //                string.IsNullOrEmpty(txtNewRegistrationNo.Text.Trim()) ? "ClientCode" : txtNewRegistrationNo.Text.Trim(),
                    //                string.IsNullOrEmpty(txtEffectiveToDate.Text.Trim()) ? "ClientCode" : formatted.Trim());
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }
        public void Page_CustomValidate()
        {
            if (endorsementSubmitted.Value == "true")
            {
                Validate("TravelEndorsementValidation");
            }
        }
        public void Reset_Content(object sender, EventArgs e)
        {
            modalBodyText.InnerText = "Your you sure want authorize this endorsement ?";
            btnOK.Text = "No";
            btnYes.Visible = true;
        }
    }
}