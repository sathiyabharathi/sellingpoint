﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General.Master" CodeBehind="HomePage.aspx.cs" Inherits="SellingPoint.Presentation.HomePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //custom-theme -->
    <link href="assets/css/component.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/export.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/flipclock.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/circles.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/style_grid.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />

    <!-- page custom main styles -->
    <link rel="stylesheet" href="assets/css/main-style.css" />
    <link rel="stylesheet" href="assets/css/chart.css" />

    <!-- font-awesome-icons -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="homePagePanel">
        <ContentTemplate>

            <!-- banner -->
            <div class="wthree_agile_admin_info">

                <div class="clearfix"></div>
                <!-- //w3_agileits_top_nav-->
                <!-- /inner_content-->
                <div class="dashboard_content">
                    <!-- /inner_content_w3_agile_info-->
                    <div class="dashboard_content_w3_agile_info">
                        <!-- /agile_top_w3_grids-->
                        <div class="agile_top_w3_grids">
                            <!------ breadcrumb ----->
                            <div class="row page-titles">
                                <div class="col-md-5 align-left">
                                    <h3>Dashboard</h3>
                                </div>
                                <div class="col-md-7 align-right">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">Dashboard</li>
                                    </ol>
                                </div>
                            </div>
                            <!------ End breadcrumb ----->
                            <!------ insurance grid ----->
                            <div class="grid-wrapper">
                                <div class="grid-item grid-1">
                                    <div class="grid-img">
                                        <img src="assets/images/travel-icon.png" alt="travel insurance">
                                    </div>
                                    <div class="count-container">
                                        <h2 runat="server" id="travelcount">0</h2>
                                        <a href="#" class="insurance-link">TRAVEL INSURANCE</a>
                                    </div>
                                    <ul>
                                        <li><a href="Travelnsurance.aspx">New</a></li>
                                        <li><a href="Travelnsurance.aspx">Renewal</a></li>
                                        <li><a href="Travelnsurance.aspx">View</a></li>
                                          <li><a href="#"></a></li>
                                    </ul>
                                </div>
                                <div class="grid-item grid-2">
                                    <div class="grid-img">
                                        <img src="assets/images/motor-icon.png" alt="MOTOR INSURANCE">
                                    </div>
                                    <div class="count-container">
                                        <h2 runat="server" id="motorcount">0</h2>
                                        <a href="./motor-insurance.html" class="insurance-link">MOTOR INSURANCE</a>
                                    </div>
                                    <ul>
                                        <li><a href="MotorInsurance.aspx">New</a></li>
                                        <li><a href="MotorRenewal.aspx">Renewal</a></li>
                                        <li><a href="MotorInsurance.aspx">View</a></li>
                                          <li><a href="#"></a></li>
                                    </ul>
                                </div>
                                <div class="grid-item grid-3">
                                    <div class="grid-img">
                                        <img src="assets/images/domestic-icon.png" alt="DOMESTIC HELP">
                                    </div>
                                    <div class="count-container">
                                        <h2 runat="server" id="domesticcount">0</h2>
                                        <a href="#" class="insurance-link">DOMESTIC HELP</a>
                                    </div>
                                    <ul>
                                        <li><a href="DomesticHelp.aspx">New</a></li>
                                        <li><a href="DomesticHelp.aspx">Renewal</a></li>
                                        <li><a href="DomesticHelp.aspx">View</a></li>
                                          <li><a href="#"></a></li>
                                    </ul>
                                </div>
                                <div class="grid-item grid-4">
                                    <div class="grid-img">
                                        <img src="assets/images/home-icon.png" alt="HOME INSURANCE">
                                    </div>
                                    <div class="count-container">
                                        <h2 runat="server" id="homecount">0</h2>
                                        <a href="#" class="insurance-link">HOME INSURANCE</a>
                                    </div>
                                    <ul>
                                        <li><a href="HomeInsurancePage.aspx">New</a></li>
                                        <li><a href="HomeRenewal.aspx">Renewal</a></li>
                                        <li><a href="HomeInsurancePage.aspx">View</a></li>
                                          <li><a href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!------ End insurance grid ----->
                        </div>
                        <!-- //agile_top_w3_grids-->
                        <div class="col-md-12 graph-container padding_zero">
                            <div class="col-md-6 graph-row ">
                                <h3 class=" w3_inner_tittle two">SELLING POINT</h3>
                                <%--                                <div class="css_bar_graph">
                                    <!-- y_axis labels -->
                                    <ul class="y_axis">
                                        <li>100%</li>
                                        <li>90%</li>
                                        <li>80%</li>
                                        <li>70%</li>
                                        <li>60%</li>
                                        <li>50%</li>
                                        <li>40%</li>
                                        <li>30%</li>
                                        <li>20%</li>
                                        <li>10%</li>
                                        <li>0%</li>
                                    </ul>
                                    <!-- x_axis labels -->
                                    <ul class="x_axis">
                                        <li>2011</li>
                                        <li>2010</li>
                                        <li>2009</li>
                                        <li>2008</li>
                                    </ul>
                                    <!-- graph -->
                                    <div class="graph">
                                        <!-- grid -->
                                        <ul class="grid">
                                            <li>
                                                <!-- 10 -->
                                            </li>
                                            <li>
                                                <!-- 9 -->
                                            </li>
                                            <li>
                                                <!-- 8 -->
                                            </li>
                                            <li>
                                                <!-- 7 -->
                                            </li>
                                            <li>
                                                <!-- 6 -->
                                            </li>
                                            <li>
                                                <!-- 5 -->
                                            </li>
                                            <li>
                                                <!-- 4 -->
                                            </li>
                                            <li>
                                                <!-- 3 -->
                                            </li>
                                            <li>
                                                <!-- 2 -->
                                            </li>
                                            <li>
                                                <!-- 1 -->
                                            </li>
                                            <li class="bottom">
                                                <!-- 0 -->
                                            </li>
                                        </ul>

                                        <!-- bars -->
                                        <!-- 250px = 100% -->
                                        <ul>
                                            <li class="bar nr_1 orange" style="height: 240px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>90%</span></li>
                                            <li class="bar nr_2 purple" style="height: 125px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>50%</span></li>
                                            <li class="bar nr_3 green" style="height: 220px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>80%</span></li>
                                            <li class="bar nr_4 orange" style="height: 145px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>50%</span></li>
                                            <li class="bar nr_5 purple" style="height: 200px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>80%</span></li>
                                            <li class="bar nr_6 green" style="height: 105px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>40%</span></li>
                                            <li class="bar nr_7 orange" style="height: 180px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>70%</span></li>
                                            <li class="bar nr_8 purple" style="height: 95px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>30%</span></li>
                                            <li class="bar nr_9 green" style="height: 35px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>10%</span></li>
                                            <li class="bar nr_10 orange" style="height: 100px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>40%</span></li>
                                            <li class="bar nr_11 purple" style="height: 55px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>20%</span></li>
                                            <li class="bar nr_12 green" style="height: 75px;">
                                                <div class="top"></div>
                                                <div class="bottom"></div>
                                                <span>30%</span></li>
                                        </ul>
                                    </div>
                                </div>--%>
                                  <div class="col-md-12 selling_logo" runat="server" id="securaLogo" visible="false">
                                    <img src="assets/images/SecuraLogo.jpg" class="img-responsive" alt="selling_logo" />
                                </div>
                                 <div class="col-md-12 selling_logo" runat="server" id ="tiscoLogo" visible="false">
                                    <img src="assets/images/TiscoLogo.jpg" class="img-responsive" alt="selling_logo" style="width: 65%;margin: 0 auto;" />
                                </div>
                            </div>
                            <!--/prograc-blocks_agileits-->
                            <%--	<div class="prograc-blocks_agileits">
								<h3 class="w3_inner_tittle two">Daily Sales</h3>
								<div class="col-md-6 bars_agileits agile_info_shadow">
									<div class='bar_group'>
										<div class='bar_group__bar thin' label='TRAVEL INSURANCE' show_values='true' tooltip='true' value='343'></div
										<div class='bar_group__bar thin' label='MOTOR INSURANCE' show_values='true' tooltip='true' value='235'></div>
										<div class='bar_group__bar thin' label='DOMESTIC HELP' show_values='true' tooltip='true' value='550'></div>
										<div class='bar_group__bar thin' label='HOME INSURANCE' show_values='true' tooltip='true' value='456'></div>
									</div>
								</div>
							</div>--%>
                            <div class="col-md-6  prograc-blocks_agileits">
                                <h3 class="w3_inner_tittle two">Policy Search</h3>
                                <div class="col-md-6 bars_agileits agile_info_shadow">
                                    <div class='bar_group'>
                                        <div class="col-md-2 page-label">
                                            <label class="control-label">CPR: *</label>
                                        </div>
                                        <div class="col-md-5  page-control">
                                            <asp:DropDownList ID="ddlCPR" runat="server" CssClass="form-control col-md-10 chzn-select" CauseValidation="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvddlCPR" CssClass="err" ErrorMessage="Please select CPR" SetFocusOnError="true" ControlToValidate="ddlCPR" runat="server" ValidationGroup="domesticValidation" />
                                        </div>
                                        <%--<asp:TextBox ID="searchPolicyByCPR" runat="server"></asp:TextBox>--%>
                                        <asp:Button ID="btnSearch" Text="Find" runat="server" OnClientClick="showPageLoader();" OnClick="Search_Policy" CssClass="btn btn-primary"></asp:Button>
                                        <asp:HiddenField runat="server" ID="InsuredCode" />
                                        <asp:HiddenField runat="server" ID="InsuredName" />
                                    </div>
                                    <asp:GridView ID="gvDocuments" OnRowDataBound="Gridview1_RowDataBound" runat="server" CssClass="table table-bordered" AutoGenerateColumns="False"
                                        AllowPaging="True" AllowSorting="True" Width="100%" PageSize="10">
                                        <%-- OnSorting="gv_Sorting" OnPageIndexChanging="gv_PageIndexChanging" OnDataBound="gvMotorInsurance_DataBound"--%>
                                        <HeaderStyle CssClass="bcolorhead fcolorwhite" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="S.No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" Text=" <%# Container.DataItemIndex + 1 %>"></asp:Label>
                                                    <asp:Label ID="lblPolicyType" runat="server" Text='<%# Eval("PolicyType") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblPolicyNo" runat="server" Text='<%# Eval("DocumentNo") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DocumentNo" HeaderText="Policy No" SortExpression="Name" />
                                            <asp:BoundField DataField="ExpireDate" HeaderText="Expire Date" SortExpression="Name" />
                                            <asp:BoundField DataField="PolicyType" HeaderText="Insurance" SortExpression="Name" />
                                            <asp:TemplateField HeaderText="Review">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnView" runat="server" ToolTip="Edit Policy Details" CssClass="fsize fcolorgreen" OnClick="lnkbtnEdit_Click" OnClientClick="document.forms[0].target ='_blank';"><i class="fa  fa-edit"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Renew">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkbtnRenew" runat="server" ToolTip="Renew Policy" CssClass="fsize fcolorgreen" OnClick="lnkbtnRenew_Click" OnClientClick="document.forms[0].target ='_blank';"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <!--//prograc-blocks_agileits-->
                        </div>
                        <!-- /w3ls_agile_circle_progress-->
                    </div>
                    <!-- //inner_content_w3_agile_info-->
                </div>
                <!-- //inner_content-->
            </div>
            <!-- banner -->
            <!--copy rights start here-->

            <!--copy rights end here-->
            <!-- js -->

            <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

            <!-- /amcharts -->
            <%--<script src="js/amcharts.js"></script>--%>
            <script src="js/serial.js"></script>
            <script src="js/export.js"></script>
            <script src="js/light.js"></script>
            <!-- Chart code -->
            <%--<script>
                var chart = AmCharts.makeChart("chartdiv", {
                    "theme": "light",
                    "type": "serial",
                    "startDuration": 2,
                    "dataProvider": [{
                        "country": "Canada",
                        "visits": 441,
                        "color": "#CD0D74"
                    }, {
                        "country": "Brazil",
                        "visits": 395,
                        "color": "#754DEB"
                    }, {
                        "country": "Italy",
                        "visits": 386,
                        "color": "#DDDDDD"
                    }, {
                        "country": "Taiwan",
                        "visits": 338,
                        "color": "#333333"
                    }],
                    "valueAxes": [{
                        "position": "left",
                        "axisAlpha": 0,
                        "gridAlpha": 0
                    }],
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "colorField": "color",
                        "fillAlphas": 0.85,
                        "lineAlpha": 0.1,
                        "type": "column",
                        "topRadius": 1,
                        "valueField": "visits"
                    }],
                    "depth3D": 10,
                    "angle": 30,
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "country",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 10,
                        "gridAlpha": 0

                    },
                    "export": {
                        "enabled": true
                    }

                }, 0);
            </script>
            <!-- Chart code -->
            <script>
                var chart = AmCharts.makeChart("chartdiv1", {
                    "type": "serial",
                    "theme": "light",
                    "legend": {
                        "horizontalGap": 10,
                        "maxColumns": 1,
                        "position": "right",
                        "useGraphSettings": true,
                        "markerSize": 10
                    },
                    "dataProvider": [{
                        "year": 2017,
                        "europe": 2.5,
                        "namerica": 2.5,
                        "asia": 2.1,
                        "lamerica": 0.3,
                        "meast": 0.2,
                        "africa": 0.1
                    }, {
                        "year": 2016,
                        "europe": 2.6,
                        "namerica": 2.7,
                        "asia": 2.2,
                        "lamerica": 0.3,
                        "meast": 0.3,
                        "africa": 0.1
                    }, {
                        "year": 2015,
                        "europe": 2.8,
                        "namerica": 2.9,
                        "asia": 2.4,
                        "lamerica": 0.3,
                        "meast": 0.3,
                        "africa": 0.1
                    }],
                    "valueAxes": [{
                        "stackType": "regular",
                        "axisAlpha": 0.5,
                        "gridAlpha": 0
                    }],
                    "graphs": [{
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 0.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 0.3,
                        "title": "Europe",
                        "type": "column",
                        "color": "#000000",
                        "valueField": "europe"
                    }, {
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 0.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 0.3,
                        "title": "North America",
                        "type": "column",
                        "color": "#000000",
                        "valueField": "namerica"
                    }, {
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 0.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 0.3,
                        "title": "Asia-Pacific",
                        "type": "column",
                        "color": "#000000",
                        "valueField": "asia"
                    }, {
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 0.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 0.3,
                        "title": "Latin America",
                        "type": "column",
                        "color": "#000000",
                        "valueField": "lamerica"
                    }, {
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 0.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 0.3,
                        "title": "Middle-East",
                        "type": "column",
                        "color": "#000000",
                        "valueField": "meast"
                    }, {
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 0.8,
                        "labelText": "[[value]]",
                        "lineAlpha": 0.3,
                        "title": "Africa",
                        "type": "column",
                        "color": "#000000",
                        "valueField": "africa"
                    }],
                    "rotate": true,
                    "categoryField": "year",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        "position": "left"
                    },
                    "export": {
                        "enabled": true
                    }
                });
            </script>--%>

            <!-- //amcharts -->
            <script src="assets/customjs/chart1.js"></script>
            <script src="assets/customjs/Chart.min.js"></script>
            <script src="assets/customjs/modernizr.custom.js"></script>
            <script src="assets/customjs/classie.js"></script>
            <script src="assets/customjs/gnmenu.js"></script>
            <script>
                new gnMenu(document.getElementById('gn-menu'));
            </script>
            <!-- script-for-menu -->

            <!-- /circle-->
            <script type="text/javascript" src="assets/customjs/circles.js"></script>
            <%--<script>
                var colors = [
                    ['#ffffff', '#fd9426'], ['#ffffff', '#fc3158'], ['#ffffff', '#53d769'], ['#ffffff', '#147efb']
                ];
                for (var i = 1; i <= 7; i++) {
                    var child = document.getElementById('circles-' + i),
                        percentage = 30 + (i * 10);

                    Circles.create({
                        id: child.id,
                        percentage: percentage,
                        radius: 80,
                        width: 10,
                        number: percentage / 1,
                        text: '%',
                        colors: colors[i - 1]
                    });
                }
            </script>--%>
            <!-- //circle -->
            <!--skycons-icons-->
            <script src="assets/customjs/skycons.js"></script>
            <script>
                var icons = new Skycons({ "color": "#fcb216" }),
                    list = [
                        "partly-cloudy-day"
                    ],
                    i;

                for (i = list.length; i--;)
                    icons.set(list[i], list[i]);
                icons.play();
            </script>
            <script>
                var icons = new Skycons({ "color": "#fff" }),
                    list = [
                        "clear-night", "partly-cloudy-night", "cloudy", "clear-day", "sleet", "snow", "wind", "fog"
                    ],
                    i;

                for (i = list.length; i--;)
                    icons.set(list[i], list[i]);
                icons.play();
            </script>
            <!--//skycons-icons-->
            <!-- //js -->
            <script src="assets/customjs/screenfull.js"></script>
            <script>
                $(function () {
                    $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

                    if (!screenfull.enabled) {
                        return false;
                    }

                    $('#toggle').click(function () {
                        screenfull.toggle($('#container')[0]);
                    });
                });
            </script>
            <script src="assets/customjs/flipclock.js"></script>

            <script type="text/javascript">
                var clock;
                $(document).ready(function () {

                    clock = $('.clock').FlipClock({
                        clockFace: 'HourlyCounter'
                    });
                });
            </script>
            <script src="assets/customjs/bars.js"></script>
            <script src="assets/customjs/jquery.nicescroll.js"></script>
            <script src="assets/customjs/scripts.js"></script>   

            <script type="text/javascript">
                function motorEdit(uri) {
                    // alert('Hai');
                    //var redirectWindow = window.open(uri, '_blank');
                    //$.ajax({
                    //    type: 'POST',
                    //    url: '/echo/json/',
                    //    async: false,
                    //    success: function (data) {
                    //        redirectWindow.location;
                    //    }
                    //});
                    var a = document.createElement("a");
                    a.setAttribute("href", uri);
                    a.setAttribute("target", "_blank");
                    a.setAttribute("id", "openwin");
                    document.body.appendChild(a);
                    a.click();
                }
            </script>

            <%--</body>
</html>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>