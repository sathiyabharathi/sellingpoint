﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class MotorCoverMaster : System.Web.UI.Page
    {
        General master;
        public MotorCoverMaster()
        {
            master = Master as General;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                btnSubmit.Text = "Save";
                LoadMotorMasterData();
                ClearControl();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string opertaion = string.Empty;
                //master.ShowLoading = true;
                var service = new BKIC.SellingPoint.Presentation.ClientUtility();//master.GetService();
                //var client = new BKIC.SellingPoint.Presentation.ClientUtility();

                service.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);
                var details = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorCoverMaster();

                details.CoversDescription = txtCoverDescription.Text.ToString();
                details.CoversCode = txtCoverCode.Text.ToString();

                opertaion = (sender as Button).Text;

                if (opertaion == "Update")
                {                    
                     details.Type = "edit";
                }
                else
                {
                    details.Type = "insert";                  

                }

                var results = service.serviceManger.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorCoverMasterResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorCoverMaster>
                    (BKIC.SellingPoint.DTO.Constants.AdminURI.MotorCoverMasterOperation, details);
                if (results.StatusCode == 200)
                {
                    LoadMotorMasterData();
                    ClearControl();
                    //btnSubmit.Text = (branchdetails.Type == "edit") ? "Save" : "Update";
                    btnSubmit.Text = "Save";
                }

                //if (service.UserInfo.StatusCode == 200)
                //{
                //client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, client.UserInfo.AccessToken, false);
                //Session["UserInfo"] = client.UserInfo;
                //// createa a new GUID and save into the session
                //string guid = Guid.NewGuid().ToString();
                //// now create a new cookie with this guid value
                //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", guid));

                //if (Session["ReturnUrl"] != null)
                //{
                //    this.Response.Redirect(Session["ReturnUrl"].ToString());
                //}
                //else
                //{
                //    this.Response.Redirect("HomePage.aspx");
                //}
                //}
                //master.ShowLoading = false;
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnSubmit.Text = "Save";
            ClearControl();
        }

        private void ClearControl()
        {
           txtCoverDescription.Text= string.Empty;
        }
        public void LoadMotorMasterData()
        {
            //master.ShowLoading = true;

            var client = new BKIC.SellingPoint.Presentation.ClientUtility();

            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);

            var response = client.serviceManger.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.MasterTableResult<BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorCoverMaster>>>
                  (BKIC.SellingPoint.DTO.Constants.AdminURI.GetMasterTableByTableName.Replace("{tableName}", BKIC.SellingPoint.DTO.RequestResponseWrappers.MasterTable.MotorCoverMaster));

            if (response.StatusCode == 200)
            {
                if (response.Result.IsTransactionDone)
                {
                     gvMotorMaster.DataSource = response.Result.TableRows;
                     gvMotorMaster.DataBind();
                }
                else
                {
                    //  lbler.Text = result.Result.TransactionErrorMessage;
                }
            }
            else
            {
                // lbler.Text = result.ErrorMessage;
            }
        }

        #region Test_1
         protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMotorMaster.PageIndex = e.NewPageIndex;
        LoadMotorMasterData(); //bindgridview will get the data source and bind it again
    }
         protected void gvMotorInsurance_DataBound(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in gvMotorMaster.Rows)
        //{
        //    string HIRStatusCode = (row.FindControl("lblHIRStatusCode") as Label).Text.Trim();
        //    string IsMessage = (row.FindControl("lblIsMessage") as Label).Text.Trim();
        //    string IsDocuments = (row.FindControl("IsDocument") as Label).Text.Trim();

        //    if (IsDocuments == "True")
        //    {
        //        var btnHIRFiles = row.FindControl("btnDocument") as LinkButton;
        //        btnHIRFiles.Visible = true;
        //    }
        //    else
        //    {
        //        var btnHIRFiles = row.FindControl("btnDocument") as LinkButton;
        //        btnHIRFiles.Visible = false;
        //    }

        //    if (IsMessage == "True")
        //    {
        //        var btnDocFiles = row.FindControl("btnViewMail") as LinkButton;
        //        btnDocFiles.Visible = true;
        //    }
        //    else
        //    {
        //        var btnDocFiles = row.FindControl("btnViewMail") as LinkButton;
        //        btnDocFiles.Visible = false;
        //    }
        //}
         }
        protected void lnkbtnEdit_Click(object sender, EventArgs e)
    {
        using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
        {
            if (ViewState["MoterMasterId"] != null)
            {
                ViewState["MoterMasterId"] = string.Empty;
            }
            string id = row.Cells[1].Text.Trim();
            ViewState["MoterMasterId"] = id;
            txtCoverCode.Text = row.Cells[0].Text.Trim();
            txtCoverDescription.Text = row.Cells[3].Text.Trim();            
            btnSubmit.Text = "Update";
        }
    }
        protected void lnkbtnDelete_Click(object sender, EventArgs e)
    {
        using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
        {
            int id = Convert.ToInt32(row.Cells[1].Text.Trim());
            var service = new BKIC.SellingPoint.Presentation.ClientUtility();//master.GetService();
            service.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);
            var details = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgentMaster();

            details.Id = id;
            details.Type = "delete";

            var branchResult = service.serviceManger.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.AgentMasterResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.AgentMaster>
                   (BKIC.SellingPoint.DTO.Constants.AdminURI.AgentOperation, details);
            if (branchResult.StatusCode == 200)
            {
                LoadMotorMasterData();
            }
        }
    }
        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dlist.DefaultView.Sort = e.SortExpression + " " + SortDir(e.SortExpression);
            //gvMotorInsurance.DataSource = dlist;
            //gvMotorInsurance.DataBind();
        }

        #endregion
    }
}