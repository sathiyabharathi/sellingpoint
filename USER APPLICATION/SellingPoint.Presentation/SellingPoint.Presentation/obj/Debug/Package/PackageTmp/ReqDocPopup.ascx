﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReqDocPopup.ascx.cs" Inherits="BKIC.SellingPoint.Presentation.ReqDocPopup" %>
<div class="custompopup" runat="server" id="RequestDocPopup" visible="false">

    <div class="clearfix">
        <div class="col-md-6">
            <label>Document ID</label>
        </div>
        <div class="col-md-6">
            <asp:HiddenField ID="hiddenInsuranceType" runat="server" />
            <asp:HiddenField ID="hiddenPolicyNo" runat="server" />
            <asp:HiddenField ID="hiddenLinkId" runat="server" />
            <asp:TextBox runat="server" Enabled="false" ID="RefIDReqDoc"></asp:TextBox>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-md-6">
            <label>Insured Code</label>
        </div>
        <div class="col-md-6">
            <asp:TextBox runat="server" Enabled="false" ID="txtInsuredIDReqDoc"></asp:TextBox>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-md-6">
            <label>Message</label>
        </div>
        <div class="col-md-6">
            <asp:TextBox runat="server" TextMode="MultiLine" ID="txtMessage"></asp:TextBox>
        </div>

        <div class="col-md-12">
            <div class="col-md-12">
                <asp:Button runat="server" ID="btnSubmitReqDoc" OnClick="btnSubmitReqDoc_Click" Text="submit" />
                <asp:Button runat="server" ID="btnClosePopup" OnClick="btnClosePopup_Click" Text="Cancel" />
            </div>
        </div>

    </div>
</div>

