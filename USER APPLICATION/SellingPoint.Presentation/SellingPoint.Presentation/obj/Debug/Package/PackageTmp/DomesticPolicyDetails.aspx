﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General.Master" CodeBehind="DomesticPolicyDetails.aspx.cs" Inherits="SellingPoint.Presentation.DomesticPolicyDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <%--Welcome to domestic detail Page!!--%> 

    <%--<div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <div class="field-wrap package col-sm-12">
                                        <div class="col-md-12 pull-left forms_colon">
                                            <label>
                                                CPR
                                            </label>
                                            <div class="field-wrap col-md-7">
                                                <span runat="server" id="CPRPreview"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="field-wrap package col-sm-12">
                                        <div class="col-md-12 pull-left forms_colon">
                                            <label>
                                                Start Date
                                            </label>
                                            <div class="field-wrap col-md-7">
                                                <span runat="server" id="StartDatePreview"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="field-wrap package col-sm-12">
                                        <div class="col-md-12 pull-left forms_colon">
                                            <label>
                                                Status
                                            </label>
                                            <div class="field-wrap col-md-7">
                                                <span runat="server" id="StatusPreview"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <div class="field-wrap package col-sm-12">
                                        <div class="col-md-12 pull-left forms_colon">
                                            <label>
                                                Insured
                                            </label>
                                            <div class="field-wrap col-md-7">
                                                <span runat="server" id="InsuredNamePreview"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                              
                                <div class="form-group ">
                                    <div class="field-wrap package col-sm-12">
                                        <div class="col-md-12 pull-left forms_colon">
                                            <label>
                                                Expiry Date
                                            </label>
                                            <div class="field-wrap col-md-7">
                                                <span runat="server" id="ExpiryDatePreview"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="field-wrap package col-sm-12">
                                        <div class="col-md-12 pull-left forms_colon">
                                            <label>
                                                Premium Amount
                                            </label>
                                            <div class="field-wrap col-md-7">
                                                <span runat="server" id="PremiumAmountPreview"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                           
                        </div>
</div>--%>


    <div class="x_panel">
                        <div class="x_title">
                            <h2>Domestic Policy details:</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-horizontal form-label-left col-md-12">

                                <div class="form-group col-md-6">
                                    <div class="col-md-4 page-label">
                                        <label class="control-label pull-right">CPR:</label>
                                    </div>
                                    <div class="col-md-8 page-control">
                                        <div class="pull-left">
                                            <label id="lblcpr" runat="server" class="control-label"></label>
                                            <%--<span runat="server" id="cpr"  class="form-control col-md-10"></span>--%>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <div class="col-md-4 page-label">
                                        <label class="control-label pull-right">InsuredName:</label>
                                    </div>
                                    <div class="col-md-8 page-control">
                                        <div class="pull-left">
                                            <label id="lblInsured" runat="server" class="control-label"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="col-md-4 page-label">
                                        <label class="control-label pull-right">Policy Expired Date:</label>
                                    </div>
                                    <div class="col-md-8 page-control">
                                        <div class="pull-left">
                                            <label id="lblPolicyExpDate" runat="server" class="control-label"></label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>

                            </div>
                        </div>
        <div class="x_content">

            <div class="x_title">
                            <h2>DomesticHelp Member details:</h2>
                            <%--<ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                            </ul>--%>
                            <div class="clearfix"></div>
                        </div>

                            <asp:GridView ID="gvDomesticHelp" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Width="100%" PageSize="10" CssClass="table table-striped">

                                <Columns>
                                    <asp:TemplateField HeaderText="S.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text=" <%# Container.DataItemIndex + 1 %>"></asp:Label> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <%--<asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-CssClass="DateKeyHide" HeaderStyle-CssClass="DateKeyHide" />--%>
                                    <asp:BoundField DataField="InsuredName" HeaderText="Name" SortExpression="Name" />
                                    <asp:BoundField DataField="Sex" HeaderText="Sex" SortExpression="Name" />
                                    <asp:BoundField DataField="DOB" HeaderText="Date Of Birth" SortExpression="Name" />
                                    <asp:BoundField DataField="Nationality" HeaderText="Nationality" SortExpression="Name" />
                                    <asp:BoundField DataField="Passport" HeaderText="Passport No" SortExpression="Name" />
                                    <asp:BoundField DataField="Occupation" HeaderText="Occupation" SortExpression="Name" />
                                    <asp:BoundField DataField="AddressType" HeaderText="Address" SortExpression="Name" />

                                    <%--<asp:BoundField DataField="" HeaderText="Flat No" SortExpression="Name" />
                                    <asp:BoundField DataField="" HeaderText="Building No" SortExpression="Name" />
                                    <asp:BoundField DataField="" HeaderText="Block No" SortExpression="Name" />

                                    <asp:BoundField DataField="" HeaderText="Road No" SortExpression="Name" />
                                    <asp:BoundField DataField="" HeaderText="Town" SortExpression="Name" />--%>
                                   
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

</asp:Content>

