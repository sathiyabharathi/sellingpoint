﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class HomePage : System.Web.UI.Page
    {
        General master;
        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!IsPostBack)
            {
                BindDropdown();
                loadCounts();
            }
        }

        public HomePage()
        {
            master = Master as General;
        }

        public void loadCounts()
        {
            if (!master.IsSessionAvailable())
            {
                master.RedirectToLogin();
            }
            var service = master.GetService();

            var userInfo = Session["UserInfo"] as OAuthTokenResponse;

            string currentdate = Convert.ToString(DateTime.Now);
            var details = new BKIC.SellingPoint.DTO.RequestResponseWrappers.DashboardRequest();

            details.FromDate = DateTime.Today;
            details.ToDate = DateTime.Today.AddDays(1);
            details.Agent = userInfo.Agency;
            details.AgencyCode = userInfo.AgentCode;
            details.BranchCode = userInfo.AgentBranch;

            var Results = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                          <BKIC.SellingPoint.DTO.RequestResponseWrappers.DashboardResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.DashboardRequest>
                         (BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.FetchDashboard, details);

            if(Results.StatusCode==200 && Results.Result.IsTransactionDone)
            {
                //foreach (var i in Results.Result.HIRList)
                //{
                //    if (i.InsuranceType == "MotorInsuranceHIR")
                //    {
                //        motorcount.InnerText = Convert.ToString(i.HIRCount);
                //    }

                //    if (i.InsuranceType == "TravelInsuranceHIR")
                //    {
                //        travelcount.InnerText = Convert.ToString(i.HIRCount);
                //    }

                //    if (i.InsuranceType == "HomeInsuranceHIR")
                //    {
                //        homecount.InnerText = Convert.ToString(i.HIRCount);
                //    }
                //    if (i.InsuranceType == "DomesticInsuranceHIR")
                //    {
                //        domesticcount.InnerText = Convert.ToString(i.HIRCount);
                //    }
                //}
                List<int> activepolicy = new List<int>();
                List<int> renewpolicy = new List<int>();
                foreach (var count in Results.Result.ActiveList)
                {
                    if (count.InsuranceType == Constants.Travel)
                    {
                        travelcount.InnerText = Convert.ToString(count.ActiveCount);
                    }
                    if (count.InsuranceType == Constants.Motor)
                    {
                        motorcount.InnerText = Convert.ToString(count.ActiveCount);
                    }
                    if (count.InsuranceType == Constants.DomesticHelp)
                    {
                        domesticcount.InnerText = Convert.ToString(count.ActiveCount);
                    }
                    if (count.InsuranceType == Constants.Home)
                    {
                        homecount.InnerText = Convert.ToString(count.ActiveCount);
                    }
                }
            }
        }
        public void BindDropdown()
        {

            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest();


            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                req.AgentBranch = userInfo.AgentBranch;
                req.AgentCode = userInfo.AgentCode;
               // req.Agency = userInfo.;
            }


            if (userInfo.Agency.ToLower() == "bbk")
                securaLogo.Visible = true;
            if (userInfo.Agency.ToLower() == "tisco")
                tiscoLogo.Visible = true;

            var service = master.GetService();

            var Results = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                          <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredResponse>, 
                          BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest>
                          (BKIC.SellingPoint.DTO.Constants.AdminURI.GetAgencyInsured, req);

            if (Results.StatusCode == 200 && Results.Result.IsTransactionDone && Results.Result.AgencyInsured.Count > 0)
            {
                ddlCPR.DataSource = Results.Result.AgencyInsured;
                ddlCPR.DataTextField = "CPR";
                ddlCPR.DataValueField = "InsuredCode";
                ddlCPR.DataBind();
                ddlCPR.Items.Insert(0, new ListItem("--Please Select--", ""));
                //InsuredNames = Results.Result.AgencyInsured;
            }
        }
   

    protected void Search_Policy(object sender, EventArgs e)
        {

            try
            {
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;

                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    //request.AgentBranch = userInfo.AgentBranch;
                    //request.AgentCode = userInfo.AgentCode;
                    //req.Agency = userInfo.;
                }
                var service = master.GetService();
                // [ApiAuthorize(BKIC.SellingPont.DTO.Constants.Roles.SuperAdmin, BKIC.SellingPont.DTO.Constants.Roles.BranchAdmin)]
                //URI.AdminURI.FetchDocumentDetailsByCPR
                //BKIC.SellingPont.DTO.Constants.AdminURI.FetchDocumentDetailsByCPR
                string uri = "api/admin/getdocumentsbycpr/{cpr}/{agentcode}";


                uri = uri.Replace("{cpr}", ddlCPR.SelectedItem.Text.Trim()).Replace("{agentcode}", userInfo.AgentCode);

                var AllDocuments = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers
                                   .ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.DocumentDetailsResult>>(uri);


                if (AllDocuments != null && AllDocuments.Result != null && AllDocuments.StatusCode == 200 && AllDocuments.Result.IsTransactionDone)
                {
                    gvDocuments.DataSource = AllDocuments.Result.DocumentDetails;
                    gvDocuments.DataBind();

                }


                string uri1 = "api/user/fetchdetailscprinsuredCode";

                BKIC.SellingPoint.DTO.RequestResponseWrappers.InsuredRequest request = new InsuredRequest();
                request.CPR = ddlCPR.SelectedItem.Text.Trim();
                request.Agency = userInfo.Agency;
                request.AgentCode = userInfo.AgentCode;



                var postData = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                              <BKIC.SellingPoint.DTO.RequestResponseWrappers.InsuredResponse>,
                              BKIC.SellingPoint.DTO.RequestResponseWrappers.InsuredRequest>(uri1, request);

                if (postData.StatusCode == 200 && postData.Result != null && postData.Result.IsTransactionDone)
                {
                    InsuredCode.Value = postData.Result.InsuredDetails.InsuredCode;
                    InsuredName.Value = postData.Result.InsuredDetails.LastName;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }

        }

        protected void lnkbtnNew_Click(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                string policyType = (row.FindControl("lblPolicyType") as Label).Text.Trim();
                string policyno = (row.FindControl("lblPolicyNo") as Label).Text.Trim();
                (row.FindControl("lnkbtnNew") as LinkButton).Attributes.Add("target", "_blank");
                
                switch (policyType)
                {
                    case "DomesticHelp":                       
                        Response.Redirect("DomesticHelp.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim());                      
                        break;
                    case "Travel":
                        Response.Redirect("Travelnsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim());
                        break;
                    case "Home":
                        Response.Redirect("HomeInsurancePage.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim());
                        break;
                    case "Motor":
                        Response.Redirect("MotorInsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim());
                        break;

                } 
            }
        }
        protected void lnkbtnRenew_Click(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                string policyType = (row.FindControl("lblPolicyType") as Label).Text.Trim();
                string policyno = (row.FindControl("lblPolicyNo") as Label).Text.Trim();

                switch (policyType)
                {
                    case "DomesticHelp":
                        Response.Redirect("DomesticHelp.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                        break;
                    case "Travel":
                        Response.Redirect("Travelnsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                        break;
                    case "Home":
                        Response.Redirect("HomeInsurancePage.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                        break;
                    case "Motor":
                        Response.Redirect("MotorInsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);                       
                        break;                       
                }
            }
        }
        protected void lnkbtnEdit_Click(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                string policyType = (row.FindControl("lblPolicyType") as Label).Text.Trim();
                string policyno = (row.FindControl("lblPolicyNo") as Label).Text.Trim();
                
                switch (policyType)
                {
                    case "DomesticHelp":
                         Response.Redirect("DomesticHelp.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('DomesticHelp.aspx?InsuredCode=" + InsuredCode.Value.ToString() + "&InsuredName=" + InsuredName.Value + "&CPR=" + searchPolicyByCPR.Text.Trim() + "&PolicyNo=" + policyno + "','_newtab');", true);
                        break;
                    case "Travel":
                        Response.Redirect("Travelnsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('Travelnsurance.aspx?InsuredCode=" + InsuredCode.Value.ToString() + "&InsuredName=" + InsuredName.Value + "&CPR=" + searchPolicyByCPR.Text.Trim() + "&PolicyNo=" + policyno + "','_newtab');", true);
                        break;
                    case "Home":
                        Response.Redirect("HomeInsurancePage.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('HomeInsurancePage.aspx?InsuredCode=" + InsuredCode.Value.ToString() + "&InsuredName=" + InsuredName.Value + "&CPR=" + searchPolicyByCPR.Text.Trim() + "&PolicyNo=" + policyno + "','_newtab');", true);
                        break;
                    case "Motor":
                        //var motorURl = "MotorInsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + searchPolicyByCPR.Text.Trim() + "&PolicyNo=" + policyno;
                        Response.Redirect("MotorInsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + ddlCPR.SelectedItem.Text.Trim() + "&PolicyNo=" + policyno);
                       // ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('MotorInsurance.aspx?InsuredCode=" + InsuredCode.Value + "&InsuredName=" + InsuredName.Value + "&CPR=" + searchPolicyByCPR.Text.Trim() + "&PolicyNo=" + policyno + "','_blank','location=1,status=1,scrollbars=1,width=100,height=100');", true);
                        break;

                }
            }
        }
        protected void Gridview1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                var uu = ((Label)e.Row.FindControl("lblPolicyType")).Text;


              //DropDownList ddl = (DropDownList)e.Row.FindControl("ddlNational");
              //ddl.DataValueField = "Code";
              //ddl.DataTextField = "Description";
              //ddl.DataSource = Nationalitydt;
              //ddl.DataBind();
              //ddl.Items.Insert(0, new ListItem("--Please Select--", ""));
                
            }
        }
    }
}