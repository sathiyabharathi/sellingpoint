﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class TravelPolicyDetails : System.Web.UI.Page
    {
        General master;

        public TravelPolicyDetails()
        {
            master = Master as General;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            var service = master.GetService();

            if (Request.QueryString["TravelId"] != null && Request.QueryString["InsuredCode"] != null)
            {
                int DomesticId = Convert.ToInt32(Request.QueryString["TravelId"]);
                string InsuredCode = Convert.ToString(Request.QueryString["InsuredCode"]);
                BindDomesticPolicyPage(DomesticId, InsuredCode);
            }
        }

        private void BindDomesticPolicyPage(int travelId, string insuredCode)
        {
            var service = master.GetService();

            var client = new BKIC.SellingPoint.Presentation.ClientUtility();
            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);

            var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyDomesticRequest();
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;

            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                req.AgentBranch = userInfo.AgentBranch;
                req.AgentCode = userInfo.AgentCode;
                //req.Agency = userInfo.;
            }           
            var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuotation.Replace("{travelQuotationId}", travelId.ToString()).Replace("{userInsuredCode}", insuredCode);
            var travelDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelSavedQuotationResponse>>(url);
            var res = travelDetails.Result.TravelInsurancePolicyDetails;

            var list = new List<TravelInsurancePolicy>();
            list.Add(res);
            //Gv
            var travel = travelDetails.Result.TravelInsurancePolicyDetails;
            gvTravelDetail.DataSource = list;
            gvTravelDetail.DataBind();

            //Details
            lblInsured.InnerText = res.InsuredName;
            lblcpr.InnerText = res.CPR;
            lblPolicyExpDate.InnerText = res.ExpiryDate.ConvertToLocalFormat();
            //cpr.InnerText = res.CPR;

        }
    }
}