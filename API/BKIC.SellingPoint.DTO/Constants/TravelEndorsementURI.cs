﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class TravelEndorsementURI
    {
        public const string GetTravelEndorsementQuote = "api/travelEndorsement/gettravelendorsementquote";
        public const string PostTravelEndorsement = "api/travelEndorsement/posttravelEndorsement";
        public const string EndorsementPreCheck = "api/travelEndorsement/endorsementprecheck";
        public const string GetAllEndorsements = "api/travelEndorsement/getallendorsements";
        public const string EndorsementOperation = "api/travelEndorsement/endorsementoperation";
    }
}
