﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class MotorEndorsementURI
    {
        public const string GetMotorEndorsementQuote = "api/motorendorsement/getmotorendorsementquote";       
        public const string PostMotorEndorsement = "api/motorendorsement/postmotorendorsement";
        public const string EndorsementPreCheck = "api/motorendorsement/endorsementprecheck";
        public const string GetAllEndorsements = "api/motorendorsement/getallendorsements";
        public const string EndorsementOperation = "api/motorendorsement/endorsementoperation";
        public const string PostAdminMotorEndorsement = "api/motorendorsement/postadminmotorendorsement";
    }
}
