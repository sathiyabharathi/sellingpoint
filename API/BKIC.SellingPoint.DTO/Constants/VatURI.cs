﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class VatURI
    {
        public const string CalculateVat = "api/insurance/Vat";
    }
}
