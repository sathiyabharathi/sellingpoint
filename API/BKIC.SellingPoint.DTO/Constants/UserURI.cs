﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class UserURI
    {
        public const string PostUserMaster = "api/user/postUserMasterDetails";
        public const string Authentication = "api/BKICSPOAuth";
        public const string RegisterAdminUser = "api/user/RegisterAdminUser";
    }
}
