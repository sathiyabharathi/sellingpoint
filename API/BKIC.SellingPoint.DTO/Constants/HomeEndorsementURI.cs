﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class HomeEndorsementURI
    {
        public const string GetHomeEndorsementQuote = "api/homeendorsement/gethomeendorsementquote";
        public const string PostHomeEndorsement = "api/homeendorsement/posthomeendorsement";
        public const string EndorsementPreCheck = "api/homeendorsement/endorsementprecheck";
        public const string GetAllEndorsements = "api/homeendorsement/getallendorsements";
        public const string EndorsementOperation = "api/homeendorsement/endorsementoperation";
        public const string GetHomeDomesticHelpEndorsementQuote = "api/homeendorsement/gethomedomestichelpquote";
    }
}
