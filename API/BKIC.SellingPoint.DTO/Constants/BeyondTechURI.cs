﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPont.DTO.Constants
{
    public class DropdownURI
    {
        public const string GetPageDropDowns = "api/dropdown/fetchdropdowns/{type}";
    }
}
