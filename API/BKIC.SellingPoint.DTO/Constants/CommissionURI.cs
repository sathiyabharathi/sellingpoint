﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class CommissionURI
    {
        public const string CalculateCommission = "api/insurance/Commission";
    }
}
