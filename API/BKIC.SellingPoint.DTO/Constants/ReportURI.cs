﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DTO.Constants
{
    public class ReportURI
    {
        public const string GetMotorReport = "api/report/motorreport";
        public const string GetTravelReport = "api/report/travelreport";
        public const string GetHomeReport = "api/report/homereport";
        public const string GetMainReport = "api/report/mainreport";
    }
}
