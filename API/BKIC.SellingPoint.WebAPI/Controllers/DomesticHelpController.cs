﻿//using KBIC.DL.BL.Repositories;
using BKIC.SellingPoint.DL.BL.Repositories;
using BKIC.SellingPoint.WebAPI.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLO = BKIC.SellingPoint.DL.BO;
using RR = BKIC.SellingPoint.DTO.RequestResponseWrappers;
using URI = BKIC.SellingPoint.DTO.Constants;

namespace KBIC.WebAPI.Controllers
{
    /// <summary>
    /// Domestic policy methods.
    /// </summary>
    public class DomesticHelpController : ApiController
    {
        public readonly IDomesticHelp _domesticHelpRepo;
        private readonly AutoMapper.IMapper _mapper;

        public DomesticHelpController(IDomesticHelp repository)
        {
            _domesticHelpRepo = repository;
            BKICAutomapper automap = new BKICAutomapper();
            _mapper = automap.GetDomesticAutoMapper();
        }
        /// <summary>
        /// Get all domestic policies by agency.
        /// </summary>
        /// <param name="request">Agency domestic request.</param>
        /// <returns>List of domestic policies by agency.</returns>
        //[ApiAuthorize]
        [HttpPost]
        [Route(URI.DomesticURI.GetDomesticAgencyPolicy)]
        public RR.AgencyDomesticPolicyResponse GetDomesticAgencyPolicy(RR.AgencyDomesticRequest request)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BLO.AgencyDomesticRequest domestic = _mapper.Map<RR.AgencyDomesticRequest, BLO.AgencyDomesticRequest>(request);
                    BLO.AgencyDomesticPolicyResponse result = _domesticHelpRepo.GetDomesticAgencyPolicy(domestic);
                    return _mapper.Map<BLO.AgencyDomesticPolicyResponse, RR.AgencyDomesticPolicyResponse>(result);
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                    return new RR.AgencyDomesticPolicyResponse()
                    {
                        IsTransactionDone = false,
                        TransactionErrorMessage = message
                    };
                }
            }
            catch (Exception ex)
            {
                return new RR.AgencyDomesticPolicyResponse()
                {
                    IsTransactionDone = false,
                    TransactionErrorMessage = ex.Message
                };
            }

        }

        /// <summary>
        /// Get domestic policies details by document number.
        /// </summary>
        /// <param name="domesticID">document number.</param>
        /// <param name="insuredCode">agent code.</param>
        /// <returns></returns>
        //[ApiAuthorize]
        [HttpGet]
        [Route(URI.DomesticURI.GetSavedQuoteDocumentNo)]
        public RR.DomesticHelpSavedQuotationResponse GetSavedQuotationPolicy(string documentNo, string agentCode)
        {
            try
            {
                BLO.DomesticHelpSavedQuotationResponse result = _domesticHelpRepo.GetSavedDomesticPolicy(documentNo, agentCode);
                return _mapper.Map<BLO.DomesticHelpSavedQuotationResponse, RR.DomesticHelpSavedQuotationResponse>(result);
            }
            catch (Exception ex)
            {
                return new RR.DomesticHelpSavedQuotationResponse
                {
                    IsTransactionDone = false,
                    TransactionErrorMessage = ex.Message
                };
            }
        }
        /// <summary>
        /// Calculate premium for the domestichelp.
        /// </summary>
        /// <param name="quoteRequest">Domestic quote request.</param>
        /// <returns>Domestic quote response.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route(URI.DomesticURI.GetQuote)]
        public RR.DomesticHelpQuoteResponse GetQuote(RR.DomesticHelpQuote quoteRequest)
        {
            try
            {
                BLO.DomesticHelpQuote quote = _mapper.Map<RR.DomesticHelpQuote, BLO.DomesticHelpQuote>(quoteRequest);
                BLO.DomesticHelpQuoteResponse result = _domesticHelpRepo.GetDomesticHelpQuote(quote);
                return _mapper.Map<BLO.DomesticHelpQuoteResponse, RR.DomesticHelpQuoteResponse>(result);
            }
            catch (Exception ex)
            {
                return new RR.DomesticHelpQuoteResponse
                {
                    IsTransactionDone = false,
                    TransactionErrorMessage = ex.Message
                };
            }
        }

        /// <summary>
        /// Insert the domestic help policy.
        /// </summary>
        /// <param name="policydetails">Domestic policy details.</param>
        /// <returns>Domestic policy response with domestic id, documentnumber and HIR status.</returns>
        [HttpPost]
        [Route(URI.DomesticURI.PostQuote)]
        public RR.DomesticHelpPolicyResponse PostDomesticPolicy(RR.DomesticPolicyDetails policydetails)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    BLO.DomesticPolicyDetails policy = new BLO.DomesticPolicyDetails();
                    policy.DomesticHelp.Agency = policydetails.DomesticHelp.Agency;
                    policy.DomesticHelp.AgentCode = policydetails.DomesticHelp.AgentCode;
                    policy.DomesticHelp.AgentBranch = policydetails.DomesticHelp.AgentBranch;
                    policy.DomesticHelp.MainClass = policydetails.DomesticHelp.MainClass;
                    policy.DomesticHelp.SubClass = policydetails.DomesticHelp.SubClass;
                    policy.DomesticHelp.DomesticID = policydetails.DomesticHelp.DomesticID;
                    policy.DomesticHelp.InsuredCode = policydetails.DomesticHelp.InsuredCode;
                    policy.DomesticHelp.InsuredName = policydetails.DomesticHelp.InsuredName;
                    policy.DomesticHelp.CPR = policydetails.DomesticHelp.CPR;
                    policy.DomesticHelp.InsurancePeroid = policydetails.DomesticHelp.InsurancePeroid;
                    policy.DomesticHelp.NoOfDomesticWorkers = policydetails.DomesticHelp.NoOfDomesticWorkers;
                    policy.DomesticHelp.PolicyStartDate = policydetails.DomesticHelp.PolicyStartDate;
                    policy.DomesticHelp.DomesticWorkType = policydetails.DomesticHelp.DomesticWorkType;
                    policy.DomesticHelp.IsPhysicalDefect = policydetails.DomesticHelp.IsPhysicalDefect;
                    policy.DomesticHelp.PhysicalDefectDescription = policydetails.DomesticHelp.PhysicalDefectDescription;
                    policy.DomesticHelp.CreatedBy = policydetails.DomesticHelp.CreatedBy;
                    policy.DomesticHelp.AuthorizedBy = policydetails.DomesticHelp.IsActivePolicy == true ? policydetails.DomesticHelp.CreatedBy : 0;
                    policy.DomesticHelp.Mobile = policydetails.DomesticHelp.Mobile;
                    policy.DomesticHelp.IsSaved = policydetails.DomesticHelp.IsSaved;
                    policy.DomesticHelp.IsActivePolicy = policydetails.DomesticHelp.IsActivePolicy;
                    policy.DomesticHelp.CommissionAmount = policydetails.DomesticHelp.CommissionAmount;
                    policy.DomesticHelp.Remarks = policydetails.DomesticHelp.Remarks;
                    policy.DomesticHelp.AccountNumber = policydetails.DomesticHelp.AccountNumber;
                    policy.DomesticHelp.PaymentType = policydetails.DomesticHelp.PaymentType;
                    policy.DomesticHelp.CommissionAfterDiscount = policydetails.DomesticHelp.CommissionAfterDiscount;
                    policy.DomesticHelp.PremiumAfterDiscount = policydetails.DomesticHelp.PremiumAfterDiscount;
                    policy.DomesticHelp.UserChangedPremium = policydetails.DomesticHelp.UserChangedPremium;
                    policy.DomesticHelpMemberdt = GetDomesticHelpMembers(policydetails, policy);
                    BLO.DomesticHelpPolicyResponse result = _domesticHelpRepo.PostDomesticPolicy(policy);
                    return _mapper.Map<BLO.DomesticHelpPolicyResponse, RR.DomesticHelpPolicyResponse>(result);
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                    return new RR.DomesticHelpPolicyResponse()
                    {
                        IsTransactionDone = false,
                        TransactionErrorMessage = message
                    };
                }
            }
            catch(Exception ex)
            {
                return new RR.DomesticHelpPolicyResponse()
                {
                    IsTransactionDone = false,
                    TransactionErrorMessage = ex.Message
                };
            }
        } 

        #region Unused Methods

        /// <summary>
        /// Get domestic policies details by domestic id.
        /// </summary>
        /// <param name="domesticID">domestic id</param>
        /// <param name="insuredCode">insured code</param>
        /// <returns></returns>
        //[ApiAuthorize]
        [HttpGet]
        [Route(URI.DomesticURI.GetSavedQuote)]
        public RR.DomesticHelpSavedQuotationResponse GetSavedQuotation(int domesticID, string insuredCode)
        {
            try
            {
                BLO.DomesticHelpSavedQuotationResponse result = _domesticHelpRepo.GetSavedDomesticHelp(domesticID, insuredCode);

                RR.DomesticHelpPolicy policydetail = new RR.DomesticHelpPolicy();
                policydetail.DomesticID = result.DomesticHelp.DomesticID;
                policydetail.InsuredCode = result.DomesticHelp.InsuredCode;
                policydetail.InsurancePeroid = result.DomesticHelp.InsurancePeroid;
                policydetail.NoOfDomesticWorkers = result.DomesticHelp.NoOfDomesticWorkers;
                policydetail.PolicyStartDate = result.DomesticHelp.PolicyStartDate;
                policydetail.PolicyExpiryDate = result.DomesticHelp.PolicyExpiryDate;
                policydetail.IsPhysicalDefect = result.DomesticHelp.IsPhysicalDefect;
                policydetail.PhysicalDefectDescription = result.DomesticHelp.PhysicalDefectDescription;
                policydetail.CPR = result.DomesticHelp.CPR;
                policydetail.FullName = result.DomesticHelp.FullName;
                policydetail.PremiumAfterDiscount = result.DomesticHelp.PremiumAfterDiscount;
                policydetail.PremiumBeforeDiscount = result.DomesticHelp.PremiumBeforeDiscount;
                policydetail.DocumentNo = result.DomesticHelp.DocumentNo;
                policydetail.SumInsured = result.DomesticHelp.SumInsured;
                policydetail.DomesticWorkType = result.DomesticHelp.DomesticWorkType;
                policydetail.IsHIR = result.DomesticHelp.IsHIR;

                List<RR.DomesticHelpMember> membersList = new List<RR.DomesticHelpMember>();
                if (result.DomesticHelpMemberList.Count > 0)
                {
                    foreach (var mem in result.DomesticHelpMemberList)
                    {
                        RR.DomesticHelpMember member = new RR.DomesticHelpMember();
                        member.InsuredCode = mem.InsuredCode;
                        member.SumInsured = mem.SumInsured;
                        member.PremiumAmount = mem.PremiumAmount;
                        member.OtherOccupation = mem.OtherOccupation;
                        member.DateOfSubmission = mem.DateOfSubmission;
                        member.CommencementDate = mem.CommencementDate;
                        member.ExpiryDate = mem.ExpiryDate;
                        member.Name = mem.Name;
                        member.Sex = mem.Sex;
                        member.DOB = mem.DOB;
                        member.Nationality = mem.Nationality;
                        member.CPRNumber = mem.CPRNumber;
                        member.Occupation = mem.Occupation;
                        member.ItemserialNo = mem.ItemserialNo;
                        member.AddressType = mem.AddressType;
                        member.Passport = mem.Passport;
                        membersList.Add(member);
                    }
                }

                return new RR.DomesticHelpSavedQuotationResponse
                {
                    IsTransactionDone = result.IsTransactionDone,
                    DomesticHelp = policydetail,
                    DomesticHelpMemberList = membersList
                };
            }
            catch (Exception ex)
            {
                return new RR.DomesticHelpSavedQuotationResponse
                {
                    IsTransactionDone = false,
                    TransactionErrorMessage = ex.Message
                };
            }
        }

        [ApiAuthorize]
        [HttpPost]
        [Route(URI.DomesticURI.UpdateDomesticDetails)]
        public RR.UpdateDomesticInsuranceDetailsResponse UpdateDomesticDetails(RR.UpdateDomesticInsuranceDetailsRequest pupdateDetails)
        {
            try
            {
                BLO.UpdateDomesticInsuranceDetailsRequest policy = new BLO.UpdateDomesticInsuranceDetailsRequest();
                policy.DomesticHelp.InsuredCode = pupdateDetails.DomesticHelp.InsuredCode;
                policy.DomesticHelp.InsuredName = pupdateDetails.DomesticHelp.InsuredName;
                policy.DomesticHelp.Agency = pupdateDetails.DomesticHelp.Agency;
                policy.DomesticHelp.AgentCode = pupdateDetails.DomesticHelp.AgentCode;
                policy.DomesticHelp.CPR = pupdateDetails.DomesticHelp.CPR;
                policy.DomesticHelp.InsurancePeroid = pupdateDetails.DomesticHelp.InsurancePeroid;
                policy.DomesticHelp.NoOfDomesticWorkers = pupdateDetails.DomesticHelp.NoOfDomesticWorkers;
                policy.DomesticHelp.PolicyStartDate = pupdateDetails.DomesticHelp.PolicyStartDate;
                policy.DomesticHelp.UpdatedBy = pupdateDetails.DomesticHelp.UpdatedBy;
                policy.DomesticHelp.Mobile = pupdateDetails.DomesticHelp.Mobile;
                policy.DomesticHelp.IsSaved = pupdateDetails.DomesticHelp.IsSaved;
                policy.DomesticHelp.LoadAmount = pupdateDetails.DomesticHelp.LoadAmount;
                policy.DomesticHelp.DiscountAmount = pupdateDetails.DomesticHelp.DiscountAmount;
                policy.DomesticHelp.Remarks = pupdateDetails.DomesticHelp.Remarks;
                policy.DomesticHelp.PremiumAfterDiscount = pupdateDetails.DomesticHelp.PremiumAfterDiscount;
                policy.DomesticHelp.PremiumBeforeDiscount = pupdateDetails.DomesticHelp.PremiumBeforeDiscount;
                policy.DomesticHelp.DomesticID = pupdateDetails.DomesticHelp.DomesticID;
                policy.DomesticHelp.DocumentNo = pupdateDetails.DomesticHelp.DocumentNo;
                policy.DomesticHelp.IsPhysicalDefect = pupdateDetails.DomesticHelp.IsPhysicalDefect;
                policy.DomesticHelp.CommissionAmount = pupdateDetails.DomesticHelp.CommissionAmount;
                policy.DomesticHelp.PolicyIssueDate = pupdateDetails.DomesticHelp.PolicyIssueDate;
                policy.DomesticHelp.PolicyExpiryDate = pupdateDetails.DomesticHelp.PolicyExpiryDate;
                policy.DomesticHelp.AccountNumber = pupdateDetails.DomesticHelp.AccountNumber;
                policy.DomesticHelp.PaymentType = pupdateDetails.DomesticHelp.PaymentType;
                policy.DomesticHelp.IsActivePolicy = pupdateDetails.DomesticHelp.IsActivePolicy;
                policy.DomesticHelp.UserChangedPremium = pupdateDetails.DomesticHelp.UserChangedPremium;
                policy.DomesticHelp.CommissionAfterDiscount = pupdateDetails.DomesticHelp.CommissionAfterDiscount;
                policy.DomesticHelp.PremiumAfterDiscount = pupdateDetails.DomesticHelp.PremiumAfterDiscount;

                if (pupdateDetails.DomesticHelpMemberList.Count > 0)
                {
                    DataTable member = new DataTable();
                    member.Columns.Add("INSUREDCODE", typeof(string));
                    member.Columns.Add("INSUREDNAME", typeof(string));
                    member.Columns.Add("SUMINSURED", typeof(decimal));
                    member.Columns.Add("PREMIUMAMOUNT", typeof(decimal));
                    member.Columns.Add("EXPIRYDATE", typeof(DateTime));
                    member.Columns.Add("ADDRESS1", typeof(string));
                    member.Columns.Add("OCCUPATION", typeof(string));
                    member.Columns.Add("NATIONALITY", typeof(string));
                    member.Columns.Add("PASSPORT", typeof(string));
                    member.Columns.Add("DOB", typeof(DateTime));
                    member.Columns.Add("SEX", typeof(char));
                    member.Columns.Add("ITEMSERIALNO", typeof(int));
                    member.Columns.Add("MAINCLASS", typeof(string));
                    member.Columns.Add("SUBCLASS", typeof(string));
                    member.Columns.Add("DATEOFSUBMISSION", typeof(DateTime));
                    member.Columns.Add("COMMENCEDATE", typeof(DateTime));
                    member.Columns.Add("IDENTITYNO", typeof(string));
                    member.Columns.Add("OCCUPATIONOTHER", typeof(string));
                    member.Columns.Add("CREATEDBY", typeof(int));
                    member.Columns.Add("CREATEDDATE", typeof(string));

                    foreach (var members in pupdateDetails.DomesticHelpMemberList)
                    {
                        member.Rows.Add(policy.DomesticHelp.InsuredCode, policy.DomesticHelp.InsuredName, 5000, policy.DomesticHelp.PremiumBeforeDiscount, null,
                            members.AddressType, members.Occupation, members.Nationality, members.Passport, members.DOB,
                            members.Sex, members.ItemserialNo, "", "", null, policy.DomesticHelp.PolicyStartDate, members.CPRNumber, "",
                              policy.DomesticHelp.CreatedBy, null);
                    }

                    policy.DomesticHelpMemberdt = member;
                }

                BLO.UpdateDomesticInsuranceDetailsResponse result = _domesticHelpRepo.UpdateDomesticDetails(policy);
                return new RR.UpdateDomesticInsuranceDetailsResponse
                {
                    IsHIR = result.IsHIR,
                    IsTransactionDone = result.IsTransactionDone,
                    TransactionErrorMessage = result.TransactionErrorMessage,
                    PaymentTrackId = result.PaymentTrackId
                };
            }
            catch (Exception ex)
            {
                return new RR.UpdateDomesticInsuranceDetailsResponse { TransactionErrorMessage = ex.Message };
            }
        }    
        #endregion Unused Methods

        #region PrivateMethod
        private DataTable GetDomesticHelpMembers(RR.DomesticPolicyDetails policyDetails, BLO.DomesticPolicyDetails policy)
        {
            DataTable member = new DataTable();
            if (policyDetails.DomesticHelpMemberList.Count > 0)
            {

                member.Columns.Add("INSUREDCODE", typeof(string));
                member.Columns.Add("INSUREDNAME", typeof(string));
                member.Columns.Add("SUMINSURED", typeof(decimal));
                member.Columns.Add("PREMIUMAMOUNT", typeof(decimal));
                member.Columns.Add("EXPIRYDATE", typeof(DateTime));
                member.Columns.Add("ADDRESS1", typeof(string));
                member.Columns.Add("OCCUPATION", typeof(string));
                member.Columns.Add("NATIONALITY", typeof(string));
                member.Columns.Add("PASSPORT", typeof(string));
                member.Columns.Add("DOB", typeof(DateTime));
                member.Columns.Add("SEX", typeof(char));
                member.Columns.Add("ITEMSERIALNO", typeof(int));
                member.Columns.Add("MAINCLASS", typeof(string));
                member.Columns.Add("SUBCLASS", typeof(string));
                member.Columns.Add("DATEOFSUBMISSION", typeof(DateTime));
                member.Columns.Add("COMMENCEDATE", typeof(DateTime));
                member.Columns.Add("IDENTITYNO", typeof(string));
                member.Columns.Add("OCCUPATIONOTHER", typeof(string));
                member.Columns.Add("CREATEDBY", typeof(int));
                member.Columns.Add("CREATEDDATE", typeof(string));

                foreach (var members in policyDetails.DomesticHelpMemberList)
                {
                    member.Rows.Add(policy.DomesticHelp.InsuredCode, members.Name, 0, 0, null,
                        members.AddressType, members.Occupation, members.Nationality, members.Passport, members.DOB,
                        members.Sex, members.ItemserialNo, "", "", null, policy.DomesticHelp.PolicyStartDate, members.CPRNumber, "",
                          policy.DomesticHelp.CreatedBy, null);
                }
            }
            return member;
        }
        #endregion
    }
}