﻿namespace BKIC.SellingPoint.DL.StoredProcedures
{
    public class BKICDropDowns
    {
        public const string GetPageDropDowns = "FetchDropDown";
        public const string GetVehicleModel = "getModelsForMakeRequest";
        public const string GetVehicleBody = "getModelBodyRequest";
    }
}