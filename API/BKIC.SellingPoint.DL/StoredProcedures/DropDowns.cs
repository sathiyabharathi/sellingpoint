﻿using BKIC.SellingPoint.DL.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BKIC.SellingPoint.DL.StoredProcedures
{
    public class DropDownSP
    {
        public const string FetchDropdowns = "SP_FetchDropDowns";

        public const string FetchAgencyProducts = "SP_FetchAgencyProducts";

        public const string FetchInsuranceProductCode = "SP_FetchInsuranceProductCode";
    }
}
