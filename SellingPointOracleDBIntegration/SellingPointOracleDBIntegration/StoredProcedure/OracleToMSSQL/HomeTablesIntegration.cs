﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SellingPoint.OracleDBIntegration.StoredProcedure.OracleToMSSQL
{
    public class HomeTablesIntegration
    {
        public const string HomeDetailsIntegration = "MIG_HomeDetailsToMSSQL";
        public const string RenewalHomeInsuranceIntegration = "MIG_RenewalHomeDetailsToMSSQL";
    }
}
