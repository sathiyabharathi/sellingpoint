﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SellingPoint.OracleDBIntegration.StoredProcedure.MSSQLToOracle
{
    public class MotorInsurance
    {
        public const string IntegrateMotorInsurance = "MIG_IntegrateMotorDetails";
        public const string PushOracleMotor = "MoveMotorPolicy";
    }
}
