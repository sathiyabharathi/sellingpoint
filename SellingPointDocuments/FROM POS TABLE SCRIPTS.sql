
------------------------------------------ MOTOR -----------------------------------------------

CREATE TABLE FROMPOS_MOTOR
(
	LINKID							VARCHAR2(30),
	DOCUMENTNO						VARCHAR2(30),
	INSUREDCODE						VARCHAR2(30),
	INSUREDNAME						VARCHAR2(500),
	VEHICLETYPE						VARCHAR2(30),
	MAKE							VARCHAR2(30),
	MODEL							VARCHAR2(30),
	BODY							VARCHAR2(30),
	MAKEYEAR						NUMBER(4),
	TONNAGE							NUMBER(21,3),
	REGISTRATIONNO					VARCHAR2(30),
	CHASSISNO						VARCHAR2(30),
	PURPOSEFUSE						VARCHAR2(30),
	VEHICLEVALUE					NUMBER(21,3),
	PREMIUMAMOUNT					NUMBER(21,3),
	FINANCECOMPANY					VARCHAR2(30),
	FINANCECOMPANYDESCRIPTION		VARCHAR2(30),
	MAINCLASS						VARCHAR2(30),
	SUBCLASS						VARCHAR2(30),
	COMMENCEDATE					DATE,
	EXPIRYDATE						DATE,
	EXCESSTYPE						VARCHAR2(30),
	EXCESSAMOUNT					NUMBER(21,3),
	RATE							NUMBER(18,6),
	BASEPREMIUM						NUMBER(21,3),
	CREATEBY						VARCHAR2(30),
	CREATEDT						DATE,
	CLAIMAMOUNT						NUMBER(21,3),
	PREVIOUSDOCUMENTNO				VARCHAR2(30),
	RENEWED							VARCHAR2(1),
	RENEWALCOUNT					NUMBER(2),
	LASTYEARSUMINSURED				NUMBER(21,3),
	LASTYEARPREMIUMAMOUNT			NUMBER(21,3),
	LASTYEAREXPIRYDATE				DATE,
	REMARKS							VARCHAR2(4000),
	FFPNUMBER						VARCHAR2(30),
	CPR								VARCHAR2(30),
	PAYMENTDATE						DATE,
	PAYMENT_METHOD					VARCHAR2(30),
	ACCOUNTNO						VARCHAR2(30),
	MOBILENUMBER					VARCHAR2(30),
	SOURCE							VARCHAR2(30),
	DISCOUNTPERCENT					NUMBER(21,3),
	DISCOUNTAMOUNT					NUMBER(21,3),
	COMMISSIONAMOUNT				NUMBER(21,3),
	COMMISSIONBEFOREDISCOUNT		NUMBER(21,3),
	PREMIUMBEFOREDISCOUNT			NUMBER(21,3),
	AGENCYCODE						VARCHAR2(30),
	CUSTOMERCODE					VARCHAR2(30),
	AGENTCODE						VARCHAR2(30),
	AGENCYBRANCH					VARCHAR2(30),
	INTRODUCEDBY					VARCHAR2(30)
)
/

CREATE PUBLIC SYNONYM FROMPOS_MOTOR FOR FROMPOS_MOTOR
/
GRANT All ON FROMPOS_MOTOR TO PUBLIC 
/

CREATE TABLE FROMPOS_MOTOR_COVERS 
(	
	LINKID 							VARCHAR2(30),
	DOCUMENTNO 						VARCHAR2(30),
	COVERCODE 						VARCHAR2(30),
	COVERAMOUNT 					NUMBER(21,3),
	CLAUSECODE 						VARCHAR2(30),
	TYPE 							VARCHAR2(30),
	TYPESERIALNO 					NUMBER(3),
	COVERCODEDESCRIPTION 			VARCHAR2(100)
)
/

CREATE PUBLIC SYNONYM FROMPOS_MOTOR_COVERS FOR FROMPOS_MOTOR_COVERS
/
GRANT ALL ON FROMPOS_MOTOR_COVERS TO PUBLIC 
/


CREATE TABLE FROMPOS_MOTOR_LOADS
(	
	LINKID 							VARCHAR2(30),
	DOCUMENTNO 						VARCHAR2(30),
	LOADCODE 						VARCHAR2(30),
	LOADPERCENT 					NUMBER(21,3),
	LOADDESCRIPTION 				VARCHAR2(400),
	LOADAMOUNT 						NUMBER(21,3),
	TRANSACTIONBASEPREMIUM 			NUMBER(21,3)
)
/

CREATE PUBLIC SYNONYM FROMPOS_MOTOR_LOADS FOR FROMPOS_MOTOR_LOADS
/
GRANT ALL ON FROMPOS_MOTOR_LOADS TO PUBLIC 
/


CREATE TABLE FROMPOS_CATEGORY
(
	LINKID							VARCHAR2(30),
	DOCUMENTNO						VARCHAR2(30),
	DOCUMENTTYPE					VARCHAR2(30),
	ENDORSEMENTNO					VARCHAR2(30),
	ENDORSEMENTCOUNT				NUMBER(3),
	AGENTCODE						VARCHAR2(30),
	LINENO							NUMBER(4),
	CATEGORY						VARCHAR2(30),
	CODE							VARCHAR2(30),
	VALUETYPE						VARCHAR2(30),
	VALUE							NUMBER(21,3),
	PREMIUM							NUMBER(21,3),
	CALCULATEDVALUE					NUMBER(21,3),
	PARENTLINKID					VARCHAR2(30),
	AGENCY							VARCHAR2(30),
	UPDATEDVALUE					NUMBER(21,3)
)
/

CREATE PUBLIC SYNONYM FROMPOS_CATEGORY FOR FROMPOS_CATEGORY
/
GRANT ALL ON FROMPOS_CATEGORY TO PUBLIC 
/

------------------------------------------ TRAVEL -----------------------------------------------

CREATE TABLE FROMPOS_TRAVEL
(	
	LINKID 							VARCHAR2(30), 
	DOCUMENTNO 						VARCHAR2(30), 
	INSUREDCODE 					VARCHAR2(30), 
	INSUREDNAME 					VARCHAR2(100), 
	SUMINSURED 						NUMBER, 
	PREMIUMAMOUNT 					NUMBER, 
	COMMENCEDATE 					DATE, 
	EXPIRYDATE						DATE, 
	CREATEBY 						VARCHAR2(30), 
	CREATEDT 						DATE,
	MAINCLASS 						VARCHAR2(30), 
	SUBCLASS 						VARCHAR2(30), 
	DOCUMENTDATE 					DATE, 
	IDENTITYNO 						VARCHAR2(30), 
	PASSPORT 						VARCHAR2(30), 
	RENEWAL 						VARCHAR2(1), 
	PREVIOUSDOCUMENTNO 				VARCHAR2(30), 
	OCCUPATION 						VARCHAR2(30), 
	RENEWED 						VARCHAR2(30), 
	PERIODOFCOVER 					VARCHAR2(30), 
	FFPNUMBER 						VARCHAR2(30), 
	RENEWALCOUNT 					NUMBER, 
	LASTYEARSUMINSURED 				NUMBER, 
	LASTYEARPREMIUMAMOUNT 			NUMBER, 
	LASTYEAREXPIRYDATE 				DATE, 
	LOADAMOUNT 						NUMBER, 
	REMARKS 						VARCHAR2(1000), 
	TRANSACTION_NO 					VARCHAR2(50), 
	PAYMENTDATE 					DATE, 
	PAYMENT_TYPE 					VARCHAR2(50), 
	PAYMENT_AUTHORIZATION_CODE 		VARCHAR2(50), 
	MOBILENUMBER 					VARCHAR2(50), 
	SOURCE 							VARCHAR2(30),
	DISCOUNTPERCENT 				NUMBER(21,3), 
	DISCOUNTAMOUNT					NUMBER(21,3),
	COMMISSIONAMOUNT				NUMBER(21,3),
	COMMISSIONBEFOREDISCOUNT		NUMBER(21,3),
	PREMIUMBEFOREDISCOUNT			NUMBER(21,3),
	AGENCYCODE						VARCHAR2(30),
	CUSTOMERCODE					VARCHAR2(30),
	AGENTCODE						VARCHAR2(30),
	AGENCYBRANCH					VARCHAR2(30),
	INTRODUCEDBY					VARCHAR2(30)
)
/

CREATE PUBLIC SYNONYM FROMPOS_TRAVEL FOR FROMPOS_TRAVEL
/
GRANT All ON FROMPOS_TRAVEL TO PUBLIC 
/

CREATE TABLE FROMPOS_TRAVEL_MEMBERS
(	
	LINKID 							VARCHAR2(30), 
	DOCUMENTNO 						VARCHAR2(30), 
	ITEMSERIALNO 					NUMBER, 
	ITEMNAME 						VARCHAR2(100), 
	SUMINSURED 						NUMBER, 
	FOREIGNSUMINSURED 				NUMBER, 
	CATEGORY 						VARCHAR2(30), 
	TITLE 							VARCHAR2(30), 
	SEX 							VARCHAR2(1), 
	DATEOFBIRTH 					VARCHAR2(100), 
	AGE 							NUMBER, 
	PREMIUMAMOUNT 					NUMBER, 
	MAKE 							VARCHAR2(30), 
	OCCUPATIONCODE 					VARCHAR2(30), 
	IDENTITYNO 						VARCHAR2(30), 
	PASSPORT 						VARCHAR2(30), 
	FIRSTNAME 						VARCHAR2(100), 
	MIDDLENAME 						VARCHAR2(30), 
	LASTNAME 						VARCHAR2(30)
)
/

CREATE PUBLIC SYNONYM FROMPOS_TRAVEL_MEMBERS FOR FROMPOS_TRAVEL_MEMBERS
/
GRANT All ON FROMPOS_TRAVEL_MEMBERS TO PUBLIC 
/

CREATE TABLE FROMPOS_QUESTIONNAIRE
(	
	LINKID 							VARCHAR2(30), 
	DOCUMENTNO 						VARCHAR2(30), 
	TYPE 							VARCHAR2(30), 
	CODE 							VARCHAR2(30), 
	DESCRIPTION 					VARCHAR2(4000), 
	ANSWER 							VARCHAR2(30), 
	REFERRAL 						VARCHAR2(30), 
	REMARKS 						VARCHAR2(4000), 
	ITEMSERIALNO 					VARCHAR2(30), 
	ITEMNAME 						VARCHAR2(30), 
	CPR 							VARCHAR2(30), 
	EXISTINGILLNESSNAME 			VARCHAR2(1000), 
	SUFFERINGSINCE 					VARCHAR2(100), 
	MEDICALCONDITION 				VARCHAR2(1000), 
	STATUS 							VARCHAR2(30)
)
/

CREATE PUBLIC SYNONYM FROMPOS_QUESTIONNAIRE FOR FROMPOS_QUESTIONNAIRE
/
GRANT All ON FROMPOS_QUESTIONNAIRE TO PUBLIC 
/
-------------------------------------------------------- HOME ------------------------------------------------------------
CREATE TABLE FROMPOS_DOCUMENT_FIRE_DET 
(	
	LINKID VARCHAR2(30), 
	DOCUMENTNO VARCHAR2(30), 
	SUMINSURED NUMBER(21,3), 
	PREMIUMAMOUNT NUMBER(21,3), 
	FINANCECOMPANYID VARCHAR2(30), 
	TYPE VARCHAR2(30), 
	ADDRESS1 VARCHAR2(100), 
	ADDRESS2 VARCHAR2(100), 
	CITYCODE VARCHAR2(30), 
	STREETNO VARCHAR2(30), 
	BLOCKNUMBER VARCHAR2(30), 
	BUILDINGNAME VARCHAR2(30), 
	STREETNAME VARCHAR2(30), 
	DOMESTICHELP VARCHAR2(1), 
	AGEOFBUILDING NUMBER(2,0), 
	CREATEBY VARCHAR2(30), 
	CREATEDT VARCHAR2(100), 
	MAINCLASS VARCHAR2(30), 
	SUBCLASS VARCHAR2(30), 
	COMMENCEDATE VARCHAR2(100), 
	EXPIRYDATE VARCHAR2(100), 
	BUILDINGNO VARCHAR2(30), 
	FLATNO VARCHAR2(30), 
	RSMDCOVER VARCHAR2(1), 
	INSUREDCODE VARCHAR2(30), 
	INSUREDNAME VARCHAR2(50), 
	RENEWAL VARCHAR2(1), 
	PREVIOUSDOCUMENTNO VARCHAR2(30), 
	RENEWED VARCHAR2(1), 
	CLAIMAMOUNT NUMBER, 
	JEWELLERYCOVER VARCHAR2(30), 
	FFPNUMBER VARCHAR2(30), 
	TRANSACTION_NO 					VARCHAR2(30), 
	PAYMENT_TYPE 					VARCHAR2(30), 
	PAYMENT_AUTHORIZATION_CODE 		VARCHAR2(30), 
	ORIGINALPREMIUMAMOUNT 			NUMBER, 
	MOBILENUMBER 					VARCHAR2(30),
	SOURCE							VARCHAR2(30),
	DISCOUNTPERCENT					NUMBER(21,3),
	DISCOUNTAMOUNT					NUMBER(21,3),
	COMMISSIONAMOUNT				NUMBER(21,3),
	COMMISSIONBEFOREDISCOUNT		NUMBER(21,3),
	PREMIUMBEFOREDISCOUNT			NUMBER(21,3),
	AGENCYCODE						VARCHAR2(30),
	CUSTOMERCODE					VARCHAR2(30),
	AGENTCODE						VARCHAR2(30),
	AGENCYBRANCH					VARCHAR2(30),
	INTRODUCEDBY					VARCHAR2(30)
);

CREATE PUBLIC SYNONYM FROMPOS_DOCUMENT_FIRE_DET FOR FROMPOS_DOCUMENT_FIRE_DET
/
GRANT All ON FROMPOS_DOCUMENT_FIRE_DET TO PUBLIC 
/

CREATE TABLE FROMPOS_DOCUMENT_ITEMS 
   (	LINKID VARCHAR2(30), 
	DOCUMENTNO VARCHAR2(30), 
	ITEMSERIALNO NUMBER(4,0), 
	ITEMCODE VARCHAR2(30), 
	ITEMNAME VARCHAR2(50), 
	FORMULATYPE VARCHAR2(30), 
	FORMULAID VARCHAR2(30), 
	SUMINSURED NUMBER(21,3), 
	PREMIUMAMOUNT NUMBER(21,3), 
	PREMIUMRATE NUMBER(14,8), 
	LIMITVALUE NUMBER(21,3), 
	LIMITRATE NUMBER(14,8), 
	DISPLAYITEMSERIALNO NUMBER(4,0)
   );
CREATE PUBLIC SYNONYM FROMPOS_DOCUMENT_ITEMS FOR FROMPOS_DOCUMENT_ITEMS
/
GRANT All ON FROMPOS_DOCUMENT_ITEMS TO PUBLIC 
/
   
CREATE TABLE FROMPOS_DOCUMENT_SUBITEMS 
   (	LINKID VARCHAR2(30), 
	DOCUMENTNO VARCHAR2(30), 
	ITEMSERIALNO NUMBER(4,0), 
	ITEMCODE VARCHAR2(30), 
	ITEMNAME VARCHAR2(50), 
	SUBITEMSERIALNO NUMBER(4,0), 
	SUBITEMCODE VARCHAR2(30), 
	SUBITEMNAME VARCHAR2(50), 
	DESCRIPTION VARCHAR2(400), 
	SUMINSURED NUMBER(21,3), 
	REMARKS VARCHAR2(400)
   ) 
CREATE PUBLIC SYNONYM FROMPOS_DOCUMENT_SUBITEMS FOR FROMPOS_DOCUMENT_SUBITEMS
/
GRANT All ON FROMPOS_DOCUMENT_SUBITEMS TO PUBLIC 
/
  
 CREATE TABLE FROMPOS_HOMEDOMESTIC_COVER 
   (	LINKID VARCHAR2(30), 
	DOCUMENTNO VARCHAR2(30), 
	LINENO NUMBER(4,0), 
	SERIALNO NUMBER(4,0), 
	ITEMSERIALNO NUMBER(4,0), 
	ITEMCODE VARCHAR2(30), 
	ITEMNAME VARCHAR2(50), 
	MEMBERSERIALNO NUMBER(4,0), 
	NAME VARCHAR2(30), 
	CPRNUMBER VARCHAR2(30), 
	TITLE VARCHAR2(5), 
	SEX VARCHAR2(1), 
	AGE NUMBER(2,0), 
	DATEOFBIRTH VARCHAR2(100), 
	SUMINSURED NUMBER(21,3), 
	PREMIUMAMOUNT NUMBER(21,3)
   ) 
CREATE PUBLIC SYNONYM FROMPOS_HOMEDOMESTIC_COVER FOR FROMPOS_HOMEDOMESTIC_COVER
/
GRANT All ON FROMPOS_HOMEDOMESTIC_COVER TO PUBLIC 
/

-------------------------------------------------------- DOMESTIC HELP -----------------------------------------------------------

CREATE TABLE FROMPOS_DOMESTICHELP 
   (	LINKID VARCHAR2(30), 
	DOCUMENTNO VARCHAR2(30), 
	INSUREDCODE VARCHAR2(30), 
	INSUREDNAME VARCHAR2(50), 
	SUMINSURED NUMBER, 
	PREMIUMAMOUNT NUMBER, 
	EXPIRYDATE VARCHAR2(100), 
	REMARKS VARCHAR2(100), 
	TYPE VARCHAR2(30), 
	ADDRESS1 VARCHAR2(400), 
	MAINCLASS VARCHAR2(30), 
	SUBCLASS VARCHAR2(30), 
	DOCUMENTDATE VARCHAR2(100), 
	CREATEBY VARCHAR2(30), 
	CREATEDT VARCHAR2(100), 
	COMMENCEDATE VARCHAR2(100), 
	IDENTITYNO VARCHAR2(30), 
	RENEWAL VARCHAR2(1), 
	PREVIOUSDOCUMENTNO VARCHAR2(30), 
	RENEWED VARCHAR2(1), 
	INSURANCEPERIOD NUMBER, 
	TRANSACTION_NO VARCHAR2(30),
	SOURCE							VARCHAR2(30),
	DISCOUNTPERCENT					NUMBER(21,3),
	DISCOUNTAMOUNT					NUMBER(21,3),
	COMMISSIONAMOUNT				NUMBER(21,3),
	COMMISSIONBEFOREDISCOUNT		NUMBER(21,3),
	PREMIUMBEFOREDISCOUNT			NUMBER(21,3),
	AGENCYCODE						VARCHAR2(30),
	CUSTOMERCODE					VARCHAR2(30),
	AGENTCODE						VARCHAR2(30),
	AGENCYBRANCH					VARCHAR2(30),
	INTRODUCEDBY					VARCHAR2(30)
   ) 
CREATE PUBLIC SYNONYM FROMPOS_DOMESTICHELP FOR FROMPOS_DOMESTICHELP
/
GRANT All ON FROMPOS_DOMESTICHELP TO PUBLIC 
/

 CREATE TABLE FROMPOS_DOMESTIC_MEMBERS 
   (	LINKID VARCHAR2(30), 
	DOCUMENTNO VARCHAR2(30), 
	INSUREDCODE VARCHAR2(30), 
	INSUREDNAME VARCHAR2(30), 
	SUMINSURED NUMBER, 
	PREMIUMAMOUNT NUMBER, 
	EXPIRYDATE VARCHAR2(100), 
	REMARKS VARCHAR2(400), 
	TYPE VARCHAR2(30), 
	ADDRESS1 VARCHAR2(400), 
	OCCUPATION VARCHAR2(30), 
	NATIONALITY VARCHAR2(30), 
	PASSPORT VARCHAR2(30), 
	DOB VARCHAR2(100), 
	SEX VARCHAR2(30), 
	ITEMSERIALNO NUMBER, 
	MAINCLASS VARCHAR2(30), 
	SUBCLASS VARCHAR2(30), 
	DOCUMENTDATE VARCHAR2(100), 
	CREATEBY VARCHAR2(30), 
	CREATEDT VARCHAR2(100), 
	COMMENCEDATE VARCHAR2(100), 
	IDENTITYNO VARCHAR2(30), 
	OCCUPATIONOTHER VARCHAR2(30)
   ) 
CREATE PUBLIC SYNONYM FROMPOS_DOMESTIC_MEMBERS FOR FROMPOS_DOMESTIC_MEMBERS
/
GRANT All ON FROMPOS_DOMESTIC_MEMBERS TO PUBLIC 
/
