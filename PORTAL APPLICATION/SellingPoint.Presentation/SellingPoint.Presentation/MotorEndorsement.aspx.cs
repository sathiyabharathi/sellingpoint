﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using KBIC.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class MotorEndorsement : System.Web.UI.Page
    {
        private General master;
        public static List<BKIC.SellingPoint.DTO.RequestResponseWrappers.InsuredMasterDetails> InsuredNames { get; set; }
        public static List<BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyMotorPolicy> policyList;
        public static long _MotorEndorsementID { get; set; }
        public static string MainClass { get; set; }
        public static bool AjdustedPremium { get; set; }
        public static string SubClass { get; set; }
        public static string OldRegNumber { get; set; }
        public static string OldChassisNumber { get; set; }

        public MotorEndorsement()
        {
            master = Master as General;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!Page.IsPostBack)
            {
                amtDisplay.Visible = false;
                transferDiv.Visible = false;
                AddRemoveBankDiv.Visible = false;
                ChangeRegistrationNoDiv.Visible = false;
                CancelPolicyDiv.Visible = false;
                ExtendedPolicyDiv.Visible = false;
                //downloadschedule.Visible = false;
                //downloadCertificate.Visible = false;
                BindAgencyClientCodeDropdown();
                if (Request.QueryString["InsuredName"] != null && Request.QueryString["InsuredCode"] != null)
                {
                    txtNewClientCode.Text = Convert.ToString(Request.QueryString["InsuredCode"]);
                    txtNewInsuredName.Text = Convert.ToString(Request.QueryString["InsuredName"]);
                    transferDiv.Visible = true;
                    ddlEndorsementType.SelectedIndex = 1;
                }
                _MotorEndorsementID = 0;
            }
        }

        private void BindAgencyClientCodeDropdown()
        {
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            var dropDownResult = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                 <BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>
                                 (BKIC.SellingPont.DTO.Constants.DropdownURI.GetPageDropDowns.Replace("{type}",
                                 BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.DomesticHelp));

            if (dropDownResult.StatusCode == 200 && dropDownResult.Result.IsTransactionDone)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDownResult.Result.dropdownresult);
                DataTable branches = dropdownds.Tables["BranchMaster"];
                DataTable Financier = dropdownds.Tables["Financier"];

                if (branches != null && branches.Rows.Count > 0)
                {
                    ddlBranch.DataValueField = "AGENTBRANCH";
                    ddlBranch.DataTextField = "BranchName";
                    ddlBranch.DataSource = branches;
                    ddlBranch.DataBind();
                    ddlBranch.Items.Insert(0, new ListItem("--Please Select--", ""));
                }

                if(Financier != null && Financier.Rows.Count > 0)
                {                   

                    ddlBank.DataValueField = "Code";
                    ddlBank.DataTextField = "Financier";
                    ddlBank.DataSource = Financier;
                    ddlBank.DataBind();
                    ddlBank.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
            }
            var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest();
            req.AgentBranch = userInfo.AgentBranch;
            req.AgentCode = userInfo.AgentCode;

            var insuredResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                               <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredResponse>,
                               BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest>
                               (BKIC.SellingPoint.DTO.Constants.AdminURI.GetAgencyInsured, req);

            if (insuredResult.StatusCode == 200 && insuredResult.Result.IsTransactionDone && insuredResult.Result.AgencyInsured.Count > 0)
            {
                ddlCPR.DataSource = insuredResult.Result.AgencyInsured;
                ddlCPR.DataTextField = "CPR";
                ddlCPR.DataValueField = "InsuredCode";
                ddlCPR.DataBind();
                ddlCPR.Items.Insert(0, new ListItem("--Please Select--", ""));
                InsuredNames = insuredResult.Result.AgencyInsured;

                ddlNewInsuredCPR.DataSource = insuredResult.Result.AgencyInsured;
                ddlNewInsuredCPR.DataTextField = "CPR";
                ddlNewInsuredCPR.DataValueField = "InsuredCode";
                ddlNewInsuredCPR.DataBind();
                ddlNewInsuredCPR.Items.Insert(0, new ListItem("--Please Select--", ""));
                
            }
            txtIndroducedBy.Text = userInfo.UserName;
            ddlBranch.SelectedIndex = ddlBranch.Items.IndexOf(ddlBranch.Items.FindByValue(userInfo.AgentBranch));
            txtIndroducedBy.Text = userInfo.UserName;

            ListItem item0 = new ListItem("-- Please Select --", "-1");
            ListItem item1 = new ListItem("Transfer", "Transfer");
            ListItem item2 = new ListItem("AddRemoveBank", "AddRemoveBank");
            ListItem item3 = new ListItem("ChangeRegistration", "ChangeRegistration");
            ListItem item4 = new ListItem("Extended", "Extended");

            ddlEndorsementType.Items.Add(item0);
            ddlEndorsementType.Items.Add(item1);
            ddlEndorsementType.Items.Add(item2);
            ddlEndorsementType.Items.Add(item3);
            ddlEndorsementType.Items.Add(item4);

            if (userInfo.Roles.ToLower() == "superadmin")
            {
                ListItem item5 = new ListItem("CancelPolicy", "CancelPolicy");
                ListItem item6 = new ListItem("AddCovers", "AddCovers");
                ListItem item7 = new ListItem("PremiumChange", "PremiumChange");
                ListItem item8 = new ListItem("ChangeOfExcess", "ChangeOfExcess");
                ListItem item9 = new ListItem("ChangeSumInsured", "ChangeSumInsured");
                ListItem item10 = new ListItem("CorrectName", "CorrectName");

                ddlEndorsementType.Items.Add(item5);
                ddlEndorsementType.Items.Add(item6);
                ddlEndorsementType.Items.Add(item7);
                ddlEndorsementType.Items.Add(item8);
                ddlEndorsementType.Items.Add(item9);
                ddlEndorsementType.Items.Add(item10);

            }
        }

        protected void insured_Master(object sender, EventArgs e)
        {
            Response.Redirect("InsuredMaster.aspx?type=" + 5);
        }

        public Control GetContentControl()
        {
            MasterPage ctl00 = FindControl("ctl00") as MasterPage;
            ContentPlaceHolder MainContent = ctl00.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;
            return MainContent.FindControl("mainDiv");
        }

        protected void Changed_EndorsementType(object sender, EventArgs e)
        {
           // master.makeReadOnly(GetContentControl(), true);
            HidePremium();
            if (ddlEndorsementType.SelectedItem.Value == "Transfer")
            {
                transferDiv.Visible = true;
                AddRemoveBankDiv.Visible = false;
                ChangeRegistrationNoDiv.Visible = false;
                CancelPolicyDiv.Visible = false;
                ExtendedPolicyDiv.Visible = false;

                txtEffectiveFromDate.Enabled = false;
                txtEffectiveToDate.Enabled = false;
                AddInsured.Visible = true;
                PaymentSection.Visible = true;
               // master.makeReadOnly(GetContentControl(), false);
            }
            else if (ddlEndorsementType.SelectedItem.Value == "AddRemoveBank")
            {
                transferDiv.Visible = false;
                AddRemoveBankDiv.Visible = true;
                ChangeRegistrationNoDiv.Visible = false;
                CancelPolicyDiv.Visible = false;
                ExtendedPolicyDiv.Visible = false;

                txtEffectiveFromDate.Enabled = false;
                txtEffectiveToDate.Enabled = false;

                AddInsured.Visible = false;
                PaymentSection.Visible = false;

            }
            else if (ddlEndorsementType.SelectedItem.Value == "ChangeRegistration")
            {
                transferDiv.Visible = false;
                AddRemoveBankDiv.Visible = false;
                ChangeRegistrationNoDiv.Visible = true;
                CancelPolicyDiv.Visible = false;
                ExtendedPolicyDiv.Visible = false;
                txtOldRegistrationNo.Text = OldRegNumber;
                txtOldRegistrationNo.Enabled = false;

                txtEffectiveFromDate.Enabled = false;
                txtEffectiveToDate.Enabled = false;
                AddInsured.Visible = false;
                PaymentSection.Visible = true;

            }
            else if (ddlEndorsementType.SelectedItem.Value == "CancelPolicy")
            {
                transferDiv.Visible = false;
                AddRemoveBankDiv.Visible = false;
                ChangeRegistrationNoDiv.Visible = false;
                CancelPolicyDiv.Visible = true;
                ExtendedPolicyDiv.Visible = false;

                txtEffectiveFromDate.Enabled = false;
                txtEffectiveToDate.Enabled = true;              

                AddInsured.Visible = false;
                PaymentSection.Visible = false;
            }
            else if (ddlEndorsementType.SelectedItem.Value == "Extended")
            {
                transferDiv.Visible = false;
                AddRemoveBankDiv.Visible = false;
                ChangeRegistrationNoDiv.Visible = false;
                CancelPolicyDiv.Visible = false;
                ExtendedPolicyDiv.Visible = true;

                txtEffectiveFromDate.Enabled = false;
                txtEffectiveToDate.Enabled = true;

                AddInsured.Visible = false;
                PaymentSection.Visible = true;
            }
           // Page_CustomValidate();
        }
        protected void Changed_MotorPolicy(object sender, EventArgs e)
        {
            try
            {
                master.ShowLoading = true;
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();

                var request = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyDomesticRequest();
                request.AgentBranch = userInfo.AgentBranch;
                request.AgentCode = userInfo.AgentCode;
                request.Agency = userInfo.Agency;
                
                //List the previous endorsements for the policy.
                ListEndorsements(service, userInfo);

                //Get saved policy details by document(policy) number.
                var url = BKIC.SellingPoint.DTO.Constants.MotorURI.GetSavedQuoteDocumentNo
                          .Replace("{documentNo}", ddlMotorPolicies.SelectedItem.Text.Trim())
                          .Replace("{agentCode}", request.AgentCode);

                var motorDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorSavedQuotationResponse>>(url);

                //Update policy details on current page for dispaly the details.
                if (motorDetails.StatusCode == 200 && motorDetails.Result.IsTransactionDone)
                {
                    var response = motorDetails.Result.MotorPolicyDetails;

                    txtOldClientCode.Text = response.InsuredCode;
                    txtOldInsuredName.Text = response.InsuredName;
                    OldRegNumber = response.RegistrationNumber;
                    txtOldRegistrationNo.Text = OldRegNumber;
                    txtEffectiveFromDate.Text = response.PolicyCommencementDate.CovertToLocalFormat();
                    txtEffectiveToDate.Text = response.ExpiryDate.CovertToLocalFormat();
                    paidPremium.Value = Convert.ToString(response.PremiumAfterDiscount);
                    subClass.Value = response.Subclass;
                    SubClass = response.Subclass;
                    MainClass = response.Mainclass;
                    expireDate.Value = response.ExpiryDate.CovertToLocalFormat();
                    ddlBank.SelectedIndex = ddlBank.Items.IndexOf(ddlBank.Items.FindByValue(response.FinancierCompanyCode));
                }                
                Page_CustomValidate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        private void ListEndorsements(DataServiceManager service, OAuthTokenResponse userInfo)
        {
            if (userInfo == null)
                Response.Redirect("Login.aspx");

            if (ddlMotorPolicies.SelectedIndex > 0)
            {
                var motorEndoRequest = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndoRequest();
                motorEndoRequest.Agency = userInfo.Agency;
                motorEndoRequest.AgentCode = userInfo.AgentCode;
                motorEndoRequest.InsuranceType = Constants.Motor;
                motorEndoRequest.DocumentNo = ddlMotorPolicies.SelectedItem.Text.Trim();

                var listEndoResponse = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                             <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndoResult>,
                             BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndoRequest>
                            (BKIC.SellingPoint.DTO.Constants.MotorEndorsementURI.GetAllEndorsements, motorEndoRequest);

                if (listEndoResponse.StatusCode == 200 && listEndoResponse.Result.IsTransactionDone)
                {
                    gvMotorEndorsement.DataSource = listEndoResponse.Result.MotorEndorsements;
                    gvMotorEndorsement.DataBind();

                    if (listEndoResponse.Result.MotorEndorsements.Count > 0)
                    {
                        _MotorEndorsementID = listEndoResponse.Result.MotorEndorsements[listEndoResponse.Result.MotorEndorsements.Count - 1].MotorEndorsementID;
                    }
                    else
                    {
                        _MotorEndorsementID = 0;
                    }
                }
            }
        }

        protected void ddlPaymentMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentMethods.SelectedIndex == 1)
            {
                //txtAccountNumber.Text = "";
                //txtAccountNumber.Enabled = false;
            }
            else
            {
                //txtAccountNumber.Enabled = true;
            }
            Page_CustomValidate();
        }

        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                Page_CustomValidate();
                if (Page.IsValid)
                {
                    if (ddlMotorPolicies.SelectedIndex > 0)
                    {
                        CalculateEndorsementQuote(true);
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        public void CalculateEndorsementQuote(bool showPremium)
        {

            master.ShowLoading = true;
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            var motorEndorementQuote = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementQuote();
            motorEndorementQuote.Agency = userInfo.Agency;
            motorEndorementQuote.AgentCode = userInfo.AgentCode;

            //Set endorsement type for the calculation.
            SetEndorsementType(motorEndorementQuote);


            //Calculate the motor endorsement premium.
            var motorEndoQuoteResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                              <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementQuoteResult>,
                                              BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementQuote>
                                              (BKIC.SellingPoint.DTO.Constants.MotorEndorsementURI.GetMotorEndorsementQuote, motorEndorementQuote);

            if (motorEndoQuoteResult.StatusCode == 200 && motorEndoQuoteResult.Result.IsTransactionDone)
            {
                
                var endoresementPremium = motorEndorementQuote.EndorsementType == "CancelPolicy" ? motorEndoQuoteResult.Result.RefundPremium : motorEndoQuoteResult.Result.EndorsementPremium;               
                calculatedPremium.Value = endoresementPremium.ToString();

                var commisionRequest = new BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionRequest();
                commisionRequest.AgentCode = userInfo.AgentCode;
                commisionRequest.Agency = userInfo.Agency;
                commisionRequest.SubClass = subClass.Value;
                commisionRequest.PremiumAmount = endoresementPremium;

               //Get commision for the endorsement premium.
                var commissionresult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                       <BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionResponse>,
                                       BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionRequest>("api/insurance/Commission", commisionRequest);

                if (commissionresult.StatusCode == 200 && commissionresult.Result.IsTransactionDone && commissionresult.Result.CommissionAmount > 0)
                {
                    //commission.Text = Convert.ToString(commissionresult.Result.CommissionAmount);
                    calculatedCommision.Value = Convert.ToString(commissionresult.Result.CommissionAmount);
                    if (showPremium)
                       ShowPremium(userInfo, endoresementPremium, commissionresult.Result.CommissionAmount);                    
                }

            }
        }

        private void SetEndorsementType(MotorEndorsementQuote motorEndoQuote)
        {
            motorEndoQuote.EndorsementType = string.Empty;
            motorEndoQuote.NewInsuredCode = string.Empty;
            if (ddlEndorsementType.SelectedIndex > 0)
            {
                motorEndoQuote.EndorsementType = ddlEndorsementType.SelectedItem.Value;
            }
            if (ddlEndorsementType.SelectedItem.Value == "Extended")
            {
                var daysDiff = (txtEffectiveToDate.Text.CovertToCustomDateTime() - expireDate.Value.CovertToCustomDateTime()).TotalDays;
                motorEndoQuote.ExtendedDays = Convert.ToInt32(daysDiff);
            }
            if (ddlEndorsementType.SelectedItem.Value == "CancelPolicy")
            {
                motorEndoQuote.CancelationDate = txtEffectiveToDate.Text.CovertToCustomDateTime();
            }
            if (ddlEndorsementType.SelectedItem.Value == "Transfer")
            {
                motorEndoQuote.NewInsuredCode = string.IsNullOrEmpty(txtNewClientCode.Text) ? string.Empty : txtNewClientCode.Text.Trim();
                motorEndoQuote.CancelationDate = DateTime.Now;
            }
            else
            {
                motorEndoQuote.CancelationDate = DateTime.Now;
            }
            motorEndoQuote.MainClass = MainClass;
            motorEndoQuote.SubClass = SubClass;
            motorEndoQuote.EffectiveFromDate = txtEffectiveFromDate.Text.CovertToCustomDateTime();
            motorEndoQuote.EffectiveToDate = txtEffectiveToDate.Text.CovertToCustomDateTime();
            motorEndoQuote.PaidPremium = string.IsNullOrEmpty(paidPremium.Value) ? decimal.Zero : Convert.ToDecimal(paidPremium.Value);
        }

        public bool EndorsementPrecheck()
        {
            master.ShowLoading = true;
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();
            
            var req = new MotorEndorsementPreCheckRequest();
            req.DocNo = ddlMotorPolicies.SelectedIndex > 0 ? ddlMotorPolicies.SelectedItem.Text : string.Empty;

            var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                         <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementPreCheckResponse>,
                         BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementPreCheckRequest>
                         (BKIC.SellingPoint.DTO.Constants.MotorEndorsementURI.EndorsementPreCheck, req);

            if (result.StatusCode == 200 && result.Result.IsTransactionDone)
            {
                return result.Result.IsAlreadyHave;
            }
            return false;
        }

        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                Page_CustomValidate();
                if (Page.IsValid)
                {
                    SaveAuthorize(false);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                Page_CustomValidate();
                if (Page.IsValid)
                {
                    SaveAuthorize(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        public void SaveAuthorize(bool isSave)
        {
            try
            {           

                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();

                if (isSave && EndorsementPrecheck())
                {
                    modalBodyText.InnerText = "Your motor policy already have saved endorsement";
                    btnYes.Visible = false;
                    btnOK.Text = "OK";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowPopup();", true);
                    return;
                }

                var postMotorEndorsement = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsement();
                postMotorEndorsement.Agency = userInfo.Agency;
                postMotorEndorsement.AgencyCode = userInfo.AgentCode;
                postMotorEndorsement.IsSaved = isSave;
                postMotorEndorsement.IsActivePolicy = !isSave;
                postMotorEndorsement.PremiumAmount = string.IsNullOrEmpty(paidPremium.Value) ? decimal.Zero : Convert.ToDecimal(paidPremium.Value);
                postMotorEndorsement.CreatedBy = Convert.ToInt32(userInfo.ID);                            

                //Get saved policy details by document(policy) number.
                var url = BKIC.SellingPoint.DTO.Constants.MotorURI.GetSavedQuoteDocumentNo
                          .Replace("{documentNo}", ddlMotorPolicies.SelectedItem.Text.Trim())
                          .Replace("{agentCode}", userInfo.AgentCode);

                var motorDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorSavedQuotationResponse>>(url);

                //Update policy details on current page for dispaly the details.
                if (motorDetails.StatusCode == 200 && motorDetails.Result.IsTransactionDone)
                {                    
                    SetEndorsementType(postMotorEndorsement, motorDetails.Result.MotorPolicyDetails);
                }
                

                var response = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                             <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementResult>, 
                             BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsement>
                             (BKIC.SellingPoint.DTO.Constants.MotorEndorsementURI.PostMotorEndorsement, postMotorEndorsement);

                if (response.Result != null && response.StatusCode == 200 && response.Result.IsTransactionDone)
                {
                    _MotorEndorsementID = response.Result.MotorEndorsementID;
                    ListEndorsements(service, userInfo);
                    modalBodyText.InnerText = GetMessageText(false, response.Result.EndorsementNo);
                    SetScheduleCertificateHref(userInfo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        private void SetScheduleCertificateHref(OAuthTokenResponse userInfo)
        {
            string sourceDateText = txtEffectiveToDate.Text;
            DateTime sourceDate = DateTime.ParseExact(sourceDateText, "dd/MM/yyyy", CultureInfo.CurrentCulture);
            string formatted = sourceDate.ToString("yyyy-MM-dd");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowPopup();", true);

            SetScheduleHRef(ddlMotorPolicies.SelectedItem.Text.Trim(), Constants.Motor, userInfo, ddlEndorsementType.SelectedItem.Text.Trim(),
                            string.IsNullOrEmpty(txtOldClientCode.Text.Trim()) ? "ClientCode" : txtOldClientCode.Text.Trim(),
                            string.IsNullOrEmpty(txtNewClientCode.Text.Trim()) ? "ClientCode" : txtNewClientCode.Text.Trim(),
                            string.IsNullOrEmpty(txtNewInsuredName.Text.Trim()) ? "ClientCode" : txtNewInsuredName.Text.Trim(),
                            string.IsNullOrEmpty("") ? "ClientCode" : "",
                            string.IsNullOrEmpty(txtNewRegistrationNo.Text.Trim()) ? "ClientCode" : txtNewRegistrationNo.Text.Trim(),
                            string.IsNullOrEmpty(txtEffectiveToDate.Text.Trim()) ? "ClientCode" : formatted.Trim());

            SetCertificateHRef(ddlMotorPolicies.SelectedItem.Text.Trim(), "Portal", userInfo, ddlEndorsementType.SelectedItem.Text.Trim(),
                            string.IsNullOrEmpty(txtOldClientCode.Text.Trim()) ? "ClientCode" : txtOldClientCode.Text.Trim(),
                            string.IsNullOrEmpty(txtNewClientCode.Text.Trim()) ? "ClientCode" : txtNewClientCode.Text.Trim(),
                            string.IsNullOrEmpty(txtNewInsuredName.Text.Trim()) ? "ClientCode" : txtNewInsuredName.Text.Trim(),
                            string.IsNullOrEmpty("") ? "ClientCode" : "",
                            string.IsNullOrEmpty(txtNewRegistrationNo.Text.Trim()) ? "ClientCode" : txtNewRegistrationNo.Text.Trim(),
                            string.IsNullOrEmpty(txtEffectiveToDate.Text.Trim()) ? "ClientCode" : formatted.Trim());
        }

        public void SetEndorsementType(BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsement mtorEndorsement, BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorInsurancePolicy mtorPolicyDetails)
        {
            mtorEndorsement.OldInsuredCode = mtorPolicyDetails.InsuredCode;
            mtorEndorsement.OldInsuredName = mtorPolicyDetails.InsuredName;
            mtorEndorsement.OldChassisNo = mtorPolicyDetails.ChassisNo;
            mtorEndorsement.OldRegistrationNo = mtorPolicyDetails.RegistrationNumber;
            mtorEndorsement.VehicleValue = mtorPolicyDetails.VehicleValue;
            mtorEndorsement.Mainclass = mtorPolicyDetails.Mainclass;
            mtorEndorsement.Subclass = mtorPolicyDetails.Subclass;
            mtorEndorsement.MotorID = mtorPolicyDetails.MotorID;
            mtorEndorsement.PolicyCommencementDate = mtorPolicyDetails.PolicyCommencementDate;
            mtorEndorsement.ExpiryDate = mtorPolicyDetails.ExpiryDate;
            mtorEndorsement.Remarks = "";
            mtorEndorsement.AccountNumber = "";
            mtorEndorsement.EndorsementType = ddlEndorsementType.SelectedItem.Value;
            mtorEndorsement.PaymentType = ddlPaymentMethods.SelectedItem.Text.Trim();

            if (mtorEndorsement.EndorsementType == "Extended")
            {
                mtorEndorsement.ExtendedExpireDate = txtEffectiveToDate.Text.CovertToCustomDateTime();

                mtorEndorsement.RegistrationNo = string.Empty;
                mtorEndorsement.InsuredCode = string.Empty;
                mtorEndorsement.InsuredName = string.Empty;
                mtorEndorsement.ChassisNo = string.Empty;
            }
            else if (mtorEndorsement.EndorsementType == "Transfer")
            {
                mtorEndorsement.InsuredCode = txtNewClientCode.Text.Trim();
                mtorEndorsement.InsuredName = txtNewInsuredName.Text.Trim();
            }
            else if (mtorEndorsement.EndorsementType == "CancelPolicy")
            {
                mtorEndorsement.CancelDate = txtEffectiveToDate.Text.CovertToCustomDateTime();
            }
            else if (mtorEndorsement.EndorsementType == "ChangeRegistrationNo")
            {
                mtorEndorsement.RegistrationNo = txtNewRegistrationNo.Text.Trim();
               // mtorEndorsement.ChassisNo = txtNewChassesNo.Text.Trim();
            }
            else if(mtorEndorsement.EndorsementType == "AddRemoveBank")
            {
                if(ddlBank.SelectedIndex > 0)
                {
                    mtorEndorsement.FinancierCompanyCode = ddlBank.SelectedItem.Value;
                }
                else
                {
                    mtorEndorsement.FinancierCompanyCode = string.Empty;
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Homepage.aspx");
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
        }

        public void ShowPremium(OAuthTokenResponse userInfo, decimal Premium, decimal Commission)
        {
            amtDisplay.Visible = true;
            if (userInfo.Roles == "SuperAdmin" || userInfo.Roles == "BranchAdmin")
            {
                premiumAmount.Text = Convert.ToString(Premium);
                commission.Text = Convert.ToString(Commission);
                includeDisc.Visible = true;
            }
            else
            {
                premiumAmount1.Text = Convert.ToString(Premium);
                commission1.Text = Convert.ToString(Commission);
                excludeDisc.Visible = true;
            }
        }

        protected void validate_Premium(object sender, EventArgs e)
        {
            var Premium = Convert.ToDecimal(calculatedPremium.Value);
            var Commision = Convert.ToDecimal(calculatedCommision.Value);
            decimal Discount = string.IsNullOrEmpty(txtDiscount.Text) ? decimal.Zero : Convert.ToDecimal(txtDiscount.Text);
            var reduceablePremium = Premium - Commision;
            var premiumDiff = Premium - Discount;

            if (premiumDiff < reduceablePremium)
            {
                premiumAmount.Text = Convert.ToString(reduceablePremium);
                txtDiscount.Text = Convert.ToString(calculatedCommision.Value);
                commission.Text = Convert.ToString(0);
            }
            else if (Discount > Premium)
            {
                premiumAmount.Text = Convert.ToString(reduceablePremium);
                txtDiscount.Text = Convert.ToString(calculatedCommision.Value);
                commission.Text = Convert.ToString(0);
            }
            else
            {
                premiumAmount.Text = Convert.ToString(premiumDiff);
                commission.Text = Convert.ToString(Commision - Discount);
                btnSubmit.Enabled = true;
            }
        }

        //protected void ChangedNewInsured_CPR(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if(ddlNewInsuredCPR.SelectedIndex > 0)
        //        {
        //            master.ShowLoading = true;
        //            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
        //            if (userInfo == null)
        //            {
        //                Response.Redirect("Login.aspx");
        //            }
        //            var service = master.GetService();

        //            var req = new AgencyInsuredRequest();

        //            req.Agency = userInfo.Agency;
        //            req.AgentCode = userInfo.AgentCode;
        //            req.CPR = ddlNewInsuredCPR.SelectedItem.Text;

        //            var response = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
        //                         <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredResult>,
        //                         BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest>
        //                         (BKIC.SellingPoint.DTO.Constants.AdminURI.CalculateCommission, req);

        //            if (response.Result != null && response.StatusCode == 200 && response.Result.IsTransactionDone)
        //            {
        //                txtNewClientCode.Text = response.Result.AgencyInsured[0].InsuredCode;
        //                txtNewInsuredName.Text = response.Result.AgencyInsured[0].FirstName + " " + response.Result.AgencyInsured[0].MiddleName + " " + response.Result.AgencyInsured[0].LastName;
        //                transferDiv.Visible = true;
        //            }

        //        }          

                
        //        //ddlEndorsementType.SelectedIndex = 1;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        master.ShowLoading = false;
        //    }
        //}

        protected void Changed_CPR(object sender, EventArgs e)
        {
            try
            {
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();

                var motorreq = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyMotorRequest();
                motorreq.AgentCode = userInfo.AgentCode;
                motorreq.Agency = userInfo.Agency;
                motorreq.AgentBranch = userInfo.AgentBranch;
                motorreq.CPR = ddlCPR.SelectedIndex > 0 ? ddlCPR.SelectedItem.Text.Trim() : string.Empty;
                motorreq.Type = Constants.Motor;

                //Get PolicyNo by Agency
                var motorPolicies = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                    <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyMotorPolicyResponse>, 
                                    BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyMotorRequest>
                                    (BKIC.SellingPoint.DTO.Constants.MotorURI.GetMotorPoliciesByTypeByCPR, motorreq);

                if (motorPolicies.StatusCode == 200 && motorPolicies.Result.IsTransactionDone && motorPolicies.Result.AgencyMotorPolicies.Count > 0)
                {
                    policyList = motorPolicies.Result.AgencyMotorPolicies;

                    ddlMotorPolicies.DataSource = motorPolicies.Result.AgencyMotorPolicies;
                    ddlMotorPolicies.DataTextField = "DOCUMENTNO";
                    ddlMotorPolicies.DataValueField = "DOCUMENTNO";
                    ddlMotorPolicies.DataBind();
                    ddlMotorPolicies.Items.Insert(0, new ListItem("--Please Select--", "none"));
                }
                Page_CustomValidate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        } 

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dlist.DefaultView.Sort = e.SortExpression + " " + SortDir(e.SortExpression);
            //gvMotorInsurance.DataSource = dlist;
            //gvMotorInsurance.DataBind();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void lnkbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                EndorsementOperation(sender, "delete");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void lnkbtnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
                EndorsementOperation(sender, "authorize");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void lnkbtnSchedule_Click(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void lnkbtnCertificate_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        public void EndorsementOperation(object sender, string type)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();             

                
                var details = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementOperation(); 
                details.MotorEndorsementID = Convert.ToInt32((row.FindControl("lblMotorEndorsementID") as Label).Text.Trim());
                details.MotorID = Convert.ToInt32((row.FindControl("lblMotorID") as Label).Text.Trim());
                details.Agency = userInfo.Agency;
                details.AgentCode = userInfo.AgentCode;
                details.Type = type;

                var endoResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                 <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementOperationResponse>,
                                 BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndorsementOperation>
                                 (BKIC.SellingPoint.DTO.Constants.MotorEndorsementURI.EndorsementOperation, details);

                if (endoResult.StatusCode == 200 && endoResult.Result.IsTransactionDone)
                {
                    ListEndorsements(service, userInfo);

                    btnOK.Text = "OK";
                    btnYes.Visible = false;
                    if(type == "delete")
                    {
                        modalBodyText.InnerText = "Your endorsement deleted successfully";                       
                    }
                    else if(type == "authorize")
                    {
                        modalBodyText.InnerText = "Your endorsement authorized successfully";                        
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowMessage();", true);
                }
            }
        }

        public void Reset_Content(object sender, EventArgs e)
        {
            modalBodyText.InnerText = "Your you sure want authorize this endorsement ?";
            btnOK.Text = "No";
            btnYes.Visible = true;
        }

        public void SetScheduleHRef(string DocNo, string Insurancetype, OAuthTokenResponse UserInfo, string EndorsementType, 
                                    string Insuredcode, string NewInsuredcode,string newInsuredName, string newChassisNo, 
                                    string newRegistrationNo, string newExpireDate)
        {
            //downloadschedule.Visible = true;
            //downloadschedule.HRef = ClientUtility.WebApiUri + BKIC.SellingPoint.DTO.Constants.ScheduleURI.downloadEndorsementSchedule
            //                        .Replace("{insuranceType}", Insurancetype)
            //                        .Replace("{agentCode}", UserInfo.AgentCode)
            //                        .Replace("{documentNo}", DocNo)
            //                        .Replace("{endorsementType}", EndorsementType)
            //                        .Replace("{insuredCode}", Insuredcode)
            //                        .Replace("{newInsuredCode}", NewInsuredcode)
            //                        .Replace("{newInsuredName}", newInsuredName)
            //                        .Replace("{newChassisNo}", newChassisNo)
            //                        .Replace("{newRegistrationNo}", newRegistrationNo)
            //                        .Replace("{newExpireDate}", newExpireDate);
        }

        public void SetCertificateHRef(string DocNo, string type, OAuthTokenResponse UserInfo, string EndorsementType, 
                                       string Insuredcode, string NewInsuredcode,string newInsuredName, string newChassisNo, 
                                       string newRegistrationNo, string newExpireDate)
        {
            //downloadCertificate.Visible = true;
            //downloadCertificate.HRef = ClientUtility.WebApiUri + BKIC.SellingPoint.DTO.Constants.MotorURI.FetchEndorsementCertificate
            //                                                     .Replace("{documentNo}", DocNo)
            //                                                     .Replace("{type}", type)
            //                                                     .Replace("{agentCode}", UserInfo.AgentCode)
            //                                                     .Replace("{endorsementType}", EndorsementType)
            //                                                     .Replace("{insuredCode}", Insuredcode)
            //                                                     .Replace("{newInsuredCode}", NewInsuredcode)
            //                                                     .Replace("{newInsuredName}", newInsuredName)
            //                                                     .Replace("{newChassisNo}", newChassisNo)
            //                                                     .Replace("{newRegistrationNo}", newRegistrationNo)
            //                                                     .Replace("{newExpireDate}", newExpireDate);
        }

        protected void gvMotorEndorsement_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvMotorEndorsement.Rows)
            {
                bool IsSaved = Convert.ToBoolean((row.FindControl("lblIsSaved") as Label).Text.Trim());
                bool IsActive = Convert.ToBoolean((row.FindControl("lblIsActive") as Label).Text.Trim());

                if (IsActive)
                {
                    var btnAuthorize = row.FindControl("lnkbtnAuthorize") as LinkButton;
                    btnAuthorize.Visible = false;

                    var btnDelete = row.FindControl("lnkbtnDelete") as LinkButton;
                    btnDelete.Visible = false;
                }
            }
        }
        public string GetMessageText(bool isHIR, string docNo)
        {
            btnYes.Visible = false;
            btnOK.Text = "OK";
            if (isHIR)
            {
                return  "Your motor endorsement saved and moved into HIR :" + docNo;                
            }
            else
            {
                return "Your motor endorsement has been saved sucessfully :" + docNo;
                
            }          
        }
        public void Page_CustomValidate()
        {
            if (endorsementSubmitted.Value == "true")
            {
                Validate("MotorEndorsementValidation");
            }
        }
        public void HidePremium()
        {
            amtDisplay.Visible = false;

            premiumAmount.Text = Convert.ToString(0);
            commission.Text = Convert.ToString(0);
            includeDisc.Visible = false;


            premiumAmount1.Text = Convert.ToString(0);
            commission1.Text = Convert.ToString(0);
            excludeDisc.Visible = false;
        }

        protected void BtnBankAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ddlBank.Enabled = true;
            }
            catch(Exception ex)
            {

            }
            finally
            {
                master.ShowLoading = false;
            }
        }
        protected void BtnBankRemove_Click(object sender, EventArgs e)
        {
            try
            {
                ddlBank.SelectedIndex = -1;
                ddlBank.Enabled = false;
            }
            catch(Exception ex)
            {

            }
            finally
            {
                master.ShowLoading = false;
            }
        }
    }
}