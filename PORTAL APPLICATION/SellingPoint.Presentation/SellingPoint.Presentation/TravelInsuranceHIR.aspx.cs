﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class TravelInsuranceHIR : System.Web.UI.Page
    {
        General master;
        public TravelInsuranceHIR()
        {
            master = Master as General;
        }

        BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchPolicyDetailsResponse dlist;
        //CommonMethods methods = new CommonMethods();

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!IsPostBack)
            {
                //master.GetCurrentUrl();
                BindAgency();
                dropdown();
                loadd();
                divThankYou.Visible = false;
            }
        }
        private void BindAgency()
        {
            var client = new BKIC.SellingPoint.Presentation.ClientUtility();
            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);


            var dropDownResult = client.serviceManger.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>
                  (BKIC.SellingPont.DTO.Constants.DropdownURI.GetPageDropDowns.Replace("{type}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.UserMaster));


            if (dropDownResult.StatusCode == 200 && dropDownResult.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDownResult.Result.dropdownresult);
                DataTable AgencyDt = dropdownds.Tables["AgentCodeDD"];

                ddlAgency.DataValueField = "AgentCode";
                ddlAgency.DataTextField = "Agency";
                ddlAgency.DataSource = AgencyDt;
                ddlAgency.DataBind();
                ddlAgency.Items.Insert(0, new ListItem("--Please Select--", ""));
                SetDefaultAgency();
            }
        }
        public void SetDefaultAgency()
        {
            OAuthTokenResponse userInfo = null;
            if (Session["UserInfo"] != null)
            {
                userInfo = Session["UserInfo"] as OAuthTokenResponse;

            }
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            ddlAgency.SelectedIndex = ddlAgency.Items.IndexOf(ddlAgency.Items.FindByText(userInfo.Agency));
            ddlAgency.Enabled = false;
        }

        public void dropdown()
        {
            if (!master.IsSessionAvailable())
            {
                Response.Redirect("Login.aspx");
            }
            //var service = master.GetService();
            var client = new BKIC.SellingPoint.Presentation.ClientUtility();
            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);
            var hirdropdown = client.serviceManger.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>
                  (BKIC.SellingPont.DTO.Constants.DropdownURI.GetPageDropDowns.Replace("{type}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.InsurancePortal));

            //var hirdropdown = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>(
            //                     BKIC.SellingPoint.DTO.Constants.DropDownURI.GetPageDropDowns.Replace("{type}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.InsurancePortal));

            if (hirdropdown.StatusCode == 200 && hirdropdown.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(hirdropdown.Result.dropdownresult);
                DataTable hirdt = dropdownds.Tables["HIRStatus"];
                if (hirdt.Rows.Count > 0)
                {
                    ddlStatus.DataValueField = "StatusID";
                    ddlStatus.DataTextField = "HIRStatus";
                    ddlStatus.DataSource = hirdt;
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("--Please Select--", "0"));
                }
            }
        }
        public void loadd()
        {
            if (!master.IsSessionAvailable())
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();
            var fetchdetailsrequest = new AdminFetchTravelDetailsRequest();
            
            fetchdetailsrequest.DocumentNo = "";
            fetchdetailsrequest.AgencyCode = ddlAgency.SelectedItem.Value;
            fetchdetailsrequest.Type = "HIR";
            fetchdetailsrequest.All = true;

            var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                         <BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchTravelDetailsResponse>,
                         BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchTravelDetailsRequest>
                        (BKIC.SellingPoint.DTO.Constants.AdminURI.FetchTravelPolicyDetails, fetchdetailsrequest);

            if (result.StatusCode == 200 && result.Result.IsTransactionDone)
            {
                gvTravelInsurance.DataSource = result.Result.TravelDetails;
                gvTravelInsurance.DataBind();
            }           
        }
        public void loaddgridevent(object sender)
        {
            var service = master.GetService();
            var fetchdetailsrequest = new FetchPolicyDetailsRequest();
            fetchdetailsrequest.CPR = "";
            fetchdetailsrequest.DocumentNo = "";
            fetchdetailsrequest.FilterType = "";
            fetchdetailsrequest.InsuranceType = Constants.Travel;
            fetchdetailsrequest.IsHIR = true;
            fetchdetailsrequest.Source = "";
            fetchdetailsrequest.InsuredCode = "";
            fetchdetailsrequest.PolicyType = "HIR";

            var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchPolicyDetailsResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchPolicyDetailsRequest>
                     (BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.FetchDetails, fetchdetailsrequest);

            DataTable dt = new DataTable();

            if (result.StatusCode == 200 && result.Result.IsTransactionDone)
            {
                gvTravelInsurance.DataSource = result.Result.PolicyDetailList;
                gvTravelInsurance.DataBind();
            }
        }

        protected void lnkbtnEdit_Click(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {
                string travelID = (row.FindControl("lbltravelID") as Label).Text.Trim();
                string InsuredCode = (row.FindControl("lblInsuredCode") as Label).Text.Trim();

                Response.Redirect("TravelInsuranceEdit.aspx?Ref=" + travelID + "&InsuredCode=" + InsuredCode);


            }
        }
        protected void lnkbtnViewDetails_Click(object sender, EventArgs e)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
                {
                   
                    string travelID = (row.FindControl("lbltravelID") as Label).Text.Trim();
                    string InsuredCode = (row.FindControl("lblInsuredCode") as Label).Text.Trim();                   
                    string PolicyNo = HttpUtility.HtmlDecode(row.Cells[2].Text.Trim());
                    string CPR = HttpUtility.HtmlDecode(row.Cells[3].Text.Trim());
                    string InsuredName = HttpUtility.HtmlDecode(row.Cells[4].Text.Trim());
                    Response.Redirect("Travelnsurance.aspx?InsuredCode=" + InsuredCode + "&InsuredName=" + InsuredName + "&CPR=" + CPR + "&PolicyNo=" + PolicyNo + "&IncludeHIR=" + true);


                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
            
        }

        protected void lnkbtnDelete_Click(object sender, EventArgs e)
        {
            using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
            {

                //string a = (row.FindControl("lblveid") as Label).Text.Trim();
                //lblcid.Text = a;
                //lblmessage.Text = "Are you sure want to delete this ?";
                //divThankYou.Visible = true;

            }
        }
        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTravelInsurance.PageIndex = e.NewPageIndex;
            loadd(); //bindgridview will get the data source and bind it again
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dlist.DefaultView.Sort = e.SortExpression + " " + SortDir(e.SortExpression);
            //gvMotorInsurance.DataSource = dlist;
            //gvMotorInsurance.DataBind();
        }

        private string SortDir(string sField)
        {
            string sDir = "asc";
            string sPrevField = (ViewState["SortField"] != null ? ViewState["SortField"].ToString() : "");
            if (sPrevField == sField)
                sDir = (ViewState["SortDir"].ToString() == "asc" ? "desc" : "asc");
            else
                ViewState["SortField"] = sField;

            ViewState["SortDir"] = sDir;
            return sDir;
        }
        protected void btndelconf_Click(object sender, EventArgs e)
        {
            if (!master.IsSessionAvailable())
            {
                Response.Redirect("Login.aspx");
            }

            var service = master.GetService();
            string type = Constants.Travel;
            string HIR = "0";
            string url = BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.FetchDetails.Replace("{HIR}", type);
            url = url.Replace("HIR", HIR);
            var result = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchPolicyDetailsResponse>>
                        (url);

            if (result.StatusCode == 200)
            {
                if (result.Result.IsTransactionDone)
                {

                    lbler.Text = "Travel Insurance Period has been deleted";
                }
                else
                {
                    lbler.Text = result.Result.TransactionErrorMessage;
                }

            }
            else
            {
                lbler.Text = result.ErrorMessage;
            }

            loadd();

            lbler.ForeColor = System.Drawing.Color.Maroon;
            lbler.Text = "Travel Insurance Period has been deleted";
            divThankYou.Visible = false;

        }
        protected void btndelcan_Click(object sender, EventArgs e)
        {
            divThankYou.Visible = false;
        }

        protected void btnApproved_Click(object sender, EventArgs e)
        {
            try
            {
                if (!master.IsSessionAvailable())
                {
                    Response.Redirect("Login.aspx");
                }
                using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
                {
                    string travelID = (row.FindControl("lbltravelID") as Label).Text.Trim();
                    string InsuredCode = (row.FindControl("lblInsuredCode") as Label).Text.Trim();
                    string documentId = HttpUtility.HtmlDecode(row.Cells[2].Text.Trim());
                    var service = master.GetService();
                    var request = new UpdateHIRStatusRequest();
                    request.HIRStatusCode = 8;
                    request.InsuranceType = Constants.Travel;
                    request.ID = Convert.ToInt32(travelID);
                    request.InsuredCode = InsuredCode;
                    request.DocumentNo = documentId;
                    request.InsuredCode = InsuredCode;
                    request.LinkId = (row.FindControl("lblLinkID") as Label).Text.Trim();
                    var approvedresponse = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateHIRStatusResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateHIRStatusRequest>
                               (BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.UpdateHIRStatus, request);

                    if (approvedresponse.Result.IsTransactionDone == true && approvedresponse.StatusCode == 200)
                    {
                        //var btnapproved = row.FindControl("btnApproved") as LinkButton;
                        //btnapproved.Visible = false;
                        // loadd();
                        Response.Redirect(Request.RawUrl);

                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
            
        }  
        protected void btnRejected_Click(object sender, EventArgs e)
        {

            try
            {
                if (!master.IsSessionAvailable())
                {
                    Response.Redirect("Login.aspx");
                }
                RequestDocPopup.Visible = true;

                using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
                {
                    string travelID = (row.FindControl("lbltravelID") as Label).Text.Trim();
                    string InsuredCode = (row.FindControl("lblInsuredCode") as Label).Text.Trim();
                    string linkId = (row.FindControl("lblLinkID") as Label).Text.Trim();
                    string documentId = row.Cells[2].Text.Trim();

                    var service = master.GetLoggedInService();
                    var request = new UpdateHIRStatusRequest();
                    request.HIRStatusCode = 2;
                    request.InsuranceType = Constants.Travel;
                    request.ID = Convert.ToInt32(travelID);
                    request.DocumentNo = documentId;
                    request.InsuredCode = InsuredCode;
                    request.LinkId = linkId;
                    var approvedresponse = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateHIRStatusResponse>,
                                          BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateHIRStatusRequest>(BKIC.SellingPoint.DTO.Constants.InsurancePortalURI.UpdateHIRStatus, request);

                    if (approvedresponse.Result.IsTransactionDone == true && approvedresponse.StatusCode == 200)
                    {
                        //var btnapproved = row.FindControl("btnApproved") as LinkButton;
                        //btnapproved.Visible = false;
                        // loadd();
                        Response.Redirect(Request.RawUrl);

                    }

                }

            }  
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }


        }
        protected void gvTravelInsurance_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvTravelInsurance.Rows)
            {
                string HIRStatusCode = (row.FindControl("lblHIRStatusCode") as Label).Text.Trim();                
                string IsMessage = (row.FindControl("lblIsMessage") as Label).Text.Trim();
                string IsDocuments = (row.FindControl("IsDocument") as Label).Text.Trim();              

                if (HIRStatusCode == "1")
                {
                    var btnapproved = row.FindControl("btnApproved") as LinkButton;
                    btnapproved.Visible = true;                   
                    var btnRejected = row.FindControl("btnRejected") as LinkButton;
                    btnRejected.Visible = true;

                }
                if (HIRStatusCode == "2")
                {
                    var btnapproved = row.FindControl("btnApproved") as LinkButton;
                    btnapproved.Visible = false;                   
                    var btnRejected = row.FindControl("btnRejected") as LinkButton;
                    btnRejected.Visible = false;
                    var btnActivate = row.FindControl("btnActivate") as LinkButton;
                    btnActivate.Visible = true;

                }           
                if (HIRStatusCode == "8")
                {
                    var btnapproved = row.FindControl("btnApproved") as LinkButton;
                    btnapproved.Visible = false;                   
                    var btnRejected = row.FindControl("btnRejected") as LinkButton;
                    btnRejected.Visible = false;
                    var btnActivate = row.FindControl("btnActivate") as LinkButton;
                    btnActivate.Visible = false;

                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                //master.ShowLoading = true;
                if (!master.IsSessionAvailable())
                {
                    Response.Redirect("Login.aspx");
                }
                //master.ShowLoading = true;
                var service = master.GetService();
                var fetchdetailsrequest = new AdminFetchTravelDetailsRequest();               
                fetchdetailsrequest.DocumentNo = txtSearchKey.Text.Trim();                
                fetchdetailsrequest.Type = "HIR";
                fetchdetailsrequest.AgencyCode = ddlAgency.SelectedItem.Value;
                   fetchdetailsrequest.HIRStatus = string.IsNullOrEmpty(ddlStatus.SelectedItem.Value) ? 0 : Convert.ToInt32(ddlStatus.SelectedItem.Value);
                   if (string.IsNullOrEmpty(fetchdetailsrequest.DocumentNo) && string.IsNullOrEmpty(fetchdetailsrequest.AgencyCode) && fetchdetailsrequest.HIRStatus == 0)
                   {
                       loadd();
                   }
                   else
                   {
                       //if(ddlAgency.SelectedIndex > 0)
                       //{
                       //    fetchdetailsrequest.AgencyCode = ddlAgency.SelectedItem.Value;
                       //}
                       //fetchdetailsrequest.HIRStatus = string.IsNullOrEmpty(ddlStatus.SelectedItem.Value) ? 0 : Convert.ToInt32(ddlStatus.SelectedItem.Value);
                       //SetSearchType(fetchdetailsrequest);

                       var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchTravelDetailsResponse>, 
                                   BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchTravelDetailsRequest>
                                  (BKIC.SellingPoint.DTO.Constants.AdminURI.FetchTravelPolicyDetails, fetchdetailsrequest);

                       if (result.StatusCode == 200 && result.Result.IsTransactionDone)
                       {
                           gvTravelInsurance.DataSource = result.Result.TravelDetails;
                           gvTravelInsurance.DataBind();
                       }
                   }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }
        protected void txtSearch_Changed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearchKey.Text))
            {
                ddlAgency.SelectedIndex = 0;
                ddlAgency.Enabled = false;
                ddlStatus.SelectedIndex = 0;
                ddlStatus.Enabled = false;
            }
            else
            {
                ddlAgency.Enabled = true;
                ddlStatus.Enabled = true;
            }
        }

        public void SetSearchType(AdminFetchTravelDetailsRequest req)
        {
            if (!string.IsNullOrEmpty(txtSearchKey.Text.Trim()))
            {
                ddlAgency.SelectedIndex = 0;
                req.ByDocumentNo = true;
            }
            if (ddlAgency.SelectedIndex > 0 && ddlStatus.SelectedIndex > 0)
            {
                req.ByStatusAndAgency = true;
            }
            if (ddlAgency.SelectedIndex > 0 && ddlStatus.SelectedIndex == 0)
            {
                req.ByAgencyCode = true;
            }
            if (ddlStatus.SelectedIndex > 0 && ddlAgency.SelectedIndex == 0)
            {
                req.ByHIRStatus = true;
            }
        }
    }
}