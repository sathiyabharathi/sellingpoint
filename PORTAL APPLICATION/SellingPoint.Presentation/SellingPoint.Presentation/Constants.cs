﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BKIC.SellingPoint.Presentation
{
    public class Constants
    {
        public const string Motor = "MotorInsurance";
        public const int RequestDocHIRStatus = 4;
        public const int RejectHIRStatus = 2;
        public const int ApproveHIRStatus = 8;
        public const string Home = "HomeInsurance";
        public const string Travel = "TravelInsurance";
        public const string DomesticHelp = "DomesticInsurance";
        public const string Filter = "Filter";
        public const string Active = "ActivePolicy";
        public const string Renewal = "Renewal";
        public const string WebRedirectUrl = "UploadDocuments.aspx";
    }
}