﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class MotorBranchReport : System.Web.UI.Page
    {
        private General master;

        public MotorBranchReport()
        {
            master = Master as General;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            try
            {
                if (!Page.IsPostBack)
                {
                    BindAgencyBranch();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportGridToExcel();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        //private void ExportGridToExcel()
        //{
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.Charset = "";
        //    string FileName = "MotorBranchReport" + DateTime.Now + ".xls";
        //    StringWriter strwritter = new StringWriter();
        //    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.ContentType = "application/vnd.ms-excel";
        //    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        //    gvMotorBranchReport.GridLines = GridLines.Both;
        //    gvMotorBranchReport.HeaderStyle.Font.Bold = true;
        //    gvMotorBranchReport.RenderControl(htmltextwrtter);
        //    Response.Write(strwritter.ToString());
        //    Response.End();
        //}

        protected void ExportGridToExcel()
        {
            Response.Clear();
            Response.Buffer = true;
            string FileName = "MotorBranchReport" + DateTime.Now + ".xls";
            Response.AddHeader("content-disposition", "attachment; filename = " + FileName);
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gvMotorBranchReport.AllowPaging = false;
                this.Bind();

                gvMotorBranchReport.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in gvMotorBranchReport.HeaderRow.Cells)
                {
                    cell.BackColor = gvMotorBranchReport.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gvMotorBranchReport.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gvMotorBranchReport.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gvMotorBranchReport.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gvMotorBranchReport.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        private void BindAgencyBranch()
        {
            OAuthTokenResponse userInfo = null;
            if (Session["UserInfo"] != null)
            {
                userInfo = Session["UserInfo"] as OAuthTokenResponse;
            }
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var client = new BKIC.SellingPoint.Presentation.ClientUtility();
            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);

            var result = client.serviceManger.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>
            (BKIC.SellingPont.DTO.Constants.DropdownURI.GetPageDropDowns.Replace("{type}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.Reports));

            if (result.StatusCode == 200 && result.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(result.Result.dropdownresult);

                DataTable AgencyDt = dropdownds.Tables["AgentCodeDD"];
                DataTable branchesDt = dropdownds.Tables["AgentBranchDD"];

                if (AgencyDt != null && AgencyDt.Rows.Count > 0)
                {
                    ddlAgency.DataValueField = "AgentCode";
                    ddlAgency.DataTextField = "Agency";
                    ddlAgency.DataSource = AgencyDt;
                    ddlAgency.DataBind();
                    ddlAgency.Items.Insert(0, new ListItem("--Please Select--", ""));
                    SetDefaultAgency();
                }
                if (branchesDt != null && branchesDt.Rows.Count > 0)
                {
                    ddlBranch.DataValueField = "AGENTBRANCH";
                    ddlBranch.DataTextField = "BranchName";
                    ddlBranch.DataSource = branchesDt;
                    ddlBranch.DataBind();
                    ddlBranch.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
            }
        }

        public void SetDefaultAgency()
        {
            OAuthTokenResponse userInfo = null;
            if (Session["UserInfo"] != null)
            {
                userInfo = Session["UserInfo"] as OAuthTokenResponse;
            }
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            ddlAgency.SelectedIndex = ddlAgency.Items.IndexOf(ddlAgency.Items.FindByText(userInfo.Agency));
            ddlAgency.Enabled = false;
        }

        public void loadd()
        {
            try
            {
                this.Bind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMotorBranchReport.PageIndex = e.NewPageIndex;
            loadd(); //bindgridview will get the data source and bind it again
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Bind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }
        public void Bind()
        {
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;

            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            var reportRequest = new AdminFetchReportRequest();

            reportRequest.Agency = ddlAgency.SelectedItem.Text;
            reportRequest.AgentCode = ddlAgency.SelectedItem.Value;
            reportRequest.BranchCode = ddlBranch.SelectedItem.Value;
            reportRequest.DateFrom = txtDateFrom.Text.CovertToCustomDateTime();
            reportRequest.DateTo = txtDateTo.Text.CovertToCustomDateTime();
            reportRequest.InsuranceType = "MotorInsurance";
            reportRequest.ReportType = "MotorBranchReport";

            var motorBranchResponse = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorReportResponse>,
                                                   BKIC.SellingPoint.DTO.RequestResponseWrappers.AdminFetchReportRequest>
                                                   (BKIC.SellingPoint.DTO.Constants.ReportURI.GetMotorReport, reportRequest);


            if (motorBranchResponse.StatusCode == 200 && motorBranchResponse.Result.IsTransactionDone)
            {
                gvMotorBranchReport.DataSource = motorBranchResponse.Result.MotorReportDetails;
                gvMotorBranchReport.DataBind();
            }
        }
    }
}