﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class Login : System.Web.UI.Page
    {
        private General master;

        public Login()
        {
            master = Master as General;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                master = Master as General;
                //master.ShowLoading = false;

                if (Session["QuicRenewUserInfo"] != null)
                {
                    Session.Remove("QuicRenewUserInfo");
                }

                if (Session["IsLoginExpired"] != null && Session["UserInfo"] != null)
                {
                    if (Session["IsLoginExpired"].ToString() == "1")
                    {
                        //divErrorMessages.Visible = true;
                        //lErrorMessage.Text = "Login Session Expired";
                    }
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                master = Master as General;
                //master.ShowLoading = true;
                var authentication = new BKIC.SellingPoint.DTO.RequestResponseWrappers.OAuthRequest();
                authentication.UserName = txtCPR.Text;
                authentication.Password = txtLoginPassword.Value;
                authentication.GrantType = "";

                var client = new BKIC.SellingPoint.Presentation.ClientUtility();

                client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", true);

                client.UserInfo = client.serviceManger.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.OAuthTokenResponse,
                                 BKIC.SellingPoint.DTO.RequestResponseWrappers.OAuthRequest>
                                 (BKIC.SellingPoint.DTO.Constants.UserURI.Authentication, authentication);

                if (client.UserInfo.StatusCode == 200)
                {
                    client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, client.UserInfo.AccessToken, false);
                    Session["UserInfo"] = client.UserInfo;
                    Session.Timeout = Convert.ToInt32(TimeSpan.FromSeconds(client.UserInfo.ExpiresIn).TotalMinutes);
                    // createa a new GUID and save into the session
                    string guid = Guid.NewGuid().ToString();
                    // now create a new cookie with this guid value
                    //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", guid));

                    if (Session["ReturnUrl"] != null)
                    {
                        this.Response.Redirect(Session["ReturnUrl"].ToString());
                    }
                    else
                    {
                        //this.Response.Redirect("Travelnsurance.aspx");
                        var productRequest = new AgecyProductRequest();
                        productRequest.Agency = client.UserInfo.Agency;
                        productRequest.AgentCode = client.UserInfo.AgentCode;
                        productRequest.MainClass = string.Empty;
                        productRequest.SubClass = string.Empty;
                        productRequest.Type = "MotorInsurance";

                        var productResponse = client.serviceManger.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyProductResponse>,
                                                BKIC.SellingPoint.DTO.RequestResponseWrappers.AgecyProductRequest>
                                                (BKIC.SellingPoint.DTO.Constants.AdminURI.FetchAgencyProductByType, productRequest);

                        if (productResponse.StatusCode == 200 && productResponse.Result.IsTransactionDone)
                        {
                            if (productResponse.Result.MotorProducts != null && productResponse.Result.MotorProducts.Count > 0)
                            {
                                Session["MotorProducts"] = productResponse.Result.MotorProducts;
                            }
                        }

                        this.Response.Redirect("HomePage.aspx");
                    }
                }
                else
                {
                    ErrorMessage.Text = "Invalid credentials";
                    Session.Remove("UserInfo");
                }
               
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
        }
    }
}