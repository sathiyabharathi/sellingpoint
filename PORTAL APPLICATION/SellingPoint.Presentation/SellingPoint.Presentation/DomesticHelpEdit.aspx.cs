﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class DomesticHelpEdit : System.Web.UI.Page
    {

       // CommonMethods methods = new CommonMethods();
        General master;
        public DomesticHelpEdit()
        {
            master = Master as General;
        }
        DataTable Genderdt;
        DataTable Nationalitydt;
        DataTable Occupationdt;
        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;

            if (!IsPostBack)
            {
                BindDropdown();
                if (Request.QueryString["Ref"] != null && Request.QueryString["InsuredCode"] != null)
                {
                    hdnDomesticID.Value = Convert.ToString(Request.QueryString["Ref"]);
                    hdnInsuredCode.Value = Convert.ToString(Request.QueryString["InsuredCode"]);
                    BindSavedDetails(hdnDomesticID.Value, hdnInsuredCode.Value);

                }
            }
        }
        public void BindDropdown()
        {
            var service = master.GetService();
            var dropDown = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>(
            BKIC.SellingPoint.DTO.Constants.DropDownURI.GetPageDropDowns.Replace("{pageName}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.DomesticHelp));
            if (dropDown.StatusCode == 200 && dropDown.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDown.Result.dropdownresult);
                DataTable domesticInsuranceYrdt = dropdownds.Tables["BK_DomesticInsuranceYear"];
                DataTable domesticHelpWorkerdt = dropdownds.Tables["BK_DomesticHelpWorkers"];
                DataTable domesticIWorkerTypedt = dropdownds.Tables["BK_DomesticWorkerType"];

                if (domesticInsuranceYrdt.Rows.Count > 0 && domesticInsuranceYrdt != null)
                {
                    ddlInsurancePeriod.DataValueField = "Value";
                    ddlInsurancePeriod.DataTextField = "Text";
                    ddlInsurancePeriod.DataSource = domesticInsuranceYrdt;
                    ddlInsurancePeriod.DataBind();
                }
                if (domesticHelpWorkerdt.Rows.Count > 0 && domesticHelpWorkerdt != null)
                {
                    ddlNoofDomesticWorker.DataValueField = "Value";
                    ddlNoofDomesticWorker.DataTextField = "Text";
                    ddlNoofDomesticWorker.DataSource = domesticHelpWorkerdt;
                    ddlNoofDomesticWorker.DataBind();
                    ddlNoofDomesticWorker.Items.Insert(0, new ListItem("--Please Select--", "0"));


                }

                if (domesticIWorkerTypedt.Rows.Count > 0 && domesticIWorkerTypedt != null)
                {
                    ddlDomesticWorkerType.DataValueField = "Value";
                    ddlDomesticWorkerType.DataTextField = "Text";
                    ddlDomesticWorkerType.DataSource = domesticIWorkerTypedt;
                    ddlDomesticWorkerType.DataBind();
                    ddlDomesticWorkerType.Items.Insert(0, new ListItem("--Please Select--", "0"));
                }
            }
        }

        public void fetchRepeaterDropdown()
        {
            var service = master.GetService();
            var dropDown = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>(
            BKIC.SellingPoint.DTO.Constants.DropDownURI.GetPageDropDowns.Replace("{pageName}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.DomesticHelp));
            if (dropDown.StatusCode == 200 && dropDown.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDown.Result.dropdownresult);
                Nationalitydt = dropdownds.Tables["BK_Nationality"];
                Genderdt = dropdownds.Tables["BK_Gender"];
                Occupationdt = dropdownds.Tables["BK_DomesticWorkerOccupation"];
            }

        }


        public void BindSavedDetails(string domesticID, string insuredCode)
        {
            var url = BKIC.SellingPoint.DTO.Constants.DomesticURI.GetSavedQuote.Replace("{domesticID}", domesticID);
            url = url.Replace("{insuredCode}", insuredCode);
            var service = master.GetService();
            var domesticdetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                  <BKIC.SellingPoint.DTO.RequestResponseWrappers.DomesticHelpSavedQuotationResponse>>(url);

            if (domesticdetails.StatusCode == 200 && domesticdetails.Result.IsTransactionDone == true)
            {
                DomesticHelpSavedQuotationResponse savedquotation = new DomesticHelpSavedQuotationResponse();
                savedquotation.DomesticHelp = domesticdetails.Result.DomesticHelp;
                savedquotation.DomesticHelpMemberList = domesticdetails.Result.DomesticHelpMemberList;
                //bindDomesticDetails(savedquotation);
                BindQuotationDetails(savedquotation);
            }
        }

        public void BindQuotationDetails(DomesticHelpSavedQuotationResponse details)
        {


            ddlInsurancePeriod.SelectedValue = Convert.ToString(details.DomesticHelp.InsurancePeroid);
            ddlNoofDomesticWorker.SelectedValue = Convert.ToString(details.DomesticHelp.NoOfDomesticWorkers);
            txtPolicyStartDate.Text = details.DomesticHelp.PolicyStartDate.CovertToLocalFormat();
            //ddlDomesticWorkType.SelectedValue=details.DomesticHelp.

            ddlPhysicalDefect.SelectedValue = details.DomesticHelp.IsPhysicalDefect;
            if (ddlPhysicalDefect.SelectedItem.Text == "Yes")
            {
                physdescDiv.Visible = true;
                txtPhysicalDescription.Text = details.DomesticHelp.PhysicalDefectDescription;
            }
            else
            {
                physdescDiv.Visible = false;
            }

            txtPhysicalDescription.Text = details.DomesticHelp.PhysicalDefectDescription;
            txtTotalPremium.Text = Convert.ToString(details.DomesticHelp.PremiumBeforeDiscount);
            txtDiscountPremium.Text = Convert.ToString(details.DomesticHelp.PremiumAfterDiscount);

            bindDomesticDetails(details);

        }

        protected void ddlNoOfWorkersPolicy_SelectedIndexChanged(object sender, EventArgs e)
        {

            NoOfWorkersPolicyChanged();

        }

        private void NoOfWorkersPolicyChanged()
        {
            int Currentcount = Convert.ToInt32(ddlNoofDomesticWorker.SelectedItem.Value);
            DataTable existingdetailsdt = GetDomesticWorkerDetails();
            if (existingdetailsdt.Rows.Count > 0)
            {
                int rowscount = existingdetailsdt.Rows.Count;
                if (Currentcount > rowscount)
                {
                    int AddCount = Currentcount - rowscount;
                    for (int i = 1; i <= AddCount; i++)
                    {
                        existingdetailsdt.Rows.Add(rowscount + i, rowscount + i, "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }

                }
                else
                {
                    int RemoveCount = rowscount - Currentcount;
                    for (int i = 1; i <= RemoveCount; i++)
                    {
                        existingdetailsdt.Rows.RemoveAt(existingdetailsdt.Rows.Count - 1);
                    }
                }


                fetchRepeaterDropdown();
                rptDomesticWorker.DataSource = existingdetailsdt;
                rptDomesticWorker.DataBind();
            }
            else
            {
                DataTable dt = CreateDomesticWorkers(Currentcount);
                fetchRepeaterDropdown();
                rptDomesticWorker.DataSource = dt;
                rptDomesticWorker.DataBind();
            }
        }
        public void bindDomesticDetails(DomesticHelpSavedQuotationResponse domesticdetails)
        {
            DataTable domesticdt = new DataTable();
            domesticdt.Columns.Add("ItemSerialNo", typeof(int));
            domesticdt.Columns.Add("Count", typeof(int));
            domesticdt.Columns.Add("Name", typeof(string));
            domesticdt.Columns.Add("Sex", typeof(string));
            domesticdt.Columns.Add("DateOfBirth", typeof(string));
            domesticdt.Columns.Add("Nationality", typeof(string));
            domesticdt.Columns.Add("CPR", typeof(string));
            domesticdt.Columns.Add("Passport", typeof(string));
            domesticdt.Columns.Add("Occupation", typeof(string));
            domesticdt.Columns.Add("FlatNo", typeof(string));
            domesticdt.Columns.Add("BuildingNo", typeof(string));
            domesticdt.Columns.Add("BlockNo", typeof(string));
            domesticdt.Columns.Add("RoadNo", typeof(string));
            domesticdt.Columns.Add("Town", typeof(string));
            domesticdt.Columns.Add("Others", typeof(string));

            foreach (var list in domesticdetails.DomesticHelpMemberList)
            {
                string FlatNo = string.Empty;
                string BuildingNo = string.Empty;
                string RoadNo = string.Empty;
                string BlockNo = string.Empty;
                string Area = string.Empty;

                List<string> adressarray = list.AddressType.Split(',').ToList();
                int i = 0;
                foreach (var item in adressarray)
                {
                    FlatNo = item;
                    if (i == 1)
                        BuildingNo = item;
                    if (i == 2)
                        RoadNo = item;
                    if (i == 3)
                        BlockNo = item;
                    if (i == 4)
                        Area = item;

                    i++;
                }
                FlatNo = Convert.ToString(adressarray[0]);

                domesticdt.Rows.Add(list.ItemserialNo, list.ItemserialNo, list.Name, list.Sex, list.DOB.CovertToLocalFormat(), list.Nationality,
                    list.CPRNumber, list.Passport, list.Occupation, FlatNo, BuildingNo, RoadNo, BlockNo, Area, list.OtherOccupation);
            }
            fetchRepeaterDropdown();
            rptDomesticWorker.DataSource = domesticdt;
            rptDomesticWorker.DataBind();

        }


        public DataTable CreateDomesticWorkers(int workerscount)
        {
            DataTable domesticdt = new DataTable();
            domesticdt.Columns.Add("ItemSerialNo", typeof(int));
            domesticdt.Columns.Add("Name", typeof(string));
            domesticdt.Columns.Add("Sex", typeof(string));
            domesticdt.Columns.Add("DateOfBirth", typeof(string));
            domesticdt.Columns.Add("Nationality", typeof(string));
            domesticdt.Columns.Add("CPR", typeof(string));
            domesticdt.Columns.Add("Passport", typeof(string));
            domesticdt.Columns.Add("Occupation", typeof(string));
            domesticdt.Columns.Add("FlatNo", typeof(string));
            domesticdt.Columns.Add("BuildingNo", typeof(string));
            domesticdt.Columns.Add("BlockNo", typeof(string));
            domesticdt.Columns.Add("RoadNo", typeof(string));
            domesticdt.Columns.Add("Town", typeof(string));
            domesticdt.Columns.Add("Others", typeof(string));
            domesticdt.Columns.Add("Count", typeof(int));
            for (int i = 1; i <= workerscount; i++)
            {
                domesticdt.Rows.Add(i, "", "", "", "", "", "", "", "", "", "", "", "", "", i);
            }

            return domesticdt;
        }

        public DataTable GetDomesticWorkerDetails()
        {
            try
            {
                string Name = string.Empty;
                string DOB = string.Empty;
                string Gender = string.Empty;
                string CPR = string.Empty;
                string Passport = string.Empty;
                string Nationality = string.Empty;
                string Occupation = string.Empty;
                string Others = string.Empty;
                string FlatNo = string.Empty;
                string BuildingNo = string.Empty;
                string BlockNo = string.Empty;
                string RoadNo = string.Empty;
                string Town = string.Empty;
                int ItemSerialNo = 0;
                //int Count = 0;


                DataTable domesticdt = new DataTable();
                domesticdt.Columns.Add("ItemSerialNo", typeof(int));
                domesticdt.Columns.Add("Count", typeof(int));
                domesticdt.Columns.Add("Name", typeof(string));
                domesticdt.Columns.Add("Sex", typeof(string));
                domesticdt.Columns.Add("DateOfBirth", typeof(string));
                domesticdt.Columns.Add("Nationality", typeof(string));
                domesticdt.Columns.Add("CPR", typeof(string));
                domesticdt.Columns.Add("Passport", typeof(string));
                domesticdt.Columns.Add("Occupation", typeof(string));
                domesticdt.Columns.Add("FlatNo", typeof(string));
                domesticdt.Columns.Add("BuildingNo", typeof(string));
                domesticdt.Columns.Add("BlockNo", typeof(string));
                domesticdt.Columns.Add("RoadNo", typeof(string));
                domesticdt.Columns.Add("Town", typeof(string));
                domesticdt.Columns.Add("Others", typeof(string));



                foreach (RepeaterItem i in rptDomesticWorker.Items)
                {

                    ItemSerialNo = i.ItemIndex + 1;
                    TextBox txtName = (TextBox)i.FindControl("txtName");
                    DropDownList ddlGender = (DropDownList)i.FindControl("ddlGender");
                    TextBox txtDOB = (TextBox)i.FindControl("txtDateOfBirth");
                    DropDownList ddlNationality = (DropDownList)i.FindControl("ddlNationality");
                    TextBox txtCPR = (TextBox)i.FindControl("txtCPRNumber");
                    TextBox txtPassport = (TextBox)i.FindControl("txtPassport");
                    DropDownList ddlOccupation = (DropDownList)i.FindControl("ddlOccupationDetails");
                    TextBox txtOthers = (TextBox)i.FindControl("txtOtherOccupationDetails");
                    TextBox txtFlatNo = (TextBox)i.FindControl("txtFlatNo");
                    TextBox txtBuidlingNo = (TextBox)i.FindControl("txtBuildingNumber");
                    TextBox txtBlockNo = (TextBox)i.FindControl("txtBlockNo");
                    TextBox txtRoadNo = (TextBox)i.FindControl("txtRoadNo");
                    TextBox txtTown = (TextBox)i.FindControl("txtTown");



                    Name = !string.IsNullOrEmpty(txtName.Text) ? txtName.Text : "";
                    Gender = ddlGender.SelectedItem != null ? ddlGender.SelectedItem.Value : "";
                    DOB = txtDOB.Text;
                    Nationality = ddlNationality.SelectedItem != null ? ddlNationality.SelectedItem.Value : "";
                    CPR = !string.IsNullOrEmpty(txtCPR.Text) ? txtCPR.Text : "";
                    Passport = !string.IsNullOrEmpty(txtPassport.Text) ? txtPassport.Text : "";
                    Occupation = !string.IsNullOrEmpty(ddlOccupation.SelectedItem.Value) ? ddlOccupation.SelectedItem.Value : "";
                    Others = !string.IsNullOrEmpty(txtOthers.Text) ? txtOthers.Text : "";
                    FlatNo = !string.IsNullOrEmpty(txtFlatNo.Text) ? txtFlatNo.Text : "";
                    BuildingNo = !string.IsNullOrEmpty(txtBuidlingNo.Text) ? txtBuidlingNo.Text : "";
                    BlockNo = !string.IsNullOrEmpty(txtBlockNo.Text) ? txtBlockNo.Text : "";
                    RoadNo = !string.IsNullOrEmpty(txtRoadNo.Text) ? txtRoadNo.Text : "";
                    Town = !string.IsNullOrEmpty(txtTown.Text) ? txtTown.Text : "";

                    domesticdt.Rows.Add(ItemSerialNo, ItemSerialNo, Name, Gender, DOB, Nationality, CPR, Passport, Occupation, FlatNo, BuildingNo, BlockNo, RoadNo, Town, Others);

                }
                return domesticdt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!master.IsSessionAvailable())
                {
                    master.RedirectToLogin();
                }
                if (Request.QueryString["Ref"] != null && Request.QueryString["InsuredCode"] != null)
                {
                    var userinfo = Session["UserInfo"] as OAuthTokenResponse;
                    string InsuredCode = Request.QueryString["InsuredCode"];
                    int domesticID = Convert.ToInt32(Request.QueryString["Ref"]);
                    var domesticHelp = new DomesticHelpPolicy();
                    domesticHelp.InsurancePeroid = Convert.ToInt32(ddlInsurancePeriod.SelectedItem.Value);
                    domesticHelp.NoOfDomesticWorkers = Convert.ToInt32(ddlNoofDomesticWorker.SelectedItem.Value);
                    domesticHelp.PolicyStartDate = txtPolicyStartDate.Text.CovertToCustomDateTime();
                    domesticHelp.DomesticWorkType = ddlDomesticWorkerType.SelectedItem.Value;
                    domesticHelp.IsPhysicalDefect = ddlPhysicalDefect.SelectedItem.Value;
                    domesticHelp.IsSaved = false;
                    if (domesticHelp.IsPhysicalDefect == "Yes")
                    {
                        domesticHelp.PhysicalDefectDescription = txtPhysicalDescription.Text;
                    }
                    domesticHelp.InsuredCode = hdnInsuredCode.Value;
                    domesticHelp.DomesticID = Convert.ToInt32(hdnDomesticID.Value);
                    domesticHelp.PremiumBeforeDiscount = Convert.ToDecimal(txtTotalPremium.Text);
                    domesticHelp.PremiumAfterDiscount = Convert.ToDecimal(txtDiscountPremium.Text);
                    domesticHelp.LoadAmount = string.IsNullOrEmpty(txtLoadAmount.Text) ? 0 : Convert.ToDecimal(txtLoadAmount.Text);
                    domesticHelp.DiscountAmount = string.IsNullOrEmpty(txtDiscountAmount.Text) ? 0 : Convert.ToDecimal(txtDiscountAmount.Text);
                    domesticHelp.Remarks = txtRemarks.Text;
                    List<DomesticHelpMember> members = new List<DomesticHelpMember>();

                    DataTable DomesticWokerdt = GetDomesticWorkerDetails();
                    DateTime expirydate = getexpiryDate(Convert.ToInt32(ddlInsurancePeriod.SelectedItem.Value));
                    members = (from DataRow dr in DomesticWokerdt.Rows

                               select new DomesticHelpMember
                               {
                                   InsuredCode = InsuredCode,
                                   Name = Convert.ToString(dr["Name"]),
                                   SumInsured = 0,
                                   PremiumAmount = 0,
                                   ExpiryDate = expirydate,
                                   AddressType = Convert.ToString(dr["FlatNo"]) + Convert.ToString(dr["BuildingNo"]) + Convert.ToString(dr["RoadNo"]) + Convert.ToString(dr["BlockNo"]) + Convert.ToString(dr["Town"]),
                                   Occupation = Convert.ToString(dr["Occupation"]),
                                   Nationality = Convert.ToString(dr["Nationality"]),
                                   Passport = Convert.ToString(dr["Passport"]),
                                   DOB = Convert.ToString(dr["DateOfBirth"]).CovertToCustomDateTime(),
                                   Sex = Convert.ToChar(dr["Sex"]),
                                   ItemserialNo = Convert.ToInt32(dr["ItemSerialNo"]),
                                   DateOfSubmission = DateTime.Now,
                                   CommencementDate = domesticHelp.PolicyStartDate,
                                   CPRNumber = Convert.ToString(dr["CPR"]),
                                   OtherOccupation = Convert.ToString(dr["Others"]),
                                   //Todo
                                   //Need to pass userinfo.UserId.
                                   CreatedBy = Convert.ToInt32(1)


                               }).ToList();


                    var updaterequest = new UpdateDomesticInsuranceDetailsRequest();
                    updaterequest.DomesticHelp.DomesticID = domesticID;
                    updaterequest.DomesticHelp = domesticHelp;
                    updaterequest.DomesticHelpMemberList = members;
                    var service = master.GetService();
                    var updatedata = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateDomesticInsuranceDetailsResponse>, BKIC.SellingPoint.DTO.RequestResponseWrappers.UpdateDomesticInsuranceDetailsRequest>
                     (BKIC.SellingPoint.DTO.Constants.DomesticURI.UpdateDomesticDetails, updaterequest);

                    if (updatedata.StatusCode == 200)
                    {
                        if (updatedata.Result.IsTransactionDone)
                        {
                            // lblMessage.InnerText = "Updated Successfull";
                            successmsg.Visible = true;
                        }
                        else
                        {
                            // lblMessage.InnerText = updatedata.Result.TransactionErrorMessage;
                            errormsg.Visible = true;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime getexpiryDate(int years)
        {
            DateTime ExpiryDate;
            DateTime date = DateTime.Now.AddYears(years);
            ExpiryDate = date.AddDays(-1);
            return ExpiryDate;

        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
           // master.RedirectToPreviousURl();
        }

        protected void WorkerDetailsDependentRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                if (Nationalitydt != null && Nationalitydt.Rows.Count > 0)
                {
                    DropDownList ddl = (DropDownList)e.Item.FindControl("ddlNationality");
                    ddl.DataValueField = "Code";
                    ddl.DataTextField = "Description";
                    ddl.DataSource = Nationalitydt;
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
                if (Genderdt != null && Genderdt.Rows.Count > 0)
                {
                    DropDownList ddlGender = (DropDownList)e.Item.FindControl("ddlGender");
                    ddlGender.DataValueField = "Value";
                    ddlGender.DataTextField = "Text";
                    ddlGender.DataSource = Genderdt;
                    ddlGender.DataBind();
                    ddlGender.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
                if (Occupationdt != null && Occupationdt.Rows.Count > 0)
                {
                    DropDownList ddlGender = (DropDownList)e.Item.FindControl("ddlOccupationDetails");
                    ddlGender.DataValueField = "Occupation";
                    ddlGender.DataTextField = "Occupation";
                    ddlGender.DataSource = Occupationdt;
                    ddlGender.DataBind();
                    ddlGender.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
                if (drv != null && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Nationality"])))
                {
                    DropDownList ddlNationality = (DropDownList)e.Item.FindControl("ddlNationality");
                    ddlNationality.SelectedValue = Convert.ToString(drv.Row["Nationality"]);

                }
                if (drv != null && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Sex"])))
                {
                    DropDownList ddlNationality = (DropDownList)e.Item.FindControl("ddlGender");
                    ddlNationality.SelectedValue = Convert.ToString(drv.Row["Sex"]);

                }
                if (drv != null && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Occupation"])))
                {
                    DropDownList ddlNationality = (DropDownList)e.Item.FindControl("ddlOccupationDetails");
                    ddlNationality.SelectedValue = Convert.ToString(drv.Row["Occupation"]);

                }
            }

        }
        protected void ddlOccupationDetails_SelectedIndexChanged1(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            int RepeaterItemIndex = ((RepeaterItem)ddl.NamingContainer).ItemIndex;
            if (ddl.SelectedItem.Value == "Others")
            {
                foreach (RepeaterItem i in rptDomesticWorker.Items)
                {
                    if (i.ItemIndex == RepeaterItemIndex)
                    {
                        HtmlGenericControl otherdetaildiv = (HtmlGenericControl)(i.FindControl("othersDiv"));
                        otherdetaildiv.Visible = true;
                    }
                }
            }
            else
            {
                foreach (RepeaterItem i in rptDomesticWorker.Items)
                {
                    if (i.ItemIndex == RepeaterItemIndex)
                    {
                        HtmlGenericControl otherdetaildiv = (HtmlGenericControl)(i.FindControl("othersDiv"));
                        otherdetaildiv.Visible = false;
                    }
                }
            }
        }

        protected void txtLoadAmount_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPremium.Text = Convert.ToString(Convert.ToDecimal(txtDiscountPremium.Text) + Convert.ToDecimal(txtLoadAmount.Text));
        }

        protected void txtDiscountAmount_TextChanged(object sender, EventArgs e)
        {
            txtDiscountPremium.Text = Convert.ToString(Convert.ToDecimal(txtDiscountPremium.Text) - Convert.ToDecimal(txtDiscountAmount.Text));
        }
    }
}