﻿<%@ Page Title="" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="TravelPolicyDetail.aspx.cs" Inherits="BKIC.SellingPoint.Presentation.TravelPolicyDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="x_panel">
                        <div class="x_title">
                            <h2>Travel Policy details:</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-horizontal form-label-left col-md-12">

                                <div class="form-group col-md-6">
                                    <div class="col-md-4 page-label">
                                        <label class="control-label pull-right">CPR:</label>
                                    </div>
                                    <div class="col-md-8 page-control">
                                        <div class="pull-left">
                                            <label id="lblcpr" runat="server" class="control-label"></label>
                                            <%--<span runat="server" id="cpr"  class="form-control col-md-10"></span>--%>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <div class="col-md-4 page-label">
                                        <label class="control-label pull-right">InsuredName:</label>
                                    </div>
                                    <div class="col-md-8 page-control">
                                        <div class="pull-left">
                                            <label id="lblInsured" runat="server" class="control-label"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="col-md-4 page-label">
                                        <label class="control-label pull-right">Policy Expired Date:</label>
                                    </div>
                                    <div class="col-md-8 page-control">
                                        <div class="pull-left">
                                            <label id="lblPolicyExpDate" runat="server" class="control-label"></label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>

                            </div>
                        </div>
        <div class="x_content">

            <div class="x_title">
                            <h2>Travel Member details:</h2>
                            <%--<ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>--%>
                            <div class="clearfix"></div>
                        </div>

                            <asp:GridView ID="gvTravelDetail" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Width="100%" PageSize="10" CssClass="table table-striped">

                                <Columns>
                                    <asp:TemplateField HeaderText="S.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text=" <%# Container.DataItemIndex + 1 %>"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <%--<asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-CssClass="DateKeyHide" HeaderStyle-CssClass="DateKeyHide" />--%>
                                    <asp:BoundField DataField="ItemName" HeaderText="Name" SortExpression="Name" />
                                    <%--<asp:BoundField DataField="Category" HeaderText="Relationship" SortExpression="Name" />--%>
                                    <asp:BoundField DataField="CPR" HeaderText="CPR" SortExpression="Name" />
                                    <asp:BoundField DataField="DateOfBirth" HeaderText="DOB" SortExpression="Name" />
                                    <asp:BoundField DataField="Passport" HeaderText="Passport No" SortExpression="Name" />
                                    <%--<asp:BoundField DataField="Nationality" HeaderText="Nationality" SortExpression="Name" />--%>
                                    <asp:BoundField DataField="OccupationCode" HeaderText="Occupation" SortExpression="Name" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
</asp:Content>
