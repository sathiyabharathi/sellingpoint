﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/General.Master"  CodeBehind="DomesticHelpEdit.aspx.cs" Inherits="BKIC.SellingPoint.Presentation.DomesticHelpEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="pnlDomestic">
        <ContentTemplate>
            <div class="content-wrapper-whole">
            <div class="col-md-12 sub-header-fixed">
                <div class="header-wrap-sub">
                        <ul class="header-sub-ul">
                             <li class="header-sub-ul-li success-msg">
                                     <label runat="server" id="lblMessage"></label>
                                </li>
                            <li class="header-sub-ul-li">
                                 <asp:Button runat="server" ID="btnUpdate" Text="Update Insurance" OnClick="btnUpdate_Click" />
                            </li>
                            <li class="header-sub-ul-li">
                                 <asp:Button runat="server" ID="btnBack" Text="Back" OnClick="btnBack_Click" />
                            </li>
                            </ul>
                    </div>
            </div>
                  <div class="alert alert-success pop-up-msg" runat="server" id="successmsg" visible="false">
                        <div class="popup-msg">
                            <i class="fa fa-times"></i>
                        <strong>Updated Successfull</strong>
                            </div>
                    </div>
                    <div class="alert alert-success pop-up-msg" runat="server" id="errormsg" visible="false">
                        <div class="popup-msg error-popup">
                           <i class="fa fa-times"></i>
                        <strong>Error</strong>
                            </div>
                    </div>
                <div class="seperator-min-ht-50"></div>
            <div class="col-md-12 view-h2">
                <h2>Personal Accident</h2>
            </div>
            <asp:HiddenField runat="server" ID="hdnDomesticID" />
            <asp:HiddenField runat="server" ID="hdnInsuredCode" />
            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 clearfix list-label">
                    <div class="col-md-6 decol">
                        <label id="lblInsurancePeriod">Insurance Period(Years)</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList runat="server" ID="ddlInsurancePeriod"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 clearfix list-label">
                    <div class="col-md-6 decol">
                        <label id="lblNoofDomesticWorker">Number of the domestic workers to be covered</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList runat="server" ID="ddlNoofDomesticWorker" OnSelectedIndexChanged="ddlNoOfWorkersPolicy_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-12 view-h2">
                <h2>Policy Start Date</h2>
                <div class="clearfix list-label">
                    <div class="col-md-6">
                        <div class="col-md-6 decol">
                            <label id="lblCommencementDate">Commencement Date</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox runat="server" ID="txtPolicyStartDate"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 view-h2">
                <h2>Personal Statement</h2>
            </div>
            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 clearfix list-label">
                    <div class="col-md-6 decol">
                        <label id="lblDomestic">Is the insured Domestic worker or employed under a business?</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList runat="server" ID="ddlDomesticWorkerType"></asp:DropDownList>
                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 clearfix list-label">
                    <div class="col-md-6 decol">
                        <label id="lblPhysicalDefect">Do Any of the persons to be Insured Have Any physical Defect Infirmity,Abnormality or Medical Condition?</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList runat="server" ID="ddlPhysicalDefect">
                            <asp:ListItem Value="" Text="-PleaseSelect-"></asp:ListItem>
                            <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="no" Text="No"></asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix list-label" runat="server" id="physdescDiv">
                <div class="col-md-6 clearfix list-label">
                    <div class="col-md-6 decol">
                        <label id="lblMoreDetail">Please provide More Details</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" ID="txtPhysicalDescription"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div>
                <asp:Repeater runat="server" ID="rptDomesticWorker" OnItemDataBound="WorkerDetailsDependentRepeater_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-12 view-h2">
                            <h2><%#"Domestic Worker"+ DataBinder.Eval(Container.DataItem, "Count").ToString() %></h2>
                        </div>
                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblName">Name</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblSex">Sex</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlGender"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblDateofBirth">DateofBirth</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" CssClass="datepicker" ID="txtDateOfBirth" Text='<%# Eval("DATEOFBIRTH") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblNationality">Nationality</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlNationality"></asp:DropDownList>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblCPRNumber">CPR Number</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtCPRNumber" CssClass="numbersOnly" Text='<%# Eval("CPR") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblPassport">Passport</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtPassport" Text='<%# Eval("Passport") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblOccupation">Occupation</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:DropDownList runat="server" ID="ddlOccupationDetails" OnSelectedIndexChanged="ddlOccupationDetails_SelectedIndexChanged1" AutoPostBack="true"></asp:DropDownList>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 clearfix list-label" runat="server" id="othersDiv" visible="false">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label>
                                        If Others, please specify
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtOtherOccupationDetails" Text='<%# Eval("Others") %>'></asp:TextBox>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 view-h2">
                            <h2>Home Address In Bahrain</h2>
                        </div>
                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblFlatNumber">FlatNumber</label>
                                </div>

                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtFlatNo" Text='<%# Eval("FlatNo") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblBulidingNumber">BulidingNumber</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtBuildingNumber" Text='<%# Eval("BuildingNo") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblBlockNumber">BlockNumber</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtBlockNo" Text='<%# Eval("BlockNo") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblRoadNumber">RoadNumber</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtRoadNo" Text='<%# Eval("RoadNo") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 clearfix list-label">
                            <div class="col-md-6 clearfix list-label">
                                <div class="col-md-6 decol">
                                    <label id="lblTown">Town</label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox runat="server" ID="txtTown" Text='<%# Eval("Town") %>'></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                
                <div class="col-md-12 clearfix list-label">
                    <div class="col-md-6 list-label">
                        <div class="col-md-6 decol">
                            <label>Total Premium</label>
                        </div>
                        <div class="col-md-6">
                            <asp:TextBox runat="server" TextMode="Number" ID="txtTotalPremium"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Discount Premium</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" TextMode="Number" ID="txtDiscountPremium"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Load Amount</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" TextMode="Number" ID="txtLoadAmount" AutoPostBack="true" OnTextChanged="txtLoadAmount_TextChanged" CausesValidation="true"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Discount Amount</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox runat="server" TextMode="Number" ID="txtDiscountAmount" AutoPostBack="true" OnTextChanged="txtDiscountAmount_TextChanged" CausesValidation="true"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Code</label>
                    </div>
                    <div class="col-md-6">
                        <asp:DropDownList runat="server" ID="DropDownList1">
                            <asp:ListItem>--Please Select--</asp:ListItem>
                            <asp:ListItem Text="Yes"> </asp:ListItem>
                            <asp:ListItem Text="No"> </asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix list-label">
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Remarks</label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txtRemarks"></asp:TextBox>
                    </div>
                </div>
            </div>
           
         
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
