﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General.Master" CodeBehind="TravelInsuranceEdit.aspx.cs" Inherits="BKIC.SellingPoint.Presentation.TravelInsuranceEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="travelEdit">
        <ContentTemplate>
            <asp:Panel runat="server" ID="travelInsurance">
                <div class="content-wrapper-whole">
                    <div class="col-md-12 sub-header-fixed">
                        <div class="header-wrap-sub">
                            <ul class="header-sub-ul">
                                <li class="header-sub-ul-li"></li>
                                <li class="header-sub-ul-li">
                                    <asp:Button ID="BtnUpdateInsurance" Text="Update Insurance" runat="server" OnClick="BtnUpdateInsurance_Click" />
                                </li>
                                <li class="header-sub-ul-li">
                                    <asp:Button ID="BtnBackToInsurance" Text="Back To Insurance" runat="server" OnClick="BtnBackToInsurance_Click" />
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="alert alert-success pop-up-msg" runat="server" id="successmsg" visible="false">
                        <div class="popup-msg">
                            <i class="fa fa-times"></i>
                        <strong>Updated Successfull</strong>
                            </div>
                    </div>
                    <div class="alert alert-success pop-up-msg" runat="server" id="errormsg" visible="false">
                        <div class="popup-msg error-popup">
                           <i class="fa fa-times"></i>
                        <strong>Error</strong>
                            </div>
                    </div>
                    <div class="seperator-min-ht-50"></div>
                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6 travel-h2">
                            <h2>TRAVEL</h2>
                            <div class="col-md-6 decol">
                                <label id="lblPackage">Package</label>
                            </div>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlPackage" OnSelectedIndexChanged="ddlPackage_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblPolicyPeriod">Policy Period</label>
                            </div>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlPolicyPeroidDetails"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblCoverage">Coverage Type</label>
                            </div>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlCoverageType"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6 travel-h2">
                            <h2>RISK DETAILS</h2>
                            <div class="col-md-6 decol">
                                <label id="lblCommencementDate">Commencement Date</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtCommencementDate" CssClass="datepicker"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label>Passport No</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtPasportNo"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblOccupation">Occupation</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtOccupation"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblFFPNo">FFP No</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtFFPNo"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6 travel-h2">
                            <h2>Questionaire</h2>
                            <div class="col-md-6 decol">
                                <label id="lblPhysicalDefect">Do any of the persons(s) to be insured have any physical defect, infirmity, abnormality, or medical condition?</label>
                            </div>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlphysicaldefect">
                                    <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="no"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label" runat="server" id="physdescDiv" visible="false">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblPhysicalDefectDesc">Physical Description</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtphysicaldefectDesc" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-md-12">
                        <div class="">
                            <asp:Button runat="server" ID="btnAdd" Text="ADD Member" OnClick="btnAdd_Click" Visible="false" CssClass="btn btn-buttons" />
                            <asp:Button runat="server" ID="btnRemove" Text="Remove Member" OnClick="btnRemove_Click" Visible="false" CssClass="btn btn-buttons" />
                        </div>
                    </div>
                    <div runat="server" id="Dependentdiv">
                        <asp:Repeater runat="server" ID="DependentRepeater" OnItemDataBound="DependentRepeater_ItemDataBound1">
                            <ItemTemplate>
                                <h2>
                                    <%--<asp:Label runat="server" ID="lbldependentcount" Text='<%#"Dependant Members"+ DataBinder.Eval(Container.DataItem, "Count").ToString() %>'></asp:Label></h2>--%>
                                    <%#"Dependant Members"+ DataBinder.Eval(Container.DataItem, "Count").ToString() %>
                                </h2>
                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label id="lblDependentName">Name</label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtDependentName" runat="server" Text='<%# Eval("ITEMNAME") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label id="lblName">Date Of Birth</label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtDependentDOB" CssClass="datepicker" runat="server" Text='<%# Eval("DATEOFBIRTH") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label id="lblSex">Sex</label>

                                        </div>
                                        <div class="col-md-6">
                                            <asp:DropDownList runat="server" ID="ddlDependentGender"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix list-label">

                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label id="lblCPR">CPR</label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtDependentCPR" CssClass="numbersOnly" runat="server" Text='<%# Eval("CPR") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label>Passport No</label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtDependentPassport" runat="server" Text='<%# Eval("PASSPORT") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label id="lblNationality">Nationality</label>

                                        </div>
                                        <div class="col-md-6">
                                            <asp:DropDownList runat="server" ID="ddlNationalityDependent"></asp:DropDownList>

                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label id="lblRelationship">Relationship</label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:DropDownList runat="server" ID="ddlRelationship"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix list-label">
                                    <div class="col-md-6">
                                        <div class="col-md-6 decol">
                                            <label>Occupation</label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtDependentOccupation" runat="server" Text='<%# Eval("OCCUPATIONCODE") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label>Premium Amount</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtPremium"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label>Discount Premium Amount</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtDiscountPremium"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblLoadAmount">Load Amount</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtLoadAmount" AutoPostBack="true" CausesValidation="true" OnTextChanged="txtLoadAmount_TextChanged"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblDiscountAmount">Discount Amount</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtDiscountAmount" AutoPostBack="true" CausesValidation="true" OnTextChanged="txtDiscountAmount_TextChanged"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblCode">Code</label>
                                <div>
                                    <!-- asp ignored -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <select>
                                    <option value="Code1">Code1</option>
                                    <option value="Code2">Code2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 clearfix list-label">
                        <div class="col-md-6">
                            <div class="col-md-6 decol">
                                <label id="lblRemark">Remark</label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtRemark" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>



                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

