﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General.Master" CodeBehind="TravelInsuranceView.aspx.cs" Inherits="BKIC.SellingPoint.Presentation.TravelInsuranceView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper-whole">
        <div class="col-md-12 sub-header-fixed">
            <div class="header-wrap-sub">
                <ul class="header-sub-ul">
                    <li class="header-sub-ul-li">
                        <asp:Button runat="server" ID="btnback" Text="Back" OnClick="btnback_Click" />
                    </li>
                    <li class="header-sub-ul-li">
                        <a runat="server" id="downloadschedule" title="Schedule" download="filename" visible="false"><i class="fa fa-download" aria-hidden="true">Schedule</i></a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="seperator-min-ht-50"></div>
        <div class="col-md-10 label-list travelinsuranceextra dashboard ">
            <div class="col-md-12">
                <div class="row view-h2">
                    <div class="col-md-5">
                        <h2>View Details Travel Insurance</h2>
                    </div>
                    <div class="col-md-2 pull-right">
                    </div>
                </div>
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Policy No</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblPolicyNo"></asp:Label>
                    </div>
                </div>


                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>CPR</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblCPR"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Insured</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblInsuredName"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Start Date</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblStartDate"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Expiry Date</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblExpiryDate"></asp:Label>
                    </div>
                </div>
                 <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Coverage Type</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblCoverage"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Period of Cover</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblPeroidOfCover"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 view-h2">
                    <h2>Travel Details</h2>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Policy Type</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblPolicyType"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Main Class</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblMainClass"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Sum Insured(USD)</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblSumInsured"></asp:Label>
                    </div>
                </div>


                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Nationality</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblNationality"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>Passport No</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblPassportNo"></asp:Label>
                    </div>
                </div>

                <div class="col-md-6 list-label">
                    <div class="col-md-6 decol">
                        <label>FFP Number</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblFFPNumber"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 view-h2">
                    <h2>QUESTIONNAIRE</h2>
                </div>
                <div class="col-md-6 list-label">
                    <div class="col-md-6 largelbl">
                        <span>Do any of the persons(s) to be insured have any physical defect, infirmity, abnormality, or medical condition?</span>
                    </div>
                    <div class="col-md-6 largelblresult aa">
                        <asp:Label runat="server" ID="lblPhysicalDefect"></asp:Label>
                    </div>
                </div>
                <div class="col-md-6 list-label" runat="server" id="PhysicalDescDiv">
                    <div class="col-md-6 decol">
                        <label>Physical Description</label>
                    </div>
                    <div class="col-md-6 aa">
                        <asp:Label runat="server" ID="lblPhysicalDesc"></asp:Label>
                    </div>
                </div>


                <asp:Repeater runat="server" ID="rptDependentDiv">
                    <ItemTemplate>
                        <div class="col-md-12 view-h2">
                            <h2>
                                <asp:Label runat="server" ID="lbldependentcount" Text='<%#"Travel Members "+Eval("Count") %>'></asp:Label>

                            </h2>
                        </div>

                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label>Name</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="lblDependentName" Text='<%# Eval("ITEMNAME") %>'></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label id="lblName">Date Of Birth</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label5" Text='<%# Eval("DATEOFBIRTH") %>'></asp:Label>
                            </div>
                        </div>


                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label id="lblSex">Sex</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label6" Text='<%# Eval("SEX") %>'></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label>CPR</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label1" Text='<%# Eval("CPR") %>'></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label>Passport No</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label2" Text='<%# Eval("PASSPORT") %>'></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label id="lblNationality">Nationality</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label3" Text='<%# Eval("MAKE") %>'></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label>Relationship</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label4" Text='<%# Eval("CATEGORY") %>'></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6 list-label">
                            <div class="col-md-6 decol">
                                <label>Occupation</label>
                            </div>
                            <div class="col-md-6 aa">
                                <asp:Label runat="server" ID="Label7" Text='<%# Eval("OCCUPATIONCODE") %>'></asp:Label>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div class="col-md-12 count-section">
            <div class="count-cal price-count">
                <span>Your Total Premium:</span>
                <asp:Label runat="server" ID="lblTotalPremium"></asp:Label>
            </div>
            <div class="count-cal price-count">
                <span>Your Discount Premium</span>
                <asp:Label runat="server" ID="lblDiscountPremium"></asp:Label>
            </div>
        </div>
    </div>

</asp:Content>

