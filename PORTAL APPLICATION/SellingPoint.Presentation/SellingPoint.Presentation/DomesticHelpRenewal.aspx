﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/General.master" CodeBehind="DomesticHelpRenewal.aspx.cs" Inherits="BKIC.SellingPoint.Presentation.DomesticHelpRenewal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="panel">
        <header class="panel-heading tab-bg-dark-navy-blue fcolorwhite ">
            Travel Insurance-Due For Renewal
                               <%--<span style="float: right;">Motor Insurance
                               </span>--%>
        </header>
        <div class="panel-body">
            <div class="row">
                <%--   <div class="col-lg-3">
                    <asp:LinkButton ID="lbtnnew" runat="server" OnClick="lbtnnew_Click" CssClass="btn btn-round btn-info"><i class="fa  fa-plus-square"></i>  Add New</asp:LinkButton>
                    <asp:Label ID="lblcid" runat="server" Text="" Visible="false"></asp:Label>
                </div>
                <div class="col-lg-9">
                </div>--%>
            </div>

        </div>

        <div class="form-group Ahas-success">
            <label class="col-sm-2 control-label col-lg-2"></label>
            <asp:Label runat="server" ID="lbler"></asp:Label>
            <div class="col-lg-12 Atitle-content">

                <div class="col-md-6 pad key-fields">
                    <asp:Label ID="lblDocNumber" runat="server" Text="Key"></asp:Label>
                    <asp:TextBox runat="server" ID="txtSearchKey"></asp:TextBox>
                    <%--<asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" />--%>
                </div>
                <div class="pull-right export">
                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    <%--<asp:Button runat="server" ID="btnExport" Text="Export" OnClick="btnExport_Click" />--%>
                </div>
            </div>
             <div class="custompopup" id="divThankYou" runat="server" visible="false">
                <p>
                    <asp:HiddenField runat="server" ID="hdnLinkID" />
                    <asp:Label ID="lblmessage" runat="server"></asp:Label>
                </p>
                <br />
              <%--  <asp:Button ID="btndelconf" CssClass="classname leftpadding" runat="server" Text="Ok" OnClientClick="showPageLoader();" OnClick="btndelconf_Click" />
                <asp:Button ID="btndelcan" CssClass="classname leftpadding" runat="server" Text="Cancel" OnClick="btndelcan_Click" />--%>
            </div>
        <div class="panel-body">
            <div class="adv-table editable-table ">
                <asp:GridView ID="gvHomeInsurance" runat="server" CssClass="table table-bordered" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="10" Width="100%">
                    <HeaderStyle CssClass="bcolorhead fcolorwhite" />
                    <Columns>
                        <asp:TemplateField HeaderText="S.No">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text=" <%# Container.DataItemIndex + 1 %>"></asp:Label>
                                <asp:Label ID="lblMotorID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblHIRStatusCode" runat="server" Text='<%# Eval("HIRStatusCode") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblLinkID" runat="server" Text='<%# Eval("LinkID") %>' Visible="false"></asp:Label>
                                 <asp:Label ID="lblInsuredCode" runat="server" Text='<%# Eval("InsuredCode") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--  <asp:BoundField DataField="InsuredCode" HeaderText="INSUREDCODE" SortExpression="CreatedDate" HeaderStyle-ForeColor="White" />--%>

                        <asp:BoundField DataField="DocumentNo" HeaderText="DOCUMENTNO" SortExpression="CreatedDate" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="CPR" HeaderText="CPR" SortExpression="UpdatedDate" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="MAINCLASS" HeaderText="MainClass" SortExpression="UpdatedDate" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="SUBCLASS" HeaderText="SUBCLASS" SortExpression="UpdatedDate" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="InsuredName" HeaderText="INSURED NAME" SortExpression="CreatedDate" HeaderStyle-ForeColor="White" />
                        <%--<asp:BoundField DataField="CommenceDate" HeaderText="PolicyStartDate" SortExpression="UpdatedDate" HeaderStyle-ForeColor="White" />--%>
                        <asp:BoundField DataField="ExpiryDate" HeaderText="ExpiryDate" SortExpression="CreatedDate" HeaderStyle-ForeColor="White" />
                        <asp:BoundField DataField="NetPremium" HeaderText="Premium" SortExpression="UpdatedDate" HeaderStyle-ForeColor="White" />

                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                  <%--              <asp:LinkButton ID="lnkbtnViewDetails" runat="server" ToolTip="View Details" CssClass="fsize fcolorgreen" OnClick="lnkbtnViewDetails_Click"><i class="fa  fa-search"></i></asp:LinkButton>
                                 <asp:LinkButton ID="lnkbtnDelete" runat="server" ToolTip="Delete" CssClass="fsize fcolorred" OnClick="lnkbtnDelete_Click"><i class="fa  fa-trash-o"></i></asp:LinkButton>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        </div>
    </section>
</asp:Content>