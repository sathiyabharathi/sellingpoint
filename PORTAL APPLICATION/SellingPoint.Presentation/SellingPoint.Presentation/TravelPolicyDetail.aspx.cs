﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BKIC.SellingPoint.Presentation
{
    public partial class TravelPolicyDetail : System.Web.UI.Page
    {
        General master;

        public TravelPolicyDetail()
        {
            master = Master as General;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            master = Master as General;
            var service = master.GetService();

            if (Request.QueryString["TravelId"] != null && Request.QueryString["InsuredCode"] != null)
            {
                int TravelId = Convert.ToInt32(Request.QueryString["TravelId"]);
                string InsuredCode = Convert.ToString(Request.QueryString["InsuredCode"]);
                BindTravelPolicyPage(TravelId, InsuredCode);
            }
        }

        private void BindTravelPolicyPage(int TravelId, string insuredCode)
        {
            var service = master.GetService();

            var client = new BKIC.SellingPoint.Presentation.ClientUtility();
            client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);

            var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyUserRequest();
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;

            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                req.AgentBranch = userInfo.AgentBranch;
                req.AgentCode = userInfo.AgentCode;
                //req.Agency = userInfo.;

            }
            var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuotation.Replace("{travelQuotationId}", TravelId.ToString()).Replace("{userInsuredCode}", insuredCode).Replace("{type}","test");
            var travelDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelSavedQuotationResponse>>(url);
            
            var insuredDetail = travelDetails.Result.InsuredDetails;
            var travPolicydetail = travelDetails.Result.TravelInsurancePolicyDetails;
            var travelMem = travelDetails.Result.TravelMembers;
            //var res3 = travelDetails.Result.InsuredDetails;

            //gV
            gvTravelDetail.DataSource = travelMem;
            gvTravelDetail.DataBind();

            //Details
            lblInsured.InnerText = travPolicydetail.InsuredName;
            lblcpr.InnerText = travPolicydetail.CPR;
            lblPolicyExpDate.InnerText = travPolicydetail.ExpiryDate.ConvertToLocalFormat();

            //var url = BKIC.SellingPoint.DTO.BKICUri.HomeInsurance.FetchHomeRenewalDetails.Replace("{cpr}", cpr).Replace("{documentNo}", doc);
            //var homeDetails = service.GetData<KBIC.DTO.RequestResponseWrappers.ApiResponseWrapper<KBIC.DTO.RequestResponseWrappers.RenewalHomeInsuranceResponse>>(url);
            //if (homeDetails.StatusCode == 200 && homeDetails.Result.IsTransactionDone)
            //{
            //    HomeSavedQuotationResult phomedetails = new HomeSavedQuotationResult();
            //    phomedetails.HomeInsurancePolicy = homeDetails.Result.HomeInsurancePolicy;
            //    phomedetails.homesubitems = homeDetails.Result.HomeSubItems;
            //    phomedetails.DomesticHelp = homeDetails.Result.DomesticHelp;

            //    renewhomeid.Value = Convert.ToString(phomedetails.HomeInsurancePolicy.HomeID);

            //    /*REview Bind*/
            //    PreviewPremiumAmountPolicyDetails.InnerText = Convert.ToString(phomedetails.HomeInsurancePolicy.PremiumBeforeDiscount);
            //    //  PreviewDiscountAmountPolicyDetails.InnerText = Convert.ToString(phomedetails.HomeInsurancePolicy.PremiumAfterDiscount);

            //    //PreviewDiscountPolicyDetails.Visible = true;
            //    //PreviewDiscountAmountPolicyDetails.Visible = true;

            //    CPRPreview.InnerText = phomedetails.HomeInsurancePolicy.CPR;
            //    InsuredNamePreview.InnerText = phomedetails.HomeInsurancePolicy.InsuredName;
            //    StartDatePreview.InnerText = phomedetails.HomeInsurancePolicy.PolicyStartDate.CovertToLocalFormat();
            //    StatusPreview.InnerText = "Pending";
            //    InsuredNamePreview.InnerText = phomedetails.HomeInsurancePolicy.InsuredName;
            //    ExpiryDatePreview.InnerText = phomedetails.HomeInsurancePolicy.PolicyExpiryDate.CovertToLocalFormat();
            //    PremiumAmountPreview.InnerText = Convert.ToString(phomedetails.HomeInsurancePolicy.PremiumBeforeDiscount);

            //    buildingValuePreview.InnerText = "BHD " + phomedetails.HomeInsurancePolicy.BuildingValue;
            //    contentValuePreview.InnerHtml = "BHD " + phomedetails.HomeInsurancePolicy.ContentValue;
            //    decimal sum = (Convert.ToDecimal(phomedetails.HomeInsurancePolicy.BuildingValue) + Convert.ToDecimal(phomedetails.HomeInsurancePolicy.ContentValue));
            //    sumInsuredPreview.InnerText = "BHD " + Convert.ToString(sum);


            //    propertyMortaggedPreview.InnerText = phomedetails.HomeInsurancePolicy.IsPropertyMortgaged == 'Y' ? "Yes" : "No";

            //    if (phomedetails.HomeInsurancePolicy.IsPropertyMortgaged == 'Y')
            //    {
            //        divFinancierPreview.Visible = true;
            //        financierPreview.InnerHtml = phomedetails.HomeInsurancePolicy.FinancierCode;
            //    }
            //    else
            //    {
            //        divFinancierPreview.Visible = false;
            //    }

            //    ageOfBuildingPreview.InnerText = phomedetails.HomeInsurancePolicy.BuildingAge + " years";
            //    riotMaliciousCoverPreview.InnerText = phomedetails.HomeInsurancePolicy.IsRiotStrikeDamage == 'Y' ? "Yes" : "No";
            //    singleItemAboveBDPreview.InnerText = phomedetails.HomeInsurancePolicy.IsSingleItemAboveContents == 'Y' ? "Yes" : "No";

            //    if (singleItemAboveBDPreview.InnerText == "Yes")
            //    {
            //        DataTable singleItemDt = new DataTable();
            //        singleItemDt.Columns.Add("Category", typeof(string));
            //        singleItemDt.Columns.Add("MoreDescription", typeof(string));
            //        singleItemDt.Columns.Add("AmountOfItem", typeof(string));
            //        singleItemDt.Columns.Add("Index", typeof(Int32));
            //        int index = 0;

            //        foreach (var list in phomedetails.homesubitems)
            //        {
            //            index++;
            //            singleItemDt.Rows.Add(list.SubItemName, list.Remarks, list.SumInsured, index);
            //        }
            //        rtSingleItemAboveBDPreview.DataSource = singleItemDt;
            //        rtSingleItemAboveBDPreview.DataBind();
            //        divSingleItemAboveBDPreview.Visible = true;
            //    }
            //    else
            //    {
            //        divSingleItemAboveBDPreview.Visible = false;
            //    }
            //    connectionWithAnythingPreview.InnerText = phomedetails.HomeInsurancePolicy.IsPropertyInConnectionTrade == 'Y' ? "Yes" : "No";
            //    last5YearSustainedPreview.InnerText = phomedetails.HomeInsurancePolicy.IsPropertyInsuredSustainedAnyLoss == 'Y' ? "Yes" : "No";
            //    insuranceUnderConstructionWorkPreview.InnerText = phomedetails.HomeInsurancePolicy.IsPropertyUndergoingConstruction == 'Y' ? "Yes" : "No";
            //    phomedetails.HomeInsurancePolicy.IsRequireDomestic = phomedetails.DomesticHelp.Count > 0 ? 'Y' : 'N';
            //    if (phomedetails.DomesticHelp.Count > 0)
            //    {
            //        divNoOfWorkerRepeaterPreview.Visible = true;
            //        divNoOfWorkersPreview.Visible = true;
            //        noOfWorkersPreview.InnerText = Convert.ToString(phomedetails.DomesticHelp.Count);

            //        if (noOfWorkersPreview.InnerText != "0")
            //        {
            //            divNoOfWorkerRepeaterPreview.Visible = true;
            //            DataTable domesticWorkerDetails = new DataTable();
            //            domesticWorkerDetails.Columns.Add("ITEMNAME", typeof(string));
            //            domesticWorkerDetails.Columns.Add("DATEOFBIRTH", typeof(string));
            //            domesticWorkerDetails.Columns.Add("CPR", typeof(string));
            //            domesticWorkerDetails.Columns.Add("Count", typeof(Int32));

            //            int count = 0;


            //            foreach (var list in phomedetails.DomesticHelp)
            //            {
            //                count++;
            //                domesticWorkerDetails.Rows.Add(list.Name, list.DOB.CovertToLocalFormat(), list.CPR, count);
            //            }
            //            rtNoOfWorkerPreview.DataSource = domesticWorkerDetails;
            //            rtNoOfWorkerPreview.DataBind();
            //        }
            //        else
            //        {
            //            divNoOfWorkerRepeaterPreview.Visible = false;
            //        }

            //    }
            //    else
            //    {
            //        divNoOfWorkersPreview.Visible = false;
            //    }

            //    residenceTypePreview.InnerHtml = phomedetails.HomeInsurancePolicy.ResidanceTypeCode;
            //    buildingNoPreview.InnerHtml = phomedetails.HomeInsurancePolicy.BuildingNo;
            //    roadNoPreview.InnerHtml = phomedetails.HomeInsurancePolicy.RoadNo;
            //    townNoPreview.InnerHtml = phomedetails.HomeInsurancePolicy.Town;
            //    blockNoPreview.InnerHtml = phomedetails.HomeInsurancePolicy.BlockNo;
            //    flatNoPreview.InnerHtml = phomedetails.HomeInsurancePolicy.FlatNo;
            //    isPropertyInsuredPreview.InnerHtml = phomedetails.HomeInsurancePolicy.IsSafePropertyInsured == 'Y' ? "Yes" : "No";
            //    domesticHelpCoverPreview.InnerHtml = phomedetails.HomeInsurancePolicy.IsRequireDomestic == 'Y' ? "Yes" : "No";
            //    haveJointOwnershipPreview.InnerHtml = phomedetails.HomeInsurancePolicy.IsJointOwnership == 'Y' ? "Yes" : "No";
            //    underOtherInsurancePreview.InnerHtml = phomedetails.HomeInsurancePolicy.IsPropertyCoveredOtherInsurance == 'Y' ? "Yes" : "No";
            //    renewhomeid.Value = Convert.ToString(phomedetails.HomeInsurancePolicy.HomeID);
            //}
        }
    }
}