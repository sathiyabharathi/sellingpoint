﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnAuthorizedPageAccess.aspx.cs" Inherits="BKIC.SellingPoint.Presentation.UnAuthorizedPageAccess" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div> You are not allowed to access this page.
        </div>
    </form>
</body>
</html>
