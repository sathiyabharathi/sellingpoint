﻿using BKIC.SellingPoint.DTO.RequestResponseWrappers;
using BKIC.SellingPoint.Presentation;
using KBIC.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SellingPoint.Presentation
{
    public partial class HomeEndorsement : System.Web.UI.Page
    {
        private General master;
        public static List<BKIC.SellingPoint.DTO.RequestResponseWrappers.InsuredMasterDetails> InsuredNames { get; set; }
        public static List<BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyHomePolicy> policyList;
        public static long _HomeEndorsementID { get; set; }
        public static string MainClass { get; set; }
        public static bool AjdustedPremium { get; set; }
        public static string SubClass { get; set; }

        public HomeEndorsement()
        {
            master = Master as General;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            master = Master as General;
            if (!Page.IsPostBack)
            {
                BindAgencyClientCodeDropdown();
             
            }
        }

        private void BindAgencyClientCodeDropdown()
        {
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            var dropDownResult = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.FetchDropDownsResponse>>
                  (BKIC.SellingPont.DTO.Constants.DropdownURI.GetPageDropDowns.Replace("{type}", BKIC.SellingPoint.DTO.RequestResponseWrappers.PageType.DomesticHelp));

            if (dropDownResult.StatusCode == 200 && dropDownResult.Result.IsTransactionDone == true)
            {
                DataSet dropdownds = JsonConvert.DeserializeObject<DataSet>(dropDownResult.Result.dropdownresult);

                DataTable branches = dropdownds.Tables["BranchMaster"];
                if (branches != null && branches.Rows.Count > 0)
                {
                    ddlBranch.DataValueField = "AGENTBRANCH";
                    ddlBranch.DataTextField = "BranchName";
                    ddlBranch.DataSource = branches;
                    ddlBranch.DataBind();
                    ddlBranch.Items.Insert(0, new ListItem("--Please Select--", ""));
                }
            }
            var req = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest();
            req.AgentBranch = userInfo.AgentBranch;
            req.AgentCode = userInfo.AgentCode;

            var insuredResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredResponse>, 
                                BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyInsuredRequest>
                                (BKIC.SellingPoint.DTO.Constants.AdminURI.GetAgencyInsured, req);

            if (insuredResult.StatusCode == 200 && insuredResult.Result.AgencyInsured.Count > 0)
            {
                ddlCPR.DataSource = insuredResult.Result.AgencyInsured;
                ddlCPR.DataTextField = "CPR";
                ddlCPR.DataValueField = "InsuredCode";
                ddlCPR.DataBind();
                ddlCPR.Items.Insert(0, new ListItem("--Please Select--", ""));
                InsuredNames = insuredResult.Result.AgencyInsured;
            }
            txtIssueDate.Text = DateTime.Now.CovertToLocalFormat();
            txtIndroducedBy.Text = userInfo.UserName;
            ddlBranch.SelectedIndex = ddlBranch.Items.IndexOf(ddlBranch.Items.FindByValue(userInfo.AgentBranch));
        }

        protected void Changed_HomePolicy(object sender, EventArgs e)
        {
            try
            {
                master.ShowLoading = true;
                
              
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                var service = master.GetService();

                var request = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyDomesticRequest();
                    request.AgentBranch = userInfo.AgentBranch;
                    request.AgentCode = userInfo.AgentCode;
                    request.Agency = userInfo.Agency;
            
                ListEndorsements(service, userInfo);

                //Get saved policy details by document(policy) number.
                var url = BKIC.SellingPoint.DTO.Constants.TravelInsuranceURI.GetSavedQuoteDocumentNo.
                              Replace("{documentNo}", ddlHomePolicies.SelectedItem.Text.Trim())
                              .Replace("{agentCode}", request.AgentCode);

                var homeDetails = service.GetData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                 <BKIC.SellingPoint.DTO.RequestResponseWrappers.HomeSavedQuotationResponse>>(url);

                //Update policy details on current page for dispaly the details.
                if (homeDetails.StatusCode == 200 && homeDetails.Result.IsTransactionDone)
                {
                    var response = homeDetails.Result.HomeInsurancePolicy;
                }
                Page_CustomValidate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void gv_Sorting(object sender, GridViewSortEventArgs e)
        {
            //dlist.DefaultView.Sort = e.SortExpression + " " + SortDir(e.SortExpression);
            //gvMotorInsurance.DataSource = dlist;
            //gvMotorInsurance.DataBind();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void lnkbtnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void lnkbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void Changed_EndorsementType(object sender, EventArgs e)
        {
        }

        private void ListEndorsements(DataServiceManager service, OAuthTokenResponse userInfo)
        {
            if (ddlHomePolicies.SelectedIndex > 0)
            {
                var motorEndoRequest = new BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndoRequest();
                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    motorEndoRequest.Agency = userInfo.Agency;
                    motorEndoRequest.AgentCode = userInfo.AgentCode;
                    motorEndoRequest.InsuranceType = Constants.Motor;
                    motorEndoRequest.DocumentNo = ddlHomePolicies.SelectedItem.Text.Trim();

                    var result = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndoResult>,
                        BKIC.SellingPoint.DTO.RequestResponseWrappers.MotorEndoRequest>
                             (BKIC.SellingPoint.DTO.Constants.MotorEndorsementURI.GetAllEndorsements, motorEndoRequest);

                    if (result.StatusCode == 200 && result.Result.IsTransactionDone)
                    {
                        gvHomeEndorsement.DataSource = result.Result.MotorEndorsements;
                        gvHomeEndorsement.DataBind();

                        if (result.Result.MotorEndorsements.Count > 0)
                        {
                            _HomeEndorsementID = result.Result.MotorEndorsements[result.Result.MotorEndorsements.Count - 1].MotorEndorsementID;
                        }
                        else
                        {
                            _HomeEndorsementID = 0;
                        }
                    }
                }
            }
        }

        public void SaveAuthorize(bool isSave)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }
        public void CalculateTravelEndorsementQuote(bool showPremium)
        {
            master.ShowLoading = true;
            var userInfo = Session["UserInfo"] as OAuthTokenResponse;
            if (userInfo == null)
            {
                Response.Redirect("Login.aspx");
            }
            var service = master.GetService();

            var travelEndorementQuote = new BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementQuote();
            travelEndorementQuote.Agency = userInfo.Agency;
            travelEndorementQuote.AgentCode = userInfo.AgentCode;

            //SetEndorsementType(travelEndorementQuote);

            var travelEndorsementQuoteResult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                               <BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementQuoteResponse>,
                                               BKIC.SellingPoint.DTO.RequestResponseWrappers.TravelEndorsementQuote>
                                               (BKIC.SellingPoint.DTO.Constants.TravelEndorsementURI.GetTravelEndorsementQuote,
                                                travelEndorementQuote);

            if (travelEndorsementQuoteResult.StatusCode == 200 && travelEndorsementQuoteResult.Result.IsTransactionDone)
            {
                var endoresementPremium = travelEndorsementQuoteResult.Result.EndorsementPremium;
                if (travelEndorementQuote.EndorsementType == "CancelPolicy")
                {
                    endoresementPremium = travelEndorsementQuoteResult.Result.RefundPremium;
                }
                calculatedPremium.Value = endoresementPremium.ToString();

                var commisionRequest = new BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionRequest();
                commisionRequest.AgentCode = userInfo.AgentCode;
                commisionRequest.Agency = userInfo.Agency;
                commisionRequest.SubClass = subClass.Value;
                commisionRequest.PremiumAmount = endoresementPremium;

                var commissionresult = service.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper<BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionResponse>,
                                 BKIC.SellingPoint.DTO.RequestResponseWrappers.CommissionRequest>("api/insurance/Commission", commisionRequest);

                if (commissionresult.StatusCode == 200 && commissionresult.Result.CommissionAmount > 0)
                {
                    //commission.Text = Convert.ToString(commissionresult.Result.CommissionAmount);
                    calculatedCommision.Value = Convert.ToString(commissionresult.Result.CommissionAmount);
                    if (showPremium)
                    {
                        ShowPremium(userInfo, endoresementPremium, commissionresult.Result.CommissionAmount);
                    }
                }
                //txtExcessAmount.Text =  GetExcess().ToString();
                //amtDisplay.Visible = true;
            }

        }

        public void ShowPremium(OAuthTokenResponse userInfo, decimal Premium, decimal Commission)
        {
            amtDisplay.Visible = true;
            if (userInfo.Roles == "SuperAdmin" || userInfo.Roles == "BranchAdmin")
            {
                premiumAmount.Text = Convert.ToString(Premium);
                commission.Text = Convert.ToString(Commission);
                includeDisc.Visible = true;
            }
            else
            {
                premiumAmount1.Text = Convert.ToString(Premium);
                commission1.Text = Convert.ToString(Commission);
                excludeDisc.Visible = true;
            }
        }

        protected void validate_Premium(object sender, EventArgs e)
        {
            var Premium = Convert.ToDecimal(calculatedPremium.Value);
            var Commision = Convert.ToDecimal(calculatedCommision.Value);
            decimal Discount = string.IsNullOrEmpty(txtDiscount.Text) ? decimal.Zero : Convert.ToDecimal(txtDiscount.Text);
            var reduceablePremium = Premium - Commision;
            var premiumDiff = Premium - Discount;

            if (premiumDiff < reduceablePremium)
            {
                premiumAmount.Text = Convert.ToString(reduceablePremium);
                txtDiscount.Text = Convert.ToString(calculatedCommision.Value);
                commission.Text = Convert.ToString(0);
            }
            else if (Discount > Premium)
            {
                premiumAmount.Text = Convert.ToString(reduceablePremium);
                txtDiscount.Text = Convert.ToString(calculatedCommision.Value);
                commission.Text = Convert.ToString(0);
            }
            else
            {
                premiumAmount.Text = Convert.ToString(premiumDiff);
                commission.Text = Convert.ToString(Commision - Discount);
                btnSubmit.Enabled = true;
            }
        }

        protected void Changed_CPR(object sender, EventArgs e)
        {
            try
            {
                var client = new BKIC.SellingPoint.Presentation.ClientUtility();
                client.serviceManger = new KBIC.Utility.DataServiceManager(BKIC.SellingPoint.Presentation.ClientUtility.WebApiUri, "", false);

                var homereq = new BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyHomeRequest();
                var userInfo = Session["UserInfo"] as OAuthTokenResponse;

                if (userInfo == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    homereq.AgentCode = userInfo.AgentCode;
                    homereq.Agency = userInfo.Agency;
                    homereq.AgentBranch = userInfo.AgentBranch;
                    homereq.CPR = ddlCPR.SelectedIndex > 0 ? ddlCPR.SelectedItem.Text.Trim() : string.Empty;
                    homereq.Type = Constants.Motor;
                }
                //Get PolicyNo by Agency
                var homePolicies = client.serviceManger.PostData<BKIC.SellingPoint.DTO.RequestResponseWrappers.ApiResponseWrapper
                                   <BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyHomePolicyResponse>, 
                                   BKIC.SellingPoint.DTO.RequestResponseWrappers.AgencyHomeRequest>
                                   (BKIC.SellingPoint.DTO.Constants.MotorURI.GetMotorPoliciesByTypeByCPR, homereq);

                if (homePolicies.StatusCode == 200 && homePolicies.Result.AgencyHomePolicies.Count > 0)
                {
                    policyList = homePolicies.Result.AgencyHomePolicies;

                    ddlHomePolicies.DataSource = homePolicies.Result.AgencyHomePolicies;
                    ddlHomePolicies.DataTextField = "DOCUMENTNO";
                    ddlHomePolicies.DataValueField = "DOCUMENTNO";
                    ddlHomePolicies.DataBind();
                    ddlHomePolicies.Items.Insert(0, new ListItem("--Please Select--", "none"));
                }
                Page_CustomValidate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void gvHomeEndorsement_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvHomeEndorsement.Rows)
            {
                bool IsSaved = Convert.ToBoolean((row.FindControl("lblIsSaved") as Label).Text.Trim());
                bool IsActive = Convert.ToBoolean((row.FindControl("lblIsActive") as Label).Text.Trim());

                if (IsActive)
                {
                    var btnAuthorize = row.FindControl("lnkbtnAuthorize") as LinkButton;
                    btnAuthorize.Visible = false;

                    var btnDelete = row.FindControl("lnkbtnDelete") as LinkButton;
                    btnDelete.Visible = false;
                }
            }
        }

        public void Page_CustomValidate()
        {
            if (endorsementSubmitted.Value == "true")
            {
                Validate("HomeEndorsementValidation");
            }
        }

        protected void ddlPaymentMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPaymentMethods.SelectedIndex == 1)
            {
                //txtAccountNumber.Text = "";
                //txtAccountNumber.Enabled = false;
            }
            else
            {
                //txtAccountNumber.Enabled = true;
            }
            Page_CustomValidate();
        }

        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                Page_CustomValidate();
                if (Page.IsValid)
                {
                    if (ddlHomePolicies.SelectedIndex > 0)
                    {
                        //CalculateHomeEndorsementQuote(true);
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void btnAuthorize_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            finally
            {
                master.ShowLoading = false;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Homepage.aspx");
        }

        public void Reset_Content(object sender, EventArgs e)
        {
            modalBodyText.InnerText = "Your you sure want authorize this endorsement ?";
            btnOK.Text = "No";
            btnYes.Visible = true;
        }
    }
}